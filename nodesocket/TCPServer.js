'use strict';

const net = require('net');
const Terminal = require('./Terminal');
const PROTOCOL = process.argv[4] || "TK 103A";
var heartbeats = require('heartbeats');
var eventBus = require('./EventBus').Bus();
var request = require('request');
var Logger = require('./Logger');
var tcpserver;

var Redis = require("ioredis");

var redis = new Redis();

redis.subscribe('gpstrackingsystem-commandterminal', function(err, count) {
    if (err) {
        Logger.error("Error when subscribing to redis channel", {details: err});
    }
});

redis.on("error", function (err) {
    Logger.error("Error occured on redis channel",{details: err});
});

redis.on("message", (channel, message) => {
    message = JSON.parse(message);
    let command = message['data']['message'];
    let sockets = message['data']['sockets'];
    Logger.debug(sockets);
    sockets.forEach((terminalname) => {
        tcpserver.commandTerminals(command, terminalname);
    });
});

class TCPServer
{
    static Server()
    {
        return tcpserver;
    }

    constructor(port, address)
    {
        tcpserver = this;

        tcpserver.port = port || 31337;
        tcpserver.address = address || '127.0.0.1';
        tcpserver.terminals = new Object();

        eventBus.on('terminal', (data) => {
            if (tcpserver.terminals.hasOwnProperty(data.imei)) {
                let terminal = tcpserver.terminals[data.imei];
                terminal.receiveMessage(data.msg);
            }
        });
    }

    setIPPort(address, port)
    {
        tcpserver.address = address || '127.0.0.1';
        tcpserver.port = port || 31337;
    }

    start(callback)
    {
        Logger.info("Starting tcpserver");
        tcpserver.connection = net.createServer(tcpserver.handleClientConnected);
        tcpserver.connection.listen(tcpserver.port, tcpserver.address);
        tcpserver.connection.on('listening', callback);
    }

    /**
     * Handler for when a socket tcpserver is created
     * @param {net.Socket} socket 
     */
    handleClientConnected(socket)
    {
        let terminal = new Terminal(socket);
        tcpserver.addTerminal(terminal);
        let heart = heartbeats.createHeart(75000, terminal.name);
        
        heart.createEvent(1, function () {
            terminal.active -= 1;
        });

        heart.createEvent(2, function () {
            if (terminal.active <= 0) {
                tcpserver.removeTerminal(terminal);
            }
        });

        socket.on('data', (data) => {
            let message = data.toString().replace(/[\n\r]*$/, '');
            Logger.info(`${terminal.name} said: ${message}`);
            
            if (terminal.active < 3) {
                terminal.active += 1;
            }

            // TODO: Pass tcpserver data to PHP, get a response and forward that response to the client
            request.put(process.env.APP_URL + "/api/tcpserverconnections/" + terminal.name, {
                headers : {
                    accept: 'application/json'
                },
                form : {
                    'name' : terminal.name,
                    'message' : data,
                    'protocol' : PROTOCOL
                }
            }, function (err, response, body) {
                if (err) {
                    Logger.error(err, {details: body});
                } else {
                    body = JSON.parse(body);
                    if (body['response'] && body['response'] != '') {
                        terminal.receiveMessage(body['response']);
                    }
                }
            });
        });

        socket.on('close', () => {
            heart.kill();
            tcpserver.removeTerminal(terminal);
        });

        socket.on('error', (err) => {
            Logger.error(err.message, {details: err, stacktrace: err.stack});
        });
    }

    /**
     * Adds a terminal from the tcpserver's list
     * 
     * @param {Terminal} terminal 
     */
    addTerminal(terminal)
    {
        if (tcpserver.terminals.hasOwnProperty(terminal.name)) {
            return;
        }

        tcpserver.terminals[terminal.name] = terminal;
        request.post(process.env.APP_URL + "/api/tcpserverconnections", {
            form : {
                'name' : terminal.name
            }
        }, function (err, response, body) { 
            if (err) {
                Logger.error(err, {details: body});
            }
        });
        Logger.info(`${terminal.address} connected.`);
    }

    /**
     * Removes a terminal from the tcpserver's list
     * 
     * @param {Terminal} terminal 
     */
    removeTerminal(terminal)
    {
        request.del(process.env.APP_URL + "/api/tcpserverconnections/" + terminal.name, function (err, response, body) { 
            if (err) {
                Logger.error(err, {details: body});
            }
        });
        if (tcpserver.terminals.hasOwnProperty(terminal.name)) {
            if (terminal) {
                Logger.info(`${terminal.address} disconnected.`);
                terminal.socket.destroy();
                delete tcpserver.terminals[terminal.name];
            }  
        }
    }

    /**
     * Stops the tcpserver, slightly more gracefully
     * 
     */
    stop(callback)
    {
        tcpserver.connection.close();
        
       request.post(process.env.APP_URL + "/api/tcpsocketserver/terminate", {
            form : {
                'sockets' : Object.keys(tcpserver.terminals)
            }
        }, callback);
    }

    commandTerminals(command, terminalname)
    {
        if (!terminalname || !command) {
            return;
        }
        Logger.debug(command);
        if (tcpserver.terminals.hasOwnProperty(terminalname)) {
            if (tcpserver.terminals[terminalname].receiveMessage(command))
            {
                return;
            }
        }
        request.del(process.env.APP_URL + "/api/tcpserverconnections/" + terminalname, function (err, response, body) { 
            if (err) {
                Logger.error(err, {details: body});
            }
        });
    }
}

module.exports = TCPServer;