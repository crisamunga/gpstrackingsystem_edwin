'use strict';
const net = require('net');

class Terminal {
    /**
     * Create new terminal with the defined socket
     * @param {net.Socket} socket 
     */
    constructor(socket) {
        this.ip = socket.remoteAddress;
        this.port = socket.remotePort;
        this.address = `${this.ip}:${this.port}`;
        this.name = this.address.replace(/(\.|:)/g,"");
        this.socket = socket;

        this.exists = false;
        this.active = 4;
    }

    /**
     * Writes a message to this terminal and returns a boolean value indicating if it was successful
     * 
     * @param {string} message 
     * @returns {boolean}
     */
    receiveMessage(message) {
        if (this.socket.writable) {
            this.socket.write(message);    
        }
        return this.socket.writable;
    }
}

module.exports = Terminal;