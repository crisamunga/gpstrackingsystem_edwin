<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TerminalEventType extends Model
{
    protected $fillable = ['keyword','type','description'];

    public function terminal_event_entries()
    {
        return $this->hasMany('App\TerminalEventEntry','keyword','keyword');
    }
}
