<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TcpServerConnections extends Model
{
    protected $fillable = ['name','data','imei'];

    public function setDataAttribute($data)
    {
        $this->attributes['data'] = json_encode($data);
    }

    public function getDataAttribute($data)
    {
        return json_decode($this->attributes['data'], true);
    }

    public function terminal()
    {
        return $this->belongsTo('App\Terminal','imei','imei');
    }
}
