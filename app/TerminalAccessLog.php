<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TerminalAccessLog extends Model
{
    protected $fillable = ['description'];
    
    public function scopeDateBetween($query, $start, $end) {
        return $query->whereBetween('created_at', [$start, $end]);
    }

    public function user() 
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function terminal() 
    {
        return $this->belongsTo('App\Terminal','terminal_id');
    }
}
