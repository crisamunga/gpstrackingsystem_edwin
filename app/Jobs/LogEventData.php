<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\TerminalEventEntry;
use App\Terminal;
use App\Driver;

class LogEventData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        $terminal = Terminal::where('imei',$data['imei'])->with('driver')->first();
        if (isset($terminal)) {
            $driver = $terminal->driver;
            TerminalEventEntry::create([
                'imei' => $data['imei'],
                'driver_id' => (isset($driver) ? $driver->id : null),
                'keyword' => $data['keyword'],
                'latitude' => $data['latitude'],
                'longitude' => $data['longitude'],
                'currenttime' => $data['currenttime'],
            ]);
        }
    }
}