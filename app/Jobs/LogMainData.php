<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\DataLogEntry;
use App\Terminal;
use App\Driver;

class LogMainData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        $terminal = Terminal::where('imei',$data['imei'])->first();
        if (isset($terminal)) {
            $driver = Driver::where('imei',$data['imei'])->first();
            DataLogEntry::create([
                'imei' => $data['imei'],
                'driver_id' => (isset($driver) ? $driver->id : null),
                'speed' => $data['speed'],
                'distance' => $data['distance'],
                'oil1' => $data['oil1'],
                'oil2' => $data['oil2'],
                'temperature' => $data['temperature'],
                'workingtime' => $data['workingtime'],
                'latitude' => $data['latitude'],
                'longitude' => $data['longitude'],
                'altitude' => $data['altitude'],
                'currenttime' => $data['currenttime'],
            ]);
        }
    }
}
