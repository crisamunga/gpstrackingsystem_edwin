<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendCommandToTerminal implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;
    public $sockets;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message, $sockets)
    {
        $this->message = $message;
        $this->sockets = $sockets;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['gpstrackingsystem-commandterminal'];
    }

     /**
     * Get the data that should be broadcast with the event
     * 
     * @return string|object|array
     */
    public function broadcastWith()
    {
        return [
            'message' => $this->message,
            'sockets' => $this->sockets
        ];
    }

    /**
     * Get the event name the event should be broadcasted as
     * 
     * @return string
     */
    public function broadcastAs()
    {
        return 'command';
    }
}