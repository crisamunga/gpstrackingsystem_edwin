<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewTerminalEvent implements ShouldBroadcast
{
    public $tries = 5;
    
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $imei;
    public $data;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($imei, $data)
    {
        $this->imei = $imei;
        $this->data = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('gpstrackingsystem-event-'.$this->imei);
    }

    /**
     * Get the data that should be broadcast with the event
     * 
     * @return string|object|array
     */
    public function broadcastWith()
    {
        return [
            'data' => $this->data
        ];
    }

    /**
     * Get the event name the event should be broadcasted as
     * 
     * @return string
     */
    public function broadcastAs()
    {
        return 'notification';
    }
}
