<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TerminalEventEntry extends Model
{
    protected $fillable = [
        'imei',
        'driver_id',
        'keyword',
        'latitude',
        'longitude',
        'currenttime'
    ];

    public function terminal()
    {
        return $this->belongsTo('App\Terminal','imei','imei');
    }

    public function terminal_event_type()
    {
        return $this->belongsTo('App\TerminalEventType','keyword','keyword');
    }

    public function scopeDateBetween($query, $start, $end) {
        return $query->whereBetween('created_at', [$start, $end]);
    }
}
