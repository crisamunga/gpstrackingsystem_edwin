<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DataLogEntry;

class Terminal extends Model
{
    protected $fillable = [
        'phone','imei','expiry_date','model','identifier','description',
    ];

    public function getDisplayAttribute() {
        $display = $this->description;
        if ($display == null) {
            $display = $this->identifier;
            if ($display == null) {
                $display = $this->imei;
                if ($display == null) {
                    $display = $this->model;
                }
            }
        }
        return $display;
    }

    public function user() 
    {
        return $this->belongsTo('App\User','client_id');
    }

    public function getLastLocationAttribute()
    {
        return DataLogEntry::where('imei',$this->imei)->where('latitude','!=',null)->where('longitude','!=',null)->orderBy('created_at','desc')->first();
    }

    public function configuration() 
    {
        return $this->belongsTo('App\TerminalConfiguration','terminal_configuration_id');
    }

    public function terminal_group()
    {
        return $this->belongsTo('App\TerminalGroup','terminal_group_id');
    }

    public function terminal_main_data_entries()
    {
        return $this->hasMany('App\DataLogEntry','imei','imei');
    }

    public function terminal_event_entries()
    {
        return $this->hasMany('App\TerminalEventEntry','imei','imei');
    }

    public function terminal_duration_logs()
    {
        return $this->hasMany('App\TerminalDurationLog','imei','imei');
    }

    public function data_log_entries()
    {
        return $this->hasMany('App\DataLogEntry','imei','imei');
    }

    public function terminal_config_logs() 
    {
        return $this->hasMany('App\TerminalConfigLog','user_id');
    }

    public function terminal_access_logs() 
    {
        return $this->hasMany('App\TerminalAccessLog','user_id');
    }

    public function driver()
    {
        return $this->hasOne('App\Driver','imei','imei');
    }

    public function tcpserverconnections() 
    {
        return $this->hasMany('App\TcpServerConnections','imei','imei');
    }
}