<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Utilities\Calculator;

class TerminalDurationLog extends Model
{
    public function terminal()
    {
        return $this->belongsTo('App\Terminal','imei','imei');
    }

    public function scopeDateBetween($query, $start, $end) {
        return $query->whereBetween('created_at', [$start, $end]);
    }

    public function getDurationAttribute()
    {
        return Calculator::getTimeDuration($this->start_time, $this->stop_time);
    }

    public function getDistanceAttribute()
    {
        return Calculator::getDistance($this->start_latitude, $this->start_longitude, $this->stop_latitude, $this->stop_longitude);
    }
}