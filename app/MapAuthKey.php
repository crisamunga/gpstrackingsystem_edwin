<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MapAuthKey extends Model
{
    protected $fillable = [
        'key','terminals'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
