<?php
namespace App\Utilities;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Auth;
use Carbon\Carbon;
use App\User;
use App\Driver;
use App\Terminal;
use App\TerminalEventType;
use App\Utilities\Chart;
use App\Utilities\PerformanceData;
use App\Utilities\DriverPerformanceData;
use App\Utilities\DriverEventData;
use \Lava;
use Khill\Lavacharts\Lavacharts;

class Test
{
    public static function runTest()
    {
        $user = Auth::user();

        $input = [
            'summary' => 0,
            'start_date' => '2018-01-01',
            'start_time' => '00:00',
            'end_date' => '2018-04-13',
            'end_time' => '00:00',
        ];

        $datamaker = new DriverEventData();

        $drivers = Driver::all();
        $types = TerminalEventType::all()->take(-10);

        $datamaker->setdrivers($drivers)->settypes($types)->fill($input)->run();

        var_dump($datamaker->data);
    }
}