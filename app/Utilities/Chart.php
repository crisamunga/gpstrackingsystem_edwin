<?php

namespace App\Utilities;

use Carbon\Carbon;
use Khill\Lavacharts\Lavacharts;
use \Lava;

class Chart
{
    public static function PrepareChart($type, $name, $dataTable, $options)
    {
        switch ($type) {
            case 'AreaChart':
                Lava::AreaChart($name, $dataTable, $options);
                break;
            case 'BarChart':
                $temp = $options['vAxis'];
                $options['vAxis'] = $options['hAxis'];
                $options['hAxis'] = $temp;
                Lava::BarChart($name, $dataTable, $options);
                break;
            case 'CalendarChart':
                Lava::CalendarChart($name, $dataTable, $options);
                break;
            case 'ColumnChart':
                Lava::ColumnChart($name, $dataTable, $options);
                break;
            case 'ComboChart':
                Lava::ComboChart($name, $dataTable, $options);
                break;
            case 'DonutChart':
                Lava::DonutChart($name, $dataTable, $options);
                break;
            case 'GaugeChart':
                Lava::GaugeChart($name, $dataTable, $options);
                break;
            case 'GeoChart':
                $options['region'] = 'KE';
                $options['displaymode'] = 'markers';
                $options['colorAxis'] = ['green','blue'];
                Lava::GeoChart($name, $dataTable, $options);
                break;
            case 'PieChart':
                Lava::PieChart($name, $dataTable, $options);
                break;
            case 'LineChart':
                Lava::LineChart($name, $dataTable, $options);
                break;
            case 'ScatterChart':
                Lava::ScatterChart($name, $dataTable, $options);
                break;
            case 'TableChart':
                Lava::TableChart($name, $dataTable, $options);
                break;
            default:
                break;
        }
    }

    public static function chooseChart($type)
    {
        $types = [
            'ColumnChart', 'BarChart', 'LineChart', 'AreaChart', 'ScatterChart', 'TableChart'
        ];

        return $types[$type];
    }
}