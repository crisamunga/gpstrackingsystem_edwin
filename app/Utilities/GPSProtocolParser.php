<?php
namespace App\Utilities;

use Carbon\Carbon;
use Log;

class GPSProtocolParser
{
    public static function Parse($message, $type)
    {
        $data = array();
        switch ($type) {
            case 'TK 103A':
                $data = GPSProtocolParser::ParseTK103AMessage($message);
                break;
            default:
                Log::critical('Unknown protocol: '.$type);
                return null;
                break;
        }
        return $data;
    }

    public static function Compose($data, $protocol)
    {
        $message = "";
        switch ($protocol) {
            case 'TK 103A':
                $message = GPSProtocolParser::ComposeTK103AMessage($data);
                break;
            default:
                Log::critical('Unknown protocol: '.$type);
                return null;
                break;
        }
        return $message;
    }

    private static function ParseTK103AMessage($message)
    {
        $data = array();
        $config = config('gpsprotocol.TK 103A');
        // Remove prefix, terminator and white spaces
        $trimmed_message = rtrim($message);
        $trimmed_message = ltrim($trimmed_message,$config['prefix']);
        $trimmed_message = rtrim($trimmed_message, $config['terminator']);

        $pieces = preg_split($config['delimiter'],$trimmed_message);
        $arr_size = count($pieces);

        if ($arr_size <= 0) {
            return $data;
        }
        
        if ($arr_size == 1) {
            $data = $config['device']['keywords']['empty'];
            $data['imei'] = $pieces[0];
            $data['data'] = array();
        }

        if ($arr_size == 2 && $pieces[1] == "A") {
            $data = $config['device']['keywords']['A'];
            $data['imei'] = $pieces[0];
            $data['data'] = array();
        }

        if ($arr_size > 2) {
            $data = $config['device']['keywords'][$pieces[1]];
            $data['imei'] = $pieces[0];
            $data['data'] = array();
            for ($i = 0; $i  < count($config['device']['parameters']); $i ++) {
                $parameter = $config['device']['parameters'][$i];
                if ($i >= $arr_size) {
                    $value = null;
                    switch ($parameter) {
                        case 'oilPercent1': case 'oilPercent2': case 'directionOrRequest': 
                        case 'altitude': case 'temperature': case 'speed':
                            $value = 0;
                            break;
                        case 'accState': case 'doorOpenState': case 'gpsSignalPresent':
                            $value = false;
                            break;
                        case 'time':
                            $value = Carbon::now()."";
                        default:
                            break;
                    }
                    $data['data'][$parameter] = $value;
                    continue;
                }
                $datapiece = $pieces[$i];
                switch ($parameter) {
                    case 'time':
                        $data['data'][$parameter] = ($datapiece == "" ? Carbon::now()."" : Carbon::parse("20".$datapiece)."");
                        break;
                    case 'latitude': 
                        if ($pieces[4] == "F" && $pieces[6] == "A") {
                            $latdeg = (double) substr($datapiece,0,2);
                            $latmin = (double) substr($datapiece,2);
                            $lat = $latdeg + ($latmin / 60);
                            $lat *= ($pieces[$i + 1] == "S" ? -1 : 1);
                            $data['data'][$parameter] = $lat;
                        } else {
                            $data['data'][$parameter] = null;
                        }
                        break;
                    case 'longitude': 
                        if ($pieces[4] == "F" && $pieces[6] == "A") {
                            $londeg = (double) substr($datapiece,0,3);
                            $lonmin = (double) substr($datapiece,3);
                            $lon = $londeg + ($lonmin / 60);
                            $lon *= ($pieces[$i + 1] == "W" ? -1 : 1);
                            $data['data'][$parameter] = $lon;
                        } else {
                            $data['data'][$parameter] = null;
                        }
                        break;
                    
                    case 'gpsSignalPresent':
                        $data['data'][$parameter] = ($pieces[4] == "F" && $pieces[6] == "A");
                        break;
                    case 'speed':
                        $speed = (double) $datapiece;
                        $speed *= 37.04;
                        $data['data'][$parameter] = $speed;
                        break;
                    case 'oilPercent1': case 'oilPercent2': case 'directionOrRequest': 
                    case 'altitude': case 'temperature':
                        $value = (double) $datapiece;
                        $data['data'][$parameter] = $value;
                        break;
                    case 'accState': case 'doorOpenState':
                        $value = $datapiece == "1";
                        $data['data'][$parameter] = $value;
                    case 'keyword':
                        $data['data'][$parameter] = $datapiece;
                    default:
                        break;
                }
            }
        }
        $data['current_time'] = Carbon::now();
        return $data;
    }

    private static function ComposeTK103AMessage($data)
    {
        $config = config('gpsprotocol.TK 103A.webapp');
        $message = "";
        $message .= $config['prefix'];
        $message .= $data['terminal']->imei;
        $message .= $config['delimiter'];
        $message .= $config[$data['command']]['keyword'];
        if ($data['parameter'] != '') {
            $message .= $config['delimiter'];
            $message .= $data['parameter'];
        }
        $message .= $config['terminator'];
        return $message;
    }
}
