<?php
namespace App\Utilities;

class ReportQueryUtility
{
    public static $datesummaries = [
        "CONCAT(DATE(currenttime),' ',LPAD(HOUR(currenttime),2,0), ':', LPAD(MINUTE(currenttime)-(MINUTE(currenttime) MOD 5),2,0), ':00') AS datetime",
        "CONCAT(DATE(currenttime),' ',LPAD(HOUR(currenttime),2,0), ':', LPAD(MINUTE(currenttime)-(MINUTE(currenttime) MOD 10),2,0), ':00') AS datetime",
        "CONCAT(DATE(currenttime),' ',LPAD(HOUR(currenttime),2,0), ':', LPAD(MINUTE(currenttime)-(MINUTE(currenttime) MOD 30),2,0), ':00') AS datetime",
        "CONCAT(DATE(currenttime),' ',LPAD(HOUR(currenttime),2,0), ':00:00') AS datetime",
        "CONCAT(DATE(currenttime),' ',LPAD(HOUR(currenttime)-(HOUR(currenttime) MOD 2),2,0), ':00:00') AS datetime",
        "CONCAT(DATE(currenttime),' ',LPAD(HOUR(currenttime)-(HOUR(currenttime) MOD 4),2,0), ':00:00') AS datetime",
        "CONCAT(DATE(currenttime),' ',LPAD(HOUR(currenttime)-(HOUR(currenttime) MOD 6),2,0), ':00:00') AS datetime",
        "CONCAT(DATE(currenttime),' ',LPAD(HOUR(currenttime)-(HOUR(currenttime) MOD 12),2,0), ':00:00') AS datetime",
        "DATE(currenttime) AS datetime",
        "CONCAT(YEAR(currenttime),'-',LPAD(MONTH(currenttime),2,0), '-', LPAD(DAY(currenttime)-(DAY(currenttime) MOD 7),2,0)) AS datetime",
        "CONCAT(YEAR(currenttime),'-',LPAD(MONTH(currenttime),2,0), '-', LPAD(DAY(currenttime)-(DAY(currenttime) MOD 14),2,0)) AS datetime",
        "CONCAT(YEAR(currenttime),'-',LPAD(MONTH(currenttime),2,0)) AS datetime",
        "CONCAT(YEAR(currenttime),'-',LPAD(MONTH(currenttime)-(MONTH(currenttime) MOD 3),2,0)) AS datetime",
        "CONCAT(YEAR(currenttime),'-',LPAD(MONTH(currenttime)-(MONTH(currenttime) MOD 6),2,0)) AS datetime",
        "YEAR(currenttime) AS datetime"
    ];

    public static $data = [
        'SUM(distance) AS distance',
        'AVG(speed) AS speed',
        'SUM(oil1) AS oil1',
        'SUM(oil2) AS oil2',
        'AVG(temperature) AS temperature',
        'SUM(workingtime) AS workingtime',
        'COUNT(*) AS frequency',
        'AVG(longitude) AS longitude,AVG(latitude) AS latitude'
    ];

    public static function getSummaryString($dateSummaryIndex)
    {
        $querystring = ReportQueryUtility::$datesummaries[$dateSummaryIndex];
        // if(isset($dataIndex) && $dataIndex == 'All')
        // {
        //     foreach (ReportQueryUtility::$data as $dataquery) {
        //         $querystring.=','.$dataquery;
        //     }
        // } else if(isset($dataIndex) && $dataIndex < 6)
        // {
        //     $querystring.=','.ReportQueryUtility::$data[$dataIndex];
        // }
        // if ($hasfrequency) {
        //     $querystring.=',COUNT(*) AS frequency';
        // }
        // if ($hascoords) {
        //     $querystring.=',AVG(longitude) AS longitude,AVG(latitude) AS latitude';
        // }
        return $querystring;
    }
}