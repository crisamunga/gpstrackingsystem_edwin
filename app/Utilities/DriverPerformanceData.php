<?php
namespace App\Utilities;

use Illuminate\Support\Facades\DB;
use App\Utilities\ReportQueryUtility;
use Carbon\Carbon;

class DriverPerformanceData 
{
    protected $db;
    protected $querystring;
    protected $drivers;
    public $data;

    function __construct() {
        $this->querystring = "";
        $this->db = DB::table('data_log_entries');
    }
    /**
     * Runs the query and sets the data in $data variable
     * Returns the data that has been collected
     * @return App\Utilities\PerformanceData
     */
    public function run()
    {
        $this->data = $this->db
                ->groupBy('driver_id')
                ->groupBy('datetime')
                ->orderBy('datetime','asc')
                ->get();
        return $this;
    }

    /**
     * Prepares the query from the provided input
     * Returns the data that has been collected
     * @return App\Utilities\PerformanceData
     */
    public function fill($input)
    {
        $this->querystring = ReportQueryUtility::$datesummaries[$input['summary']];
        $this->querystring .=','.implode(',',ReportQueryUtility::$data);
        $this->db->select(DB::raw('driver_id,'.$this->querystring))
                ->havingRaw('datetime BETWEEN TIMESTAMP("'.$input['start_date'].'","'.$input['start_time'].'") AND TIMESTAMP("'.$input['end_date'].'","'.$input['end_time'].'")');
        return $this;
    }

    /**
     * Prepares the chart array from the provided input
     * Returns the data that has been collected
     * 
     * @param  string  $fields
     * @return array
     */
    public function getChartData($field)
    {
        $drivers = $this->drivers->pluck('name','id')->toArray();
        $header = [['date','Date']];
        $driver_ids = array();
        foreach ($drivers as $key => $value) {
            array_push($header, ['number',$value]);
            array_push($driver_ids, $key);
        }
        $datatable = array();
        $this->data->groupBy('datetime')->map(function($row, $date) use ($driver_ids, $field, &$datatable) {
            $rowdata = [$date];
            for ($i=0; $i < count($driver_ids); $i++) { 
                $driver_id = $driver_ids[$i];
                $driverdata = $row->firstWhere('driver_id',$driver_id);
                $data = (isset($driverdata)) ? $driverdata->{$field} : 0;
                array_push($rowdata, $data);
            }
            array_push($datatable, $rowdata);
            return $row;
        });
        $r = [
            'coldefs' => $header,
            'data' => $datatable
        ];
        return $r;
    }

    /**
     * Prepares the chart array from the provided input
     * Returns the data that has been collected
     * 
     * @param  string  $fields
     * @return array
     */
    public function getChartSummaryData($field, $datasource)
    {
        $drivers = $this->drivers->pluck('name','id')->toArray();
        $header = [['string', ucfirst($field)], ['number', ucfirst($datasource)]];
        $f = ($field == 'driver') ? 'driver_id' : $field;

        $datatable = array();
        $this->data->groupBy($f)->map(function($row, $fkey) use ($datasource, $drivers, $field, &$datatable) {
            $rowdata = ($field == 'driver') ? [$drivers[$fkey]] : [$fkey];
            if ($datasource == 'distance' || $datasource == 'oil1' || $datasource == 'oil2' || $datasource == 'frequency') {
                array_push($rowdata, $row->sum($datasource));
            } else {
                array_push($rowdata, $row->avg($datasource));
            }
            array_push($datatable, $rowdata);
        });
        $r = [
            'coldefs' => $header,
            'data' => $datatable
        ];
        return $r;
    }

    /**
     * Prepares the XLS Data array from the retrieved data
     * Returns the data that has been collected
     * 
     * @return array
     */

    public function getXLSData()
    {
        $drivers = $this->drivers->pluck('name','id')->toArray();
        $xlsheader = ['Date','driver','Speed','Distance','Oil 1','Oil 2','Temperature','Working time','latitude','longitude'];
        $xlsdata = $this->data->map(function ($data) use ($drivers, $xlsheader) {
            return [$data->datetime, $drivers[$data->driver_id], $data->speed, $data->distance, $data->oil1, $data->oil2, $data->temperature, $data->workingtime, $data->latitude, $data->longitude];
        })->toArray();
        array_unshift($xlsdata,$xlsheader);
        return $xlsdata;
    }

    /**
     * Prepares the chart array from the provided input
     * Returns the data that has been collected
     * 
     * @return array
     */

    public function getLocationData ()
    {
        $drivers = $this->drivers->pluck('name','id')->toArray();
        $var = $this->data->map(function ($locdata, $key) use ($drivers) {
            $r = [
                'terminal' => $drivers[$locdata->driver_id],
                'latitude' => $locdata->latitude,
                'longitude' => $locdata->longitude,
                'description' => '
                <div id="iw-container">
                <div class="iw-title">
                <div>'.$drivers[$locdata->driver_id].'</div>
                <div>'.Carbon::createFromTimeStamp(strtotime($locdata->datetime))->toDayDateTimeString().'</div>
                </div>
                <div class="iw-content">
                <table class="center-block" width="100%">
                <tr> <th class="text-right"> Latitude:</th> <td>'.$locdata->latitude.' </td> </tr>
                <tr> <th class="text-right"> Longitude:</th> <td>'.$locdata->longitude.' </td> </tr>
                <tr> <th class="text-right"> Distance:</th> <td>'.$locdata->distance.' </td> </tr>
                <tr> <th class="text-right"> Speed:</th> <td>'.$locdata->speed.' </td> </tr>
                <tr> <th class="text-right"> Temperature:</th> <td>'.$locdata->temperature.' </td> </tr>
                <tr> <th class="text-right"> Oil 1:</th> <td>'.$locdata->oil1.' </td> </tr>
                <tr> <th class="text-right"> Oil 2:</th> <td>'.$locdata->oil2.' </td> </tr>
                <tr> <th class="text-right"> Working time:</th> <td>'.$locdata->workingtime.' </td> </tr>
                </table>
                </div>
                </div>'
            ];
            return $r;
        })->toArray();
        return $var;
    }

    /**
     * Used to set the drivers for which records are retrieved
     *
     * @param  App\driver  $drivers
     * @return App\Utilities\PerformanceData
     */
    public function setdrivers($drivers)
    {
        $this->drivers = $drivers;
        $driver_ids = $drivers->pluck('id')->toArray();
        $this->db->whereIn('driver_id', $driver_ids );
        return $this;
    }

    /**
     * Used to set start and end date bounds for the data to retrieve
     * Returns this same object to support chaining
     * @param  Carbon\Carbon  $start
     * @param  Carbon\Carbon  $end
     * @return App\Utilities\PerformanceData
     */
    public function whereBetween($start, $end)
    {
        $this->db->havingRaw('datetime BETWEEN TIMESTAMP("'.$start.'") AND TIMESTAMP("'.$end.'")');
        return $this;
    }

    /**
     * Used to set the summary duration to be used
     * Returns this same object to support chaining
     *
     * @param  string  $summary
     * @return App\Utilities\PerformanceData
     */
    public function summary($summary)
    {
        $this->querystring .= ReportQueryUtility::$datesummaries[$input['summary']];
        return $this;
    }

    /**
     * Select ordering field and direction
     * Returns this same object to support chainign
     *
     * @param  string  $field
     * @param  int  $id
     * @return App\Utilities\PerformanceData
     */
    public function orderBy($field, $direction)
    {
        $this->db->orderBy($field, $direction);
        return $this;
    }

    /**
     * Selects fields with which to group the data
     *
     * @param  string  $field
     * @param  int  $id
     * @return App\Utilities\PerformanceData
     */
    public function groupBy($field)
    {
        $this->db->groupBy($field);
        return $this;
    }
}