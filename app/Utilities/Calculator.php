<?php

namespace App\Utilities;

use Carbon\Carbon;

class Calculator
{
    public static function getDistance($lat1, $lon1, $lat2, $lon2)
    {
        $R = 6371e3;
        $t1 = deg2rad($lat1);
        $t2 = deg2rad($lat2);
        $dt = deg2rad($lat2-$lat1);
        $dl = deg2rad($lon2-$lon1);

        $a = sin($dt/2) * sin($dt/2) + cos($t1) * cos($t2) * sin($dl/2) * sin($dl/2);
        $c = 2 * atan2(sqrt($a), sqrt(1-$a));
        $d = $R * $c;
        $d = round($d,0)/1000;
        return $d;
    }

    public static function getTimeDuration($startTime, $stopTime)
    {
        return Calculator::getTimeDurationInSeconds($startTime, $stopTime) / 3600;
    }

    public static function getTimeDurationInSeconds($startTime, $stopTime)
    {
        $start = Carbon::parse($startTime);
        $stop = Carbon::parse($stopTime);
        $result = $start->diffInSeconds($stop);
        return $result;
    }
}