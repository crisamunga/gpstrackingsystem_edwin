<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class ReportsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->is('home/reports') || $request->is('home/reports/*')) {
            if (Auth::user()->hasPermissionTo('View Reports'))
            {
                return $next($request);
            }
        }
        abort('401');
    }
}
