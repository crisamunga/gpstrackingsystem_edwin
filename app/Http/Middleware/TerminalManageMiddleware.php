<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class TerminalManageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->is('manageterminals')) {
            if (Auth::user()->hasPermissionTo('Manage Terminals - Client'))
            {
                return $next($request);
            }
        }

        if ($request->is('userterminals') || $request->is('userterminals/*')) {
            if (Auth::user()->hasPermissionTo('Manage Terminals - Client'))
            {
                return $next($request);
            }
        }

        if ($request->is('terminalgroups') || $request->is('terminalgroups/*')) {
            if (Auth::user()->hasPermissionTo('Manage Terminals - Client'))
            {
                return $next($request);
            }
        }

        if ($request->is('drivers') || $request->is('drivers/*')) {
            if (Auth::user()->hasPermissionTo('Manage Terminals - Client'))
            {
                return $next($request);
            }
        }

        abort('401');
    }
}
