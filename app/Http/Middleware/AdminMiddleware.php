<?php

namespace App\Http\Middleware;

use Closure;
use AUth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->hasRole('Super Admin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Access Admin Panel'))
        {
            return $next($request);
        }
        return $next($request);
    }
}
