<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class TerminalAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->is('terminaladmin')) {
            if (Auth::user()->hasPermissionTo('Manage Terminals - Admin'))
            {
                return $next($request);
            }
        }        

        if ($request->is('terminals') || $request->is('terminals/*')) {
            if (Auth::user()->hasPermissionTo('Manage Terminals - Admin'))
            {
                return $next($request);
            }
        }
        
        if ($request->is('terminalconfigs') || $request->is('terminalconfigs/*')) {
            if (Auth::user()->hasPermissionTo('Manage Terminals - Admin'))
            {
                return $next($request);
            }
        }        

        abort('401');
    }    
}
