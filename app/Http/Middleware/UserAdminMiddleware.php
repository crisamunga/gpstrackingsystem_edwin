<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {           
        if ($request->is('roles') || $request->is('roles/*')) {
            if (Auth::user()->hasPermissionTo('Administer roles and permissions'))
            {
                return $next($request);
            }
        }        

        if ($request->is('permissions') || $request->is('permissions/*')) {
            if (Auth::user()->hasPermissionTo('Administer roles and permissions'))
            {
                return $next($request);
            }
        }

        if ($request->is('manageusers')) {
            if (Auth::user()->hasPermissionTo('Manage Users'))
            {
                return $next($request);
            }
        }

        if ($request->is('users') || $request->is('users/*')) {
            if (Auth::user()->hasPermissionTo('Manage Users'))
            {
                return $next($request);
            }
        }
        
        abort('401');
    }
}
