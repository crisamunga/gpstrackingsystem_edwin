<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Utilities\ReadFile;
use File;

class ViewLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('server.logs.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function other()
    {
        return view('server.logs.other');
    }

    /**
     * Display the specified resource.
     * 
     * @param \Illuminate\Http\Request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'logfile' => ['required',Rule::in([0,1,2,3])],
            'length' => 'required|integer|min:10|max:10000'
        ]);
        if ($validator->fails()) {
            return view('server.logs.other')
                ->withErrors($validator)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'LOGS',
                    'type' => 'ERROR'
                ]
            );
        }
        $rlog = $request['logfile'];
        $path;
        if ($rlog == 0) {
            $path = Storage::disk('socketserver-logs')->getDriver()->getAdapter()->applyPathPrefix('socketserver.err.log');
        } else if ($rlog == 1) {
            $path = Storage::disk('socketserver-logs')->getDriver()->getAdapter()->applyPathPrefix('socketserver.out.log');
        } else if ($rlog == 2) {
            $path = Storage::disk('socketserver-logs')->getDriver()->getAdapter()->applyPathPrefix('echoserver.err.log');
        } else if ($rlog == 3) {
            $path = Storage::disk('socketserver-logs')->getDriver()->getAdapter()->applyPathPrefix('echoserver.out.log');
        }
        if (!file_exists($path)) {
            return view('server.logs.other')->with('logdata', '')->with('size', 0);
        }
        $length = $request['length'];
        $content = ReadFile::tailCustom($path, $length);
        $size = File::size($path);

        // $content = str_replace('/n','<br>',$content);
        $content = nl2br($content);

        return view('server.logs.other')->with('logdata', $content)->with('size', $size);
    }
}
