<?php

namespace App\Http\Controllers;

use Auth;
use Validator;

use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MyAccountController extends Controller
{
    public function index()
    {
        return view('myaccount.edit');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('myaccount.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [        
            'email' => 'nullable|string|email|max:255|unique:users,email,'.$user->id,
            'phone' => 'nullable|regex:/^[0-9]{10,12}$/|unique:users,phone,'.$user->id,
            'password' => 'nullable|string|min:6|confirmed'
        ]);
        if ($validator->fails()) {
            $request->flashExcept('password');
            return view('myaccount.index', compact('user'))->withErrors($validator);
        }
        $input = $request->only(['email','phone','password']);
        $input = array_filter($input, 'strlen');
        $user->fill($input)->save();
        return view('myaccount.edit')
            ->with('flash_message',[
                'message' => 'You changedd your account details',
                'title' => 'Success',
                'sender' => 'ACCOUNT',
                'type' => 'INFO'
            ]);
    }
}
