<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Terminal;
use Validator;

use Carbon\Carbon;
use App\Utilities\Calculator;

use Illuminate\Http\Request;
use App\Utilities\PerformanceData;

class RouteRepeatController extends Controller
{
    public function create()
    {
        $user = Auth::user();
        $terminals;
        if ($user->hasPermissionTo('Terminal Control - All Terminals')) {
            $terminals = Terminal::all();
        }
        else if ($user->hasPermissionTo('Terminal Control')) {
            $terminals = $user->terminals;
        }

        return view('map.routerepeat.create',compact('terminals'));
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $request['start_date'] = implode(",",$request->only('start_date'));
        $request['stop_date'] = implode(",",$request->only('stop_date'));
        $validator = Validator::make($request->all(), [
            'start_date' => 'required|date_format:"Y-m-d"',
            'start_time' => 'required|date_format:"H:i"',
            'end_date' => 'required|date_format:"Y-m-d"',
            'end_time' => 'required|date_format:"H:i"',
            'terminal' => 'required|exists:terminals,id'
        ]);

        if($validator->fails()) {
            $user = Auth::user();
            $terminals;
            if ($user->hasPermissionTo('Terminal Control - All Terminals')) {
                $terminals = Terminal::all();
            }
            else if ($user->hasPermissionTo('Terminal Control')) {
                $terminals = $user->terminals;
            }

            return view('map.routerepeat.create',compact('terminals'))
                ->withErrors($validator)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'REPORT',
                    'type' => 'ERROR'
                ]);
        }
        $terminals = $user->adminTerminals->whereIn('id',$request['terminal']);
        $input = $request->only('start_date', 'start_time', 'end_date', 'end_time');
        $input['summary'] = 0;
        $datamaker = new PerformanceData();
        $datamaker->setTerminals($terminals)->fill($input)->run();
        $data = $datamaker->getLocationData();

        return view('map.routerepeat.show')
            ->with('data', $data);
    }
}
