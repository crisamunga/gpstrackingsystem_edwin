<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use Auth;
use App\Utilities\PerformanceData;
use App\Terminal;

class MapTerminalPerformanceController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $terminals = $user->adminTerminals;
        return view('map.terminals.performance.create')
            ->with('terminals',$terminals);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $request['start_date'] = implode(",",$request->only('start_date'));
        $request['stop_date'] = implode(",",$request->only('stop_date'));
        $validator = Validator::make($request->all(), [
            'display' => [
                'required',
                Rule::in([0,1,2])
            ],
            'summary' => [
                'required',
                Rule::in([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14])
            ],
            'start_date' => 'required|date_format:"Y-m-d"',
            'start_time' => 'required|date_format:"H:i"',
            'end_date' => 'required|date_format:"Y-m-d"',
            'end_time' => 'required|date_format:"H:i"',
            'terminals' => 'required|exists:terminals,id',
        ]);
        if ($validator->fails()) {
            $terminals = $user->adminTerminals;
            $id = $request['reportid'];
            $terminals = $user->adminTerminals;
            return view('map.terminals.performance.create')
                ->with('terminals',$terminals)
                ->withErrors($validator)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'REPORT',
                    'type' => 'ERROR'
                ]);
        }
        $terminals = $user->adminTerminals->whereIn('id',$request['terminals']);
        $input = $request->only('summary', 'start_date', 'start_time', 'end_date', 'end_time');
        $datamaker = new PerformanceData();
        $datamaker->setTerminals($terminals)->fill($input)->run();
        $data = $datamaker->getLocationData();
        return view('map.showreport')->with('data', $data)->with('display', $request['display']);
    }
}