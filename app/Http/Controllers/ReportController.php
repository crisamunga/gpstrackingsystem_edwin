<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    public function index()
    {
        return view('home.reports.index');
    }

    public function dashboard()
    {
        return view('home.reports.dashboard');
    }

    public function export(Request $request)
    {
        $data = json_decode($request['data']);
        $title = $request['title'];
        $filename = "report.csv";

        return Excel::create($title, function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download();
    }
}
