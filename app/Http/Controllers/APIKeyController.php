<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class APIKeyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $json = Storage::disk('socketserver')->get('system.json');
        $config = json_decode($json, true);
        $apikeys = collect($config['api-keys']);

        return view('server.apikeys.index')->with('keys', $apikeys);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('server.apikeys.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valdiator = Validator::make($request->all(), [
            'name' => 'required|string|max:30',
            'key' => 'required|string|max:100'
        ]);

        if ($valdiator->fails()) {
            return view('server.apikeys.create')
                ->withErrors($valdiator->errors())
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'API',
                    'type' => 'ERROR'
                ]);
        }

        $json = Storage::disk('socketserver')->get('system.json');
        $config = json_decode($json, true);
        $apikeys = collect($config['api-keys']);

        $exists = $apikeys->where('name', $request['name'])->first();
        
        if (isset($exists)) {
            $apikeys = $apikeys->map(function($apikey) use ($request) { 
                if($apikey['name'] == $request['name']) {
                    $apikey['key'] = $request['key'];
                }
                return $apikey;
            });
        } else {
            $apikeys = $apikeys;
            $apikeys->push([
                'name' => $request['name'],
                'key' => $request['key'],
            ]);
        }

        $config['api-keys'] = $apikeys->all();
        $json = json_encode($config, JSON_PRETTY_PRINT);
        Storage::disk('socketserver')->put('system.json',$json);

        return view('server.apikeys.index')
            ->with('keys', $apikeys)
            ->with('flash_message',[
                'message' => 'You created an API key for '.$request['name'],
                'title' => 'Success',
                'sender' => 'API',
                'type' => 'INFO'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $json = Storage::disk('socketserver')->get('system.json');
        $config = json_decode($json, true);
        $apikeys = collect($config['api-keys']);
        $exists = $apikeys->where('name', $id)->first();

        if ($exists) {
            return view('server.apikeys.edit')->with('socket', $exists);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $json = Storage::disk('socketserver')->get('system.json');
        $config = json_decode($json, true);
        $apikeys = collect($config['api-keys']);
        $exists = $apikeys->where('name', $id)->first();

        if ($exists) {
            return view('server.apikeys.edit')->with('socket', $exists);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $valdiator = Validator::make($request->all(), [
            'name' => 'required|string|max:30',
            'key' => 'required|string|max:100'
        ]);

        if ($valdiator->fails()) {
            return view('server.apikeys.create')
                ->withErrors($valdiator->errors())
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'API',
                    'type' => 'ERROR'
                ]);
        }

        $json = Storage::disk('socketserver')->get('system.json');
        $config = json_decode($json, true);
        $apikeys = collect($config['api-keys']);

        $exists = $apikeys->where('name', $request['name'])->first();
        
        if (isset($exists)) {
            $apikeys = $apikeys->map(function($apikey) use ($request) { 
                if($apikey['name'] == $request['name']) {
                    $apikey['key'] = $request['key'];
                }
                return $apikey;
            });
        } else {
            $apikeys = $apikeys;
            $apikeys->push([
                'name' => $request['name'],
                'key' => $request['key'],
            ]);
        }

        $config['api-keys'] = $apikeys->all();
        $json = json_encode($config, JSON_PRETTY_PRINT);
        Storage::disk('socketserver')->put('system.json',$json);

        return view('server.apikeys.index')
            ->with('keys', $apikeys)
            ->with('flash_message',[
                'message' => 'You changed the API key for '.$id,
                'title' => 'Success',
                'sender' => 'API',
                'type' => 'INFO'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $json = Storage::disk('socketserver')->get('system.json');
        $config = json_decode($json, true);
        $apikeys = collect($config['api-keys']);

        $apikeys = $apikeys->map(function($apikey) use ($id) { 
            if($id != 'google-maps-api' && $apikey['name'] == $id) {
                return null;
            }
            return $apikey;
        })->filter();
        $config['api-keys'] = $apikeys->all();
        $json = json_encode($config, JSON_PRETTY_PRINT);
        Storage::disk('socketserver')->put('system.json',$json);

        return view('server.apikeys.index')
            ->with('keys', $apikeys)
            ->with('flash_message',[
                'message' => 'Successfully deleted the API key',
                'title' => 'Success',
                'sender' => 'API',
                'type' => 'INFO'
            ]);
    }
}
