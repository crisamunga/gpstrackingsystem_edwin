<?php

namespace App\Http\Controllers;

use Auth;
use Validator;

use App\Driver;
use App\User;
use App\Terminal;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $drivers = Auth::user()->drivers;

        return view('home.drivers.index',compact('drivers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $terminals = Auth::user()->terminals;

        return view('home.drivers.create')
            ->with('terminals',$terminals);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|unique:terminal_groups|max:100',
            'phone' => [
                'required',
                'regex:/((\+[0-9]{1,3})|(0))([0-9]{9})/'
            ],
            'email' => 'required',
            'vehicle' => 'nullable|exists:terminals,id'
        ]);
        if($validator->fails()) {
            $terminals = Auth::user()->terminals;
            return view('home.drivers.create')
                ->withErrors($validator)
                ->with('terminals',$terminals)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'DRIVER',
                    'type' => 'ERROR'
                ]);
        }

        $input = $request->only('name','phone','email');
        $driver = Driver::create($input);
        if (isset($request['vehicle']) && $request['vehicle'] > -1) {
            $terminal = Terminal::findOrFail($request['vehicle']);
            $driver->setTerminal($terminal);
        }
        $driver->owner()->associate(Auth::user());
        $driver->save();

        $drivers = Auth::user()->drivers;

        return view('home.drivers.index',compact('drivers'))
            ->with('flash_message',[
                'message' => 'You added driver ' . $request['name'],
                'title' => 'success',
                'sender' => 'DRIVER',
                'type' => 'INFO'
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Integer  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $driver = Auth::user()->drivers()->where('id',$id)->first();
        $terminals = Auth::user()->terminals;

        return view('home.drivers.edit')
            ->with('terminals', $terminals)
            ->with('driver', $driver);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Integer  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $driver = Auth::user()->drivers()->where('id',$id)->first();

        $validator = Validator::make($request->all(), [
            'name'=>'required|unique:terminal_groups|max:100',
            'phone' => [
                'required',
                'regex:/^((\+[0-9]{1,3})|(0))([0-9]{9})$/'
            ],
            'email' => 'required',
            'vehicle' => 'nullable|integer'
        ]);

        if($validator->fails()) {
            $terminals = Auth::user()->terminals;

            return view('home.drivers.edit')
                ->withErrors($validator)
                ->with('terminals', $terminals)
                ->with('driver', $driver)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'DRIVER',
                    'type' => 'ERROR'
                ]);
        }

        $input = $request->only('name','phone','email');
        $driver->fill($input)->save();

        $driver->removeTerminal();
        if (isset($request['vehicle']) && $request['vehicle'] > -1) {
            $terminal = Terminal::findOrFail($request['vehicle']);
            $driver->setTerminal($terminal);
        }

        $drivers = Auth::user()->drivers;

        return view('home.drivers.index',compact('drivers'))
        ->with('flash_message',[
            'message' => 'You edited driver ' . $driver->name,
            'title' => 'success',
            'sender' => 'DRIVER',
            'type' => 'INFO'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Integer  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $driver = Auth::user()->drivers()->where('id',$id)->get();
        $driver->delete();

        $drivers = $user->drivers;

        return view('home.drivers.index',compact('drivers'))
        ->with('flash_message',[
            'message' => 'You removed driver ' . $driver->name,
            'title' => 'success',
            'sender' => 'DRIVER',
            'type' => 'INFO'
        ]);
    }
}
