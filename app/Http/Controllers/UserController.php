<?php

namespace App\Http\Controllers;

use Auth;
use Validator;

use App\User;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();        
        return view('admin.users.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Auth::user()->roles;
        return view('admin.users.create', ['roles'=>$roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'middle_last_name' => 'required|string|max:255',            
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'required|regex:/^[0-9]{10,12}$/|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'email_token' => base64_encode($request['email'])
        ]);

        if($validator->fails()) {
            $request->flashExcept('password');
            $roles = Role::get();
            return view('admin.users.create', ['roles'=>$roles])
                ->withErrors($validator)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'USER',
                    'type' => 'ERROR'
                ]);
        }

        $user = new User();
        $user->fill($request->only('first_name','middle_last_name','email','phone','password'));
        $d = date('Y-m-d h:i:s');
        $user->lastdashboardcheck = $d;
        $user->save();
        $roles = $request['roles'];
        $m = '';
        if (isset($roles)) {            
            foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $role)->firstOrFail();     
                if ($role_r->name == 'Super Admin' && !Auth::user()->hasRole('Super Admin')) {
                    $m .= '\nYou cannot assign that permission to the user';
                    continue;
                }
                $user->assignRole($role_r); //Assigning role to user
            }
        }
        
        $users = User::all();
        //return Response::json($users);
        return view('admin.users.index')
            ->with('users', $users)
            ->with('flash_message',[
                'message' => 'You created user ' . $user->username,
                'title' => 'Success',
                'sender' => 'USER',
                'type' => 'INFO'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Auth::user()->hasRole('Super Admin') ? Role::all() : Auth::user()->roles;
        //return Response::json(compact('user', 'roles'));
        return view('admin.users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id); //Get user specified by id
        
        //Validate name, email  fields  
        $validator = Validator::make($request->all(), [
        //$this->validate($request, [
            'first_name' => 'nullable|string|max:255',
            'middle_last_name' => 'nullable|string|max:255',
            'email' => 'nullable|string|email|max:255|unique:users',
            'phone' => 'nullable|regex:/^[0-9]{10,12}$/|unique:users',
            'password' => 'nullable|string|min:6|confirmed'
        ]);
        if ($validator->fails()) {
            $request->flashExcept('password');            
            $roles = Role::get();
            //return Response::json(compact('user', 'roles'));
            return view('admin.users.edit', compact('user', 'roles'))
                ->withErrors($validator)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'USER',
                    'type' => 'ERROR'
                ]);
        }

        // A non superadmin cannot edit a superadmin
        if ($user->hasRole('Super Admin') && !Auth::user()->hasRole('Super Admin')) {
            $request->flashExcept('password');            
            $roles = Role::get();
            return view('admin.users.edit', compact('user', 'roles'))
                ->with('flash_message',[
                    'message' => 'You can\'t change that user\'s details',
                    'title' => 'Restricted action',
                    'sender' => 'USER',
                    'type' => 'ERROR'
                ]);
        }
        //Retreive the fields
        $input = $request->only(['first_name','middle_last_name','email','phone','password']); 
        $input = array_filter($input, 'strlen');
        //Retreive all roles
        $roles = $request['roles'];
        $user->fill($input)->save();
        //$user->save();

        if (isset($roles)) {        
            $user->roles()->sync($roles);  //If one or more role is selected associate user to roles          
        }        
        else {
            $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }
        $users = User::all();
        //return Response::json($users);
        return view('admin.users.index')
            ->with('users', $users)
            ->with('flash_message',[
                'message' => 'You modified user ' . $user->username,
                'title' => 'Success',
                'sender' => 'USER',
                'type' => 'INFO'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find a user with a given id and delete
        $user = User::findOrFail($id);
        if ($user->hasRole('Super Admin') && !Auth::user()->hasRole('Super Admin')) {
            $users = User::all();
            return view('admin.users.index')
                ->with('users', $users)
                ->with('flash_message',[
                    'message' => 'You can\'t delete that user',
                    'title' => 'Restricted action',
                    'sender' => 'USER',
                    'type' => 'ERROR'
                ]);
        }
        if ($user->hasRole('Admin') && !Auth::user()->hasRole('Admin')) {
            $users = User::all();
            return view('admin.users.index')
                ->with('users', $users)
                ->with('flash_message',[
                    'message' => 'You can\'t delete that user',
                    'title' => 'Restricted action',
                    'sender' => 'USER',
                    'type' => 'ERROR'
                ]);
        }
        $user->delete();
        $users = User::all();
        //return Response::json($users);
        return view('admin.users.index')
            ->with('users', $users)
            ->with('flash_message',[
                'message' => 'You deleted user ' . $user->username,
                'title' => 'Success',
                'sender' => 'USER',
                'type' => 'INFO'
            ]);
    }
}
