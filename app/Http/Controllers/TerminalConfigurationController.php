<?php

namespace App\Http\Controllers;

use App\TerminalConfiguration;
use Illuminate\Http\Request;

class TerminalConfigurationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TerminalConfiguration  $terminalConfiguration
     * @return \Illuminate\Http\Response
     */
    public function show(TerminalConfiguration $terminalConfiguration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TerminalConfiguration  $terminalConfiguration
     * @return \Illuminate\Http\Response
     */
    public function edit(TerminalConfiguration $terminalConfiguration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TerminalConfiguration  $terminalConfiguration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TerminalConfiguration $terminalConfiguration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TerminalConfiguration  $terminalConfiguration
     * @return \Illuminate\Http\Response
     */
    public function destroy(TerminalConfiguration $terminalConfiguration)
    {
        //
    }
}
