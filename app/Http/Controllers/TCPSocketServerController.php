<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\TcpServerConnections;
use App\Events\NewTerminalEvent;
use Log;

class TCPSocketServerController extends Controller
{
    public function terminate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sockets' => 'nullable|exists:tcp_server_connections,name'
        ]);

        if ($validator->fails()) {
            return response()->json(['msg' => 'There were errors in your input', 'errors' => $validator->errors()], 422);
        }
        $socketnames = $request['sockets'];
        if ($socketnames != null && count($socketnames) > 0) {
            $sockets = TcpServerConnections::whereIn('name',$socketnames)->with('terminal')->get();
            foreach ($sockets as $connection) {
                if ($connection->terminal != null) {
                    $data = [
                        'imei' => $connection->imei,
                        "type" => "Logout",
                        "description" => "Now offline",
                    ];
                    event(new NewTerminalEvent($data['imei'],$data));
                }
            }
            TcpServerConnections::whereIn('name',$socketnames)->delete();
        }
        return response()->json(['msg' => 'Server was deregistered successfully'], 200);
    }
}