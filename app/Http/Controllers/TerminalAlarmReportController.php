<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Auth;
use Carbon\Carbon;
use App\User;
use App\Terminal;
use App\TerminalEventType;
use App\Utilities\Chart;
use App\Utilities\EventData;
use \Lava;
use Khill\Lavacharts\Lavacharts;

class TerminalAlarmReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.reports.terminals.alarms.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = uniqid();
        $user = Auth::user();
        $terminals = $user->adminTerminals;
        $alarms = TerminalEventType::whereIn('type',['alarm','soft alarm'])->get();
        return view('home.reports.terminals.alarms.create')
            ->with('terminals',$terminals)
            ->with('alarms',$alarms)
            ->with('id',$id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $request['start_date'] = implode(",",$request->only('start_date'));
        $request['stop_date'] = implode(",",$request->only('stop_date'));
        $id = $request['reportid'];
        $validator = Validator::make($request->all(), [
            'data-source' => 'required|exists:terminal_event_types,id',
            'summary' => [
                'required',
                Rule::in([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14])
            ],
            'trendchart' => [
                'required',
                Rule::in([0,1,2,3,4,5])
            ],
            'start_date' => 'required|date_format:"Y-m-d"',
            'start_time' => 'required|date_format:"H:i"',
            'end_date' => 'required|date_format:"Y-m-d"',
            'end_time' => 'required|date_format:"H:i"',
            'terminals' => 'required|exists:terminals,id',
            'reportid' => 'required'
        ]);

        if ($validator->fails()) {
            $terminals = $user->adminTerminals;
            $alarms = TerminalEventType::whereIn('type',['alarm','soft alarm'])->get();
            return view('home.reports.terminals.alarms.create')
                ->with('terminals',$terminals)
                ->with('alarms',$alarms)
                ->with('id',$id);
        }

        $input = $request->only('summary', 'start_date', 'start_time', 'end_date', 'end_time');
        $terminals = $user->adminTerminals->whereIn('id',$request['terminals']);
        $types = TerminalEventType::whereIn('id',$request['data-source'])->get();
        $datamaker = new EventData();

        $datamaker->setTerminals($terminals)->settypes($types)->fill($input)->run();

        if ($datamaker->data->isEmpty()) {
            $id = $request['reportid'];
            $startdate = Carbon::parse($request['start_date'].' '.$request['start_time'])->formatLocalized('%A %d %B %Y');
            $enddate = Carbon::parse($request['end_date'].' '.$request['end_time'])->formatLocalized('%A %d %B %Y');
            $events = implode(', ',array_values($types->pluck('description','keyword')->toArray()));
            $title = $events.' report between '.$startdate.' and '.$enddate;
            return view('home.reports.noreport')
                ->with('id',$id)
                ->with('title',$title);
        }

        $terminaltrenddata = $datamaker->getChartData('terminals');
        $terminaltrenddt = Lava::DataTable();
        $terminaltrenddt->addColumns($terminaltrenddata['coldefs'])
                ->addRows($terminaltrenddata['data']);

        $keywordtrenddata = $datamaker->getChartData('events');
        $keywordtrenddt = Lava::DataTable();
        $keywordtrenddt->addColumns($keywordtrenddata['coldefs'])
                ->addRows($keywordtrenddata['data']);

        $timesummarydata = $datamaker->getChartSummaryData('datetime');
        $timesummarydt = Lava::DataTable();
            $timesummarydt->addColumns($timesummarydata['coldefs'])
                    ->addRows($timesummarydata['data']);

        $terminalsummarydata = $datamaker->getChartSummaryData('terminal');
        $terminalsummarydt = Lava::DataTable();
        $terminalsummarydt->addColumns($terminalsummarydata['coldefs'])
                ->addRows($terminalsummarydata['data']);

        $eventsummarydata = $datamaker->getChartSummaryData('event');
        $eventsummarydt = Lava::DataTable();
        $eventsummarydt->addColumns($eventsummarydata['coldefs'])
                ->addRows($eventsummarydata['data']);

        $xlsdata = $datamaker->getXLSData();

        $charttype = Chart::chooseChart($request['trendchart']);
        $startdate = Carbon::parse($request['start_date'].' '.$request['start_time'])->formatLocalized('%A %d %B %Y');
        $enddate = Carbon::parse($request['end_date'].' '.$request['end_time'])->formatLocalized('%A %d %B %Y');
        $events = implode(', ',array_values($types->pluck('description','keyword')->toArray()));
        $title = $events.' report between '.$startdate.' and '.$enddate;
        $options = [
            'legend' => [
                'position' => 'top'
            ],
            'hAxis' => [
                'title' => 'Date',
                'gridlines' => [
                    "count" => "-1",
                    "units" => [
                      "days" => "{format: ['MMM dd']}",
                      "hours" => "{format: ['HH:mm', 'ha']}",
                    ]
                ],
            ],
            'vAxis' => [
                'title' => 'Alarm frequency'
            ]
        ];

        Chart::PrepareChart($charttype, $id.'-terminals', $terminaltrenddt, $options);
        Chart::PrepareChart($charttype, $id.'-events', $keywordtrenddt, $options);
        Chart::PrepareChart('PieChart', $id.'-time', $timesummarydt, $options);
        Chart::PrepareChart('DonutChart', $id.'-terminal', $terminalsummarydt, $options);
        Chart::PrepareChart('PieChart', $id.'-event', $eventsummarydt, $options);

        return view('home.reports.terminals.alarms.show')
                ->with('id', $id)
                ->with('charttype', $charttype)
                ->with('title', $title)
                ->with('data', $xlsdata);
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
