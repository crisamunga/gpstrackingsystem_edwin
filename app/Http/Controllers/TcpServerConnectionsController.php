<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TcpServerConnections;
use App\DataLogEntry;
use App\Events\NewTerminalData;
use App\Events\NewTerminalEvent;
use App\TerminalEventEntry;
use Validator;
use App\Utilities\GPSProtocolParser;
use App\Utilities\Calculator;
use Log;
use Illuminate\Database\QueryException;

class TcpServerConnectionsController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|regex:/^[0-9]{7,17}$/'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'There were errors in the input',
                'errors' => $validator->errors()
            ], 422);
        }

        $input = $request->only('name');
        $connection = TcpServerConnections::create($input);

        return response()->json([
            'message' => 'Socket added',
        ], 200);
    }

    public function update(Request $request, $name)
    {
        $connection = TcpServerConnections::where('name', $name)->first();

        if ($connection == null) {
            return response()->json([
                'message' => 'That socket does not exist',
            ], 404);
        }

        $validator = Validator::make($request->all(), [
            'message' => 'required|string',
            'protocol' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'There were errors in the input',
                'errors' => $validator->errors()
            ], 422);
        }
        $distance = 0;
        $input = $request->only('message','protocol');
        $data = GPSProtocolParser::Parse($input['message'], $input['protocol']);

        // Exit early if data is invalid
        if (count($data) == 0) {
            return response()->json([
                'message' => 'Possible invalid protocol provided',
            ], 422);
        }

        // Compute new save data
        if (count($data['data']) > 0) {
            $newdata = $data['data'];
            $olddata = $connection->data;
            if ($connection->data != null) {
                $duration = array_key_exists('time',$olddata) ? Calculator::getTimeDuration($olddata['time'],$newdata['time']) : 0;
                if($newdata['gpsSignalPresent'] && $olddata['gpsSignalPresent'])
                {
                    $distance = Calculator::getDistance($olddata['latitude'],$olddata['longitude'],$newdata['latitude'],$newdata['longitude']);
                    $newdata['distance'] = array_key_exists('distance', $olddata) ? $olddata['distance'] + $distance : $distance;
                    $newdata['speed'] = ($duration > 0 ) ? $distance / $duration : 0;
                } else {
                    $newdata['distance'] = array_key_exists('distance', $olddata) ? $olddata['distance'] + $newdata['speed'] * $duration : + $newdata['speed'] * $duration;
                }

                $newdata['oil1'] = array_key_exists('oil1', $olddata) ? $olddata['oil1'] + $olddata['oilPercent1'] - $newdata['oilPercent1'] : $olddata['oilPercent1'] - $newdata['oilPercent1'];
                $newdata['oil2'] = array_key_exists('oil2', $olddata) ? $olddata['oil2'] + $olddata['oilPercent2'] - $newdata['oilPercent2'] : $olddata['oilPercent2'] - $newdata['oilPercent2'];

                $newdata['workingtime'] = 0;
                
                if ($olddata['accState'] && $newdata['accState']) {
                    if (array_key_exists('workingtime', $olddata)) {
                        $newdata['workingtime'] = $olddata['workingtime'] + $duration;
                    } else {
                        $newdata['workingtime'] = $duration;
                    }
                }

                if(array_key_exists('lastsavetime', $olddata)) {
                    $newdata['lastsavetime'] = $olddata['lastsavetime'];
                } else if (array_key_exists('time',$newdata)) {
                    $newdata['lastsavetime'] = $newdata['time'];
                }

                $durationSinceLastSave = array_key_exists('lastsavetime',$olddata) ? Calculator::getTimeDurationInSeconds($olddata['lastsavetime'],$newdata['time']) : 0;
                if ($durationSinceLastSave > config('appsettings.save_frequency') || config('appsettings.save_frequency') == 0 ) {
                    try {
                        DataLogEntry::create([
                            'driver_id' => ($connection->terminal->driver != null) ? $connection->terminal->driver->id : null,
                            'imei' => $data['imei'],
                            'latitude' => $newdata['latitude'],
                            'longitude' => $newdata['longitude'],
                            'altitude' => $newdata['altitude'],
                            'distance' => $newdata['distance'],
                            'speed' => $newdata['speed'],
                            'oil1' => $newdata['oil1'],
                            'oil2' => $newdata['oil2'],
                            'temperature' => $newdata['temperature'],
                            'workingtime' => $newdata['workingtime'],
                            'currenttime' => $newdata['time']
                        ]);
                    } catch (QueryException $excception) {
                        Log::error("Unable to log incoming data", [
                                'protocol' => $input['protocol'],
                                'message' => $input['message'],
                                'details' => $exception->errorInfo
                            ]);
                    }
                    $newdata['workingtime'] = $newdata['oil2'] = $newdata['oil1'] = $newdata['distance'] = 0;
                    $newdata['lastsavetime'] = $newdata['time'];
                }
            } else {
                if (count($newdata) > 0) {
                    $newdata['workingtime'] = $newdata['oil2'] = $newdata['oil1'] = $newdata['distance'] = 0;
                }
            }

            // Record events of interest
            if ($data['type'] != "location data" && $data['type'] != "Hearbeat") {
                if ($data['type'] != "Login" && $data['type'] != "Logout") {
                    TerminalEventEntry::create([
                        'imei' => $data['imei'],
                        'driver_id' => ($connection->terminal->driver != null) ? $connection->terminal->driver->id : null,
                        'keyword' => $newdata['keyword'],
                        'latitude' => $newdata['latitude'],
                        'longitude' => $newdata['longitude'],
                        'currenttime' => $newdata['time']
                    ]);
                }
            }

            $connection->data = $newdata;
            event(new NewTerminalData($data['imei'],$data));
        }
        if ($data['type'] != "location data" && $data['type'] != "Heartbeat") {
            event(new NewTerminalEvent($data['imei'],$data));
        }
        // Update save data
        if (count($data) > 0) {
            if ($connection->imei != $data['imei']) {
                TcpServerConnections::where('imei',$data['imei'])->delete();
                $connection->imei = $data['imei'];
            }
            $connection->save();
        }
        $data['data']['distance'] = $distance;

        return response()->json([
            'message' => 'Data received',
            'response' => $data['response']
        ], 200);
    }

    public function destroy($name)
    {
        $connection = TcpServerConnections::where('name', $name)->first();

        if ($connection == null) {
            return response()->json([
                'message' => 'That socket does not exist',
            ], 404);
        }

        if ($connection->terminal != null) {
            $data = [
                'imei' => $connection->imei,
                "type" => "Logout",
                "description" => "Now offline",
            ];
            event(new NewTerminalEvent($data['imei'],$data));
        }

        $connection->delete();
        return response()->json([
            'message' => 'Socket removed',
        ], 200);
    }
}
