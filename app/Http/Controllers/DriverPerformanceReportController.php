<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Auth;
use Carbon\Carbon;
use App\User;
use App\Driver;
use App\Utilities\Chart;
use App\Utilities\DriverPerformanceData;
use \Lava;
use Khill\Lavacharts\Lavacharts;

class DriverPerformanceReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.reports.drivers.performance.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = uniqid();
        $user = Auth::user();
        $drivers = $user->drivers;
        return view('home.reports.drivers.performance.create')
            ->with('drivers',$drivers)
            ->with('id',$id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $request['start_date'] = implode(",",$request->only('start_date'));
        $request['stop_date'] = implode(",",$request->only('stop_date'));
        $validator = Validator::make($request->all(), [
            'data-source' => [
                'required',
                Rule::in([0,1,2,3,4,5])
            ],
            'summary' => [
                'required',
                Rule::in([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14])
            ],
            'trendchart' => [
                'required',
                Rule::in([0,1,2,3,4,5])
            ],
            'start_date' => 'required|date_format:"Y-m-d"',
            'start_time' => 'required|date_format:"H:i"',
            'end_date' => 'required|date_format:"Y-m-d"',
            'end_time' => 'required|date_format:"H:i"',
            'drivers' => 'required|exists:drivers,id',
            'reportid' => 'required'
        ]);

        if ($validator->fails()) {
            $drivers = $user->drivers;
            $id = $request['reportid'];
            return view('home.reports.drivers.performance.create')
                ->with('id',$id)
                ->with('drivers',$drivers)
                ->withErrors($validator)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'REPORT',
                    'type' => 'ERROR'
                ]);
        }

        $input = $request->only('summary', 'start_date', 'start_time', 'end_date', 'end_time');
        $drivers = $user->drivers->whereIn('id',$request['drivers']);
        $datamaker = new DriverPerformanceData();
        $datamaker->setdrivers($drivers)->fill($input)->run();

        $fields = [
            'distance', 'speed', 'oil1', 'oil2', 'temperature', 'workingtime'
        ];

        if ($datamaker->data->isEmpty()) {
            $id = $request['reportid'];
            $startdate = Carbon::parse($request['start_date'].' '.$request['start_time'])->formatLocalized('%A %d %B %Y');
            $enddate = Carbon::parse($request['end_date'].' '.$request['end_time'])->formatLocalized('%A %d %B %Y');
            $title = ucfirst('Driver '.$fields[$request['data-source']]).' report between '.$startdate.' and '.$enddate;
            return view('home.reports.noreport')
                ->with('id',$id)
                ->with('title',$title);
        }
        
        $field = $fields[$request['data-source']];
        $trenddata = $datamaker->getChartData($field);
        $xlsdata = $datamaker->getXLSData();
        
        $trenddt = Lava::DataTable();
        $trenddt->addColumns($trenddata['coldefs'])
                ->addRows($trenddata['data']);
        
        $timesummarydata = $datamaker->getChartSummaryData('datetime', $field);
        $timesummarydt = Lava::DataTable();
        $timesummarydt->addColumns($timesummarydata['coldefs'])
                ->addRows($timesummarydata['data']);

        $driversummarydata = $datamaker->getChartSummaryData('driver', $field);
        $driversummarydt = Lava::DataTable();
        $driversummarydt->addColumns($driversummarydata['coldefs'])
                ->addRows($driversummarydata['data']);
        
        $charttype = Chart::chooseChart($request['trendchart']);
        $startdate = Carbon::parse($request['start_date'].' '.$request['start_time'])->formatLocalized('%A %d %B %Y');
        $enddate = Carbon::parse($request['end_date'].' '.$request['end_time'])->formatLocalized('%A %d %B %Y');
        $title = ucfirst($fields[$request['data-source']]).' report between '.$startdate.' and '.$enddate;
        $options = [
            'legend' => [
                'position' => 'top'
            ],
            'hAxis' => [
                'title' => 'Date',
            ],
            'vAxis' => [
                'title' => ucfirst($fields[$request['data-source']])
            ]
        ];

        $id = $request['reportid'];

        Chart::PrepareChart($charttype, $id, $trenddt, $options);
        Chart::PrepareChart('PieChart', $id, $timesummarydt, $options);
        Chart::PrepareChart('DonutChart', $id, $driversummarydt, $options);

        return view('home.reports.drivers.performance.show')
                ->with('id', $id)
                ->with('charttype', $charttype)
                ->with('title', $title)
                ->with('data', $xlsdata);
    }
}
