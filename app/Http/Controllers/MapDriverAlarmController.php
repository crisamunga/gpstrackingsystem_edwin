<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use Auth;
use App\Utilities\DriverEventData;
use App\Terminal;
use App\Driver;
use App\TerminalEventType;

class MapDriverAlarmController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $drivers = $user->drivers;
        $alarms = TerminalEventType::whereIn('type',['alarm','soft alarm'])->get();
        return view('map.drivers.alarms.create')
            ->with('drivers',$drivers)
            ->with('alarms',$alarms);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $request['start_date'] = implode(",",$request->only('start_date'));
        $request['stop_date'] = implode(",",$request->only('stop_date'));
        $validator = Validator::make($request->all(), [
            'display' => [
                'required',
                Rule::in([0,1,2])
            ],
            'summary' => [
                'required',
                Rule::in([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14])
            ],
            'start_date' => 'required|date_format:"Y-m-d"',
            'start_time' => 'required|date_format:"H:i"',
            'end_date' => 'required|date_format:"Y-m-d"',
            'end_time' => 'required|date_format:"H:i"',
            'drivers' => 'required|exists:drivers,id',
        ]);
        if ($validator->fails()) {
            $alarms = TerminalEventType::whereIn('type',['alarm','soft alarm'])->get();
            $id = $request['reportid'];
            $drivers = $user->drivers;
            return view('map.drivers.alarms.create')
                ->with('drivers',$drivers)
                ->with('alarms',$alarms)
                ->withErrors($validator)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'REPORT',
                    'type' => 'ERROR'
                ]);
        }
        $drivers = $user->drivers->whereIn('id',$request['drivers']);
        $alarms = TerminalEventType::whereIn('id',$request['alarms'])->get();
        $input = $request->only('summary', 'start_date', 'start_time', 'end_date', 'end_time');
        $datamaker = new DriverEventData();
        $datamaker->setdrivers($drivers)->settypes($alarms)->fill($input)->run();
        $data = $datamaker->getLocationData();
        return view('map.showreport')->with('data', $data)->with('display', $request['display']);
    }
}
