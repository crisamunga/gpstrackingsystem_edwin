<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::all();
        return view('admin.permissions.index')->with('permissions', $permissions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();
        return view('admin.permissions.create')->with('roles', $roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string|max:40',
        ]);

        if($validator->fails()) {            
            $roles = Role::get();            
            return view('admin.permissions.create')
                ->with('roles', $roles)
                ->withErrors($validator)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'PERMISSION',
                    'type' => 'ERROR'
                ]);
        }

        $name = $request['name'];
        $permission = new Permission();
        $permission->name = $name;

        $roles = $request['roles'];
        $permission->save();

        if (!empty($request['roles'])) { //If one or more role is selected
            foreach ($roles as $role) {
                $r = Role::where('id', '=', $role)->firstOrFail(); //Match input role to db record

                $permission = Permission::where('name', '=', $name)->first(); //Match input //permission to db record
                $r->givePermissionTo($permission);
            }
        }

        $permissions = Permission::all();
        return view('admin.permissions.index')
            ->with('permissions', $permissions)
            ->with('flash_message',[
                'message' => 'You created permission '.$permission->name,
                'title' => 'Success',
                'sender' => 'PERMISSION',
                'type' => 'INFO'
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::findOrFail($id);
        return view('admin.permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $permission = Permission::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'name'=>'required|max:40',
        ]);
        if($validator->fails()) {
            $request->flash();
            return view('admin.permissions.edit', compact('permission'))
                ->withErrors($validator)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'PERMISSION',
                    'type' => 'ERROR'
                ]);
        }
        $input = $request->all();
        $permission->fill($input)->save();

        $permissions = Permission::all();        
        return view('admin.permissions.index')
            ->with('permissions', $permissions)
            ->with('flash_message',[
                'message' => 'You modified permission '.$permission->name,
                'title' => 'Success',
                'sender' => 'PERMISSION',
                'type' => 'INFO'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);
        if ($permission->name == "Administer roles and permissions" || $permission->name == "Access Admin Panel") {            
            $permissions = Permission::all();
            return view('admin.permissions.index', compact('permissions'))
            ->with('flash_message',[
                'message' => 'You can\'t delete that permission',
                'title' => 'Restricted action',
                'sender' => 'PERMISSION',
                'type' => 'ERROR'
            ]);
        }
        $permission->delete();
        $permissions = Permission::all();
        return view('admin.permissions.index')
            ->with('permissions', $permissions)
            ->with('flash_message',[
                'message' => 'You deleted permission '.$permission->name,
                'title' => 'Success',
                'sender' => 'PERMISSION',
                'type' => 'INFO'
            ]);
    }
}