<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('admin.roles.index')->with('roles', $roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();
        return view('admin.roles.create', ['permissions'=>$permissions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|unique:roles|max:30',
            'permissions' =>'required',
        ]);

        if($validator->fails()) {                        
            $permissions = Permission::all();
            return view('admin.roles.create', ['permissions'=>$permissions])
                ->withErrors($validator)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'UGROUP',
                    'type' => 'ERROR'
                ]);
        }
        
        $name = $request['name'];
        $role = new Role();
        $role->name = $name;

        $permissions = $request['permissions'];

        $role->save();

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail(); 
            //Fetch the newly created role and assign permission
            $role = Role::where('name', '=', $name)->first(); 
            $role->givePermissionTo($p);
        }
        $roles = Role::all();
        return view('admin.roles.index')
            ->with('roles', $roles)
            ->with('flash_message',[
                'message' => 'You created user group ' . $role->name,
                'title' => 'Success',
                'sender' => 'UGROUP',
                'type' => 'INFO'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('roles');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);        
        $permissions = Permission::all();
        return view('admin.roles.edit', compact('role','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'name'=>'max:30|unique:roles,name,'.$id,
            'permissions' =>'required',
        ]);

        if($validator->fails()) {            
            $role = Role::findOrFail($id);
            $permissions_all = Permission::all();
            return view('admin.roles.edit', compact('role'))
                ->with('permissions',$permissions_all)    
                ->withErrors($validator)                
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'UGROUP',
                    'type' => 'ERROR'
                ]);
        }

        $input = $request->except(['permissions']);
        $input = array_filter($input, 'strlen');
        $permissions = $request['permissions'];
        $role->fill($input)->save();

        $p_all = Permission::all();//Get all permissions
            
        foreach ($p_all as $p) {
            $role->revokePermissionTo($p); //Remove all permissions associated with role
        }

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail(); //Get corresponding form //permission in db
            $role->givePermissionTo($p);  //Assign permission to role
        }

        $roles = Role::all();
        return view('admin.roles.index')->with('roles', $roles)
            ->with('flash_message',[
                'message' => 'You modified user group ' . $role->name,
                'title' => 'Success',
                'sender' => 'UGROUP',
                'type' => 'INFO'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        if($role->name == "Client" || $role->name == "Admin" || $role->name == "Super Admin") 
        {
            $roles = Role::all();
            return view('admin.roles.index')->with('roles', $roles)
                ->with('flash_message',[
                    'message' => 'You cannot delete that user group',
                    'title' => 'Restricted action',
                    'sender' => 'UGROUP',
                    'type' => 'ERROR'
                ]);
        }

        $roles = Role::all();
        return view('admin.roles.index')->with('roles', $roles)
            ->with('flash_message',[
                'message' => 'You deleted user group ' . $role->name,
                'title' => 'Success',
                'sender' => 'UGROUP',
                'type' => 'INFO'
            ]);
    }
}
