<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Terminal;

class User extends Authenticatable
{
    use Notifiable;
    use HasROles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'middle_last_name', 'email', 'phone', 'password','email_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'verified', 'email_token'
    ];    

    public function setPasswordAttribute($password)
    {   
        $this->attributes['password'] = bcrypt($password);
    }

    public function getUsernameAttribute()
    {
        return $this->first_name." ".$this->middle_last_name;
    }

    public function getAdminTerminalsAttribute()
    {
        $terminals;
        if ($this->hasPermissionTo('Terminal Control - All Terminals')) {
            $terminals = Terminal::all();
        }
        else if ($this->hasPermissionTo('Terminal Control')) {
            $terminals = $this->terminals;
        }
        
        return $terminals;
    }

    public function terminals() 
    {
        return $this->hasMany('App\Terminal','client_id');
    }

    public function terminal_groups()
    {
        return $this->hasMany('App\TerminalGroup','user_id');
    }

    public function terminal_config_logs() 
    {
        return $this->hasMany('App\TerminalConfigLog','user_id');
    }

    public function terminal_access_logs() 
    {
        return $this->hasMany('App\TerminalAccessLog','user_id');
    }

    public function owns_terminal($terminal)
    {
        return $this->terminals->contains($terminal);
    }

    public function drivers() 
    {
        return $this->hasMany('App\Driver','owner_id');
    }

    public function map_auth_keys()
    {
        return $this->hasMany('App\MapAuthKey','user_id');
    }
}
