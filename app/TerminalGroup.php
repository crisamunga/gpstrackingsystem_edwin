<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TerminalGroup extends Model
{
    protected $fillable = ['name'];

    public function terminals() 
    {
        return $this->hasMany('App\Terminal','terminal_group_id');
    }

    public function user() 
    {
        return $this->belongsTo('App\User','user_id');
    }
}
