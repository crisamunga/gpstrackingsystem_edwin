var map;
var route;
var infowindow;
var markerlist = [];
var geocoder;
var heatmap;
var circle;
var rectangle;

function initMap() {
    var mapelement = document.getElementById('map');
    var myLatLng = new google.maps.LatLng(-1, 37);
    map = new google.maps.Map(mapelement, {
        center: myLatLng,
        zoom: 8
    });

    infowindow = new google.maps.InfoWindow({
        content: null
    });

    geocoder = new google.maps.Geocoder;
}

function findLocation(latitude, longitude)
{
    var pos = new google.maps.LatLng(latitude, longitude);
    geocodeLatLng(pos, function (location) {
        if (location) {
            var marker = new google.maps.Marker({
                position: pos,
                map: map,
                animation: google.maps.Animation.DROP,
                icon: icons.PURPLE
            });
            map.setCenter(pos);
            map.setZoom(9);
            var content = '' +
            '<div id="iw-container">' +
                '<div class="iw-title"> Location found  </div>' +
                '<div class="iw-content">' +
                    '<div class="iw-subTitle">Location coordinates</div>' +
                    '<div><b>Latitude</b> ' + latitude + '</div>' +
                    '<div><b>Longitude</b> ' + longitude + '</div>' +
                    '<div class="iw-subTitle">Location description</div>' +
                    '<div>' + location + '</div>' +
                '</div>' +
            '</div>';
            infowindow.setContent(content);
            infowindow.open(map, marker);

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.setContent(content);
                infowindow.open(map,marker);
            });

            markerlist.push(marker);
        }
    });
}

function drawPolyLine(data)
{
    var path = [];
    for (let i = 0; i < data.length; i++) {
        const coordinate = data[i];
        path.push({lat: coordinate.latitude, lng: coordinate.longitude});
    }
    route = new google.maps.Polyline({
        path: path,
        geodesic: true,
        strokeColor: 'brown',
        strokeOpacity: 1.0,
        strokeWeight: 2
    });
    route.setMap(map);
}

function removePolyline()
{
    if (route) {
        route.setMap(null);
        route = null;
    }
}

function geocodeLatLng(latlng, callback) {
    if(!geocoder || !latlng) return "Unknown location";

    //Method to actually do the check from google maps api
    geocoder.geocode({ "location": latlng }, function (results, status) {
        if (status === "OK") { // Check if searched returned successfully
            if (results[0]) { // check if first element of results is non-null
                callback (results[0].formatted_address);
            } else {
                callback ("Unknown location");
            }
        } else {
            console.log("Geocoder failed due to: " + status);
            callback ("Unknown location");
        }
    });
}

function showHeatMap(data)
{
    var cleandata = [];
    for (let i = 0; i < data.length; i++) {
        const coordinate = data[i];
        cleandata.push(new google.maps.LatLng(coordinate.latitude, coordinate.longitude));
    }
    heatmap = new google.maps.visualization.HeatmapLayer({
        data: cleandata,
        map: map
    });
    var gradient = [
        'rgba(0, 255, 255, 0)',
        'rgba(0, 255, 255, 1)',
        'rgba(0, 191, 255, 1)',
        'rgba(0, 127, 255, 1)',
        'rgba(0, 63, 255, 1)',
        'rgba(0, 0, 255, 1)',
        'rgba(0, 0, 223, 1)',
        'rgba(0, 0, 191, 1)',
        'rgba(0, 0, 159, 1)',
        'rgba(0, 0, 127, 1)',
        'rgba(63, 0, 91, 1)',
        'rgba(127, 0, 63, 1)',
        'rgba(191, 0, 31, 1)',
        'rgba(255, 0, 0, 1)'
    ];
    heatmap.set('gradient', gradient);
    heatmap.set('opacity',0.5);
}

function removeHeatMap()
{
    if (heatmap) {
        heatmap.setMap(null);
        heatmap = null;
    }
}

function showMarkers(data)
{
    for (let i = 0; i < data.length; i++) {
        const element = data[i];
        var pos = new google.maps.LatLng(element.latitude, element.longitude);
        var marker = new google.maps.Marker({
            position: pos,
            map: map,
        });
        markerlist.push(marker);
    }
}

function removeMarkers()
{
    for (let i = 0; i < markerlist.length; i++) {
        var marker = markerlist[i];
        marker.setMap(null);
    }
    markerlist = [];
}

function resetMap()
{
    removeHeatMap();
    removePolyline();
    removeMarkers();
    removeCircle();
    removeRectangle();
}

$('#search-form').submit( function (e) {
    e.preventDefault();
    var latitude = $('#search-latitude').val();
    var longitude = $('#search-longitude').val();
    findLocation(latitude, longitude);
});

$('#clear-map').click(function (e) {  
    e.preventDefault();
    resetMap();
});

function drawCircle(data)
{
    circle = new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        map: map,
        center: data.center,
        radius: data.radius
    });
}

function removeCircle() {
    if (circle) {
        circle.setMap(null);
    }
    circle = null;
}

function drawRectangle(data)
{
    rectangle = new google.maps.Rectangle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        map: map,
        bounds: {
          north: data.north,
          south: data.south,
          east: -data.east,
          west: -data.west
        }
    });
}

function removeRectangle() {
    if (rectangle) {
        rectangle.setMap(null);
    }
    rectangle == null;
}