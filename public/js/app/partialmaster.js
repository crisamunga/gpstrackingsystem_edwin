$(init)

function init() {    
    $('div.partial-container').each(function (index) {        
        partialcontainers[$(this).data('name')] = '#'+this.id;            
    });

    $('div.partial-container.auto-load').each(function (index) {
        var state = $(this).data('state');
        if (state == 'done') {
            return;
        }
        var url = $(this).data('source');            
        var container = $(this);
        $.get(url, function(response) {        
            container.html(response);            
        });
        $(this).data('state','done');
    });   
}