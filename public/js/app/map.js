var terminals;
var map;
var route;
var infowindow;
var markerlist = [];
var geocoder = null;
var heatmap;
var routeRepeat;

function initMap() {
    var mapelement = document.getElementById('map');
    var myLatLng = new google.maps.LatLng(-1, 37);
    map = new google.maps.Map(mapelement, {
        center: myLatLng,
        zoom: 8
    });

    infowindow = new google.maps.InfoWindow({
        content: null
    });

    geocoder = new google.maps.Geocoder;

    /*
    * The google.maps.event.addListener() event waits for
    * the creation of the infowindow HTML structure 'domready'
    * and before the opening of the infowindow defined styles
    * are applied.
    */
    google.maps.event.addListener(infowindow, 'domready', function() {
        // Reference to the DIV which receives the contents of the infowindow using jQuery
        
        // Reference to the DIV which receives the contents of the infowindow using jQuery
        var iwOuter = $('.gm-style-iw');

        /* The DIV we want to change is above the .gm-style-iw DIV.
            * So, we use jQuery and create a iwBackground variable,
            * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
            */
        var iwBackground = iwOuter.prev();

        // Remove the background shadow DIV
        iwBackground.children(':nth-child(2)').css({'display' : 'none'});

        // Remove the white background DIV
        iwBackground.children(':nth-child(4)').css({'display' : 'none'});

        iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6', 'z-index': '1'});

        var iwCloseBtn = iwOuter.next();

        // Apply the desired effect to the close button
        iwCloseBtn.css({
        opacity: '1', // by default the close button has an opacity of 0.7
        right: '60px', top: '20px', // button repositioning
        // border: '.5px solid #e90000', // increasing button border and new color
        'border-radius': '50%', // circular effect
        width: '1.5em', height: '1.5em',
        // 'background-color' : '#212',
        'text-align' : 'center',
        'font-size' : '18px',
        // 'vertical-align' : 'middle',
        });

        iwCloseBtn.html('<span class="mdi mdi-close-circle-outline mdi-24px mdi-light" title="Close"></span>');

        // The API automatically applies 0.7 opacity to the button after the mouseout event.
        // This function reverses this event to the desired value.
        iwCloseBtn.mouseout(function(){
            $(this).css({opacity: '1'});
        });

    });

    initTerminals();
}

function initTerminals() {
    var terminals = Object.values(terminalList);
    for (let index = 0; index < terminals.length; index++) {
        const terminal = terminals[index];
        terminal.setMarker(map, infowindow);
    }
}

function hideTerminals() {
    var terminals = Object.values(terminalList);

    for (let index = 0; index < terminals.length; index++) {
        const terminal = terminals[index];
        terminal.hide();
    }
}

function showTerminals() {
    var terminals = Object.values(terminalList);
    for (let index = 0; index < terminals.length; index++) {
        const terminal = terminals[index];
        terminal.showOn(map);
    }
}

function findTerminal(imei) {
    if (imei && terminalList.hasOwnProperty(imei)) {
        var terminal = terminalList[imei];
        map.setCenter(terminal.getPos());
        map.setZoom(12);
    }
}

function findLocation(latitude, longitude)
{
    var pos = new google.maps.LatLng(latitude, longitude);
    geocodeLatLng(pos, function (location) {
        if (location) {
            var marker = new google.maps.Marker({
                position: pos,
                map: map,
                animation: google.maps.Animation.DROP,
                icon: icons.PURPLE
            });
            map.setCenter(pos);
            map.setZoom(9);
            var content = '' +
            '<div id="iw-container">' +
                '<div class="iw-title"> Location found  </div>' +
                '<div class="iw-content">' +
                    '<div class="iw-subTitle">Location coordinates</div>' +
                    '<div><b>Latitude</b> ' + latitude + '</div>' +
                    '<div><b>Longitude</b> ' + longitude + '</div>' +
                    '<div class="iw-subTitle">Location description</div>' +
                    '<div>' + location + '</div>' +
                '</div>' +
            '</div>';
            infowindow.setContent(content);
            infowindow.open(map, marker);

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.setContent(content);
                infowindow.open(map,marker);
            });

            markerlist.push(marker);
        }
    });
}

function drawPolyLine(data)
{
    var path = [];
    for (let i = 0; i < data.length; i++) {
        const coordinate = data[i];
        path.push({lat: coordinate.latitude, lng: coordinate.longitude});
    }
    route = new google.maps.Polyline({
        path: path,
        geodesic: true,
        strokeColor: 'brown',
        strokeOpacity: 1.0,
        strokeWeight: 2
    });
    route.setMap(map);
}

function removePolyline()
{
    if (route) {
        route.setMap(null);
        route = null;
    }
}

function geocodeLatLng(latlng, callback) {
    if(!geocoder || !latlng) return "Unknown location";

    //Method to actually do the check from google maps api
    geocoder.geocode({ "location": latlng }, function (results, status) {
        if (status === "OK") { // Check if searched returned successfully
            if (results[0]) { // check if first element of results is non-null
                callback (results[0].formatted_address);
            } else {
                callback ("Unknown location");
            }
        } else {
            console.log("Geocoder failed due to: " + status);
            callback ("Unknown location");
        }
    });
}

function showHeatMap(data)
{
    var cleandata = [];
    for (let i = 0; i < data.length; i++) {
        const coordinate = data[i];
        cleandata.push(new google.maps.LatLng(coordinate.latitude, coordinate.longitude));
    }
    heatmap = new google.maps.visualization.HeatmapLayer({
        data: cleandata,
        map: map
    });
    var gradient = [
        'rgba(0, 255, 255, 0)',
        'rgba(0, 255, 255, 1)',
        'rgba(0, 191, 255, 1)',
        'rgba(0, 127, 255, 1)',
        'rgba(0, 63, 255, 1)',
        'rgba(0, 0, 255, 1)',
        'rgba(0, 0, 223, 1)',
        'rgba(0, 0, 191, 1)',
        'rgba(0, 0, 159, 1)',
        'rgba(0, 0, 127, 1)',
        'rgba(63, 0, 91, 1)',
        'rgba(127, 0, 63, 1)',
        'rgba(191, 0, 31, 1)',
        'rgba(255, 0, 0, 1)'
    ];
    heatmap.set('gradient', gradient);
    heatmap.set('opacity',0.5);
}

function removeHeatMap()
{
    if (heatmap) {
        heatmap.setMap(null);
        heatmap = null;
    }
}

function showMarkers(data)
{
    for (let i = 0; i < data.length; i++) {
        const element = data[i];
        var pos = new google.maps.LatLng(element.latitude, element.longitude);
        var marker = new google.maps.Marker({
            position: pos,
            map: map,
            icon: icons.BROWN
        });
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent(element.description);
            infowindow.open(map,this);
        });
        markerlist.push(marker);
    }
}

function removeMarkers()
{
    for (let i = 0; i < markerlist.length; i++) {
        var marker = markerlist[i];
        marker.setMap(null);
    }
    markerlist = [];
}

function resetMap()
{
    removeHeatMap();
    removePolyline();
    removeMarkers();
    removeRouteRepeat();
}

function getLatLng(callback)
{
    var listener = google.maps.event.addListener(map, "click", function (e) {  
        callback(e.latLng);
    });
    google.maps.event.removeListener(listener1);
}

$('#search-form').submit( function (e) {
    e.preventDefault();
    var latitude = $('#search-latitude').val();
    var longitude = $('#search-longitude').val();
    findLocation(latitude, longitude);
});

$('#clear-map').click(function (e) {  
    e.preventDefault();
    resetMap();
});

function RouteStat(data)
{
    var path = [];
    for (let i = 0; i < data.length; i++) {
        const coordinate = data[i];
        path.push({lat: coordinate.latitude, lng: coordinate.longitude, description: coordinate.description});
    }
    this.path = path;
    this.currentIndex = 0;
    
    var pos = new google.maps.LatLng(path[0].lat, path[0].lng);
    var marker = new google.maps.Marker({
        position: pos,
        map: map,
        icon: icons.BROWN
    });

    var rStat = this;
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(rStat.path[rStat.currentIndex].description);
        infowindow.open(map,this);
    });
    
    this.marker = marker;

    var temp_path = [];
    
    temp_path.push({lat: path[0].lat, lng: path[0].lng});

    var polyline = new google.maps.Polyline({
        path: temp_path,
        geodesic: true,
        strokeColor: 'brown',
        strokeOpacity: 1.0,
        strokeWeight: 2
    });
    polyline.setMap(map);
    this.p_route = polyline;

    this.speed = 1;

    this.pausestatus = false;

    // this.play();
    
}

RouteStat.prototype.stepForward = function() {
    var currentIndex = (this.currentIndex + 1) % this.path.length;

    var currentLatLng = this.path[currentIndex];
    if (currentLatLng) {
        var temp_path = this.p_route.getPath();

        var pos = new google.maps.LatLng(currentLatLng.lat, currentLatLng.lng);

        temp_path.push(pos);
        this.p_route.setPath(temp_path);
        this.marker.setPosition(pos);
        map.setCenter(pos);
        infowindow.setContent(currentLatLng.description);
    }

    this.currentIndex = currentIndex;
}

RouteStat.prototype.stepBack = function() {
    var temp_path = this.p_route.getPath();
    var currentIndex = (this.currentIndex - 1) < 0 ? 0 : this.currentIndex - 1;
    
    if (temp_path.length > 1) {
        temp_path.pop();
        this.p_route.setPath(temp_path);

        var currentLatLng = temp_path[temp_path.length - 1];
        var pos = new google.maps.LatLng(currentLatLng);
        this.marker.setPos(pos);
        map.setCenter(pos);
        var otherLatLng = this.path[currentIndex];
        infowindow.setContent(otherLatLng.description);
    }

    this.currentIndex = currentIndex;
}

RouteStat.prototype.setSpeed = function (speed) {
    this.speed = speed;
}

function playRouteRepeat() {
    var speed = routeRepeat.speed * 500;
    if (routeRepeat.pausestatus || routeRepeat.currentIndex >= routeRepeat.path.length - 1) {
        return;
    }
    setTimeout(playRouteRepeat, speed);
    routeRepeat.stepForward();
}

RouteStat.prototype.play = function () {
    console.log(routeRepeat);
    var speed = this.speed * 500;
    if (this.pausestatus || this.currentIndex >= this.path.length - 1) {
        return;
    }
    setTimeout(this.play, speed);
    // this.stepForward();
}

RouteStat.prototype.pause = function ()
{
    this.pausestatus = true;
}

RouteStat.prototype.resume = function ()
{
    if (this.pausestatus) {
        this.pausestatus = false;
    }
}

RouteStat.prototype.clear = function()
{
    this.p_route.setMap(null);
    this.p_route = null;
    this.marker.setMap(null);
    this.marker = null;
}

function createRouteRepeat(data)
{
    routeRepeat = new RouteStat(data);
    playRouteRepeat();
}

function removeRouteRepeat()
{
    if (routeRepeat) {
        routeRepeat.clear();
    }
    routeRepeat = null;
}