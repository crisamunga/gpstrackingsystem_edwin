var partialcontainers = {};
(function ($) {    
    $.fn.partialLoader = function (action) {
        if (action == 'autoload') {
            $('#loading-screen').fadeIn('fast');
            var state = $(this).data('state');
            if (state == 'done') {
                return;
            }
            var url = $(this).data('source');
            var container = $('.partial-container[data-name='+$(this).data('container')+']');
            var mode = $(this).data('mode');
            $.ajax({
                url: url,
                dataType: 'html',
                timeout: 15000,
                beforeSend: function () {  
                    $('#loading-screen').fadeIn('fast');
                }
            }).done(function (data, status) {
                if(container != null)
                    if (mode == 'append') {
                        container.append(data);
                    } else if (mode == 'prepend') {
                        container.prepend(data);
                    } else {
                        container.html(data);
                    }
                else
                    flash_message('Browser could not load the requested page', 'Error loading page', 'SYSTEM', 'ERROR');
            }).fail(function (data, status, error) {  
                flash_message('Server responded with error code ' + data.status, error, 'SERVER', 'ERROR');
            }).always(function (data) {
                $('#loading-screen').fadeOut('fast');
            });
            $(this).data('state','done');
        }
        else if (action == 'link') {
            $(this).click(function(e) {
                e.preventDefault();
                var url = this.href;
                var container = $('.partial-container[data-name='+$(this).data('container')+']');
                var mode = $(this).data('mode');       
                
                $.ajax({
                    url: url,
                    dataType: 'html',
                    timeout: 15000,
                    beforeSend: function () {  
                        $('#loading-screen').fadeIn('fast');
                    }
                }).done(function (data, status) {
                    if(container != null)
                        if (mode == 'append') {
                            container.append(data);
                        } else if (mode == 'prepend') {
                            container.prepend(data);
                        } else {
                            container.html(data);
                        }
                    else
                        flash_message('Browser could not load the requested page', 'Error loading page', 'SYSTEM', 'ERROR');
                }).fail(function (data, status, error) {  
                    flash_message('Server responded with error code ' + data.status, error, 'SERVER', 'ERROR');
                }).always(function (data) {
                    $('#loading-screen').fadeOut('fast');
                });
            });
        }
        else if (action == 'button') {
            $(this).click(function(e) {
                e.preventDefault();
                var url = $(this).data('source');
                var container = $('.partial-container[data-name='+$(this).data('container')+']');
                var containername = $(this).data('container');
                var mode = $(this).data('mode');

                $.ajax({
                    url: url,
                    dataType: 'html',
                    timeout: 15000,
                    beforeSend: function () {  
                        $('#loading-screen').fadeIn('fast');
                    }
                }).done(function (data, status) {
                    if(container != null) {
                        if (mode == 'append') {
                            container.append(data);
                        } else if (mode == 'prepend') {
                            container.prepend(data);
                        } else {
                            container.html(data);
                        }
                        if (containername == 'modal-content') {
                            show_modal()
                        }
                    }
                    else {
                        flash_message('Browser could not load the requested page', 'Error loading page', 'SYSTEM', 'ERROR');
                    }
                }).fail(function (data, status, error) {  
                    flash_message('Server responded with error code ' + data.status, error, 'SERVER', 'ERROR');
                }).always(function (data) {
                    $('#loading-screen').fadeOut('fast');
                });
            });
        }
        else if (action == 'form') {                                     
            $(this).submit( function (e) {
                e.preventDefault();                
                $form = $(this);
                var url = $form.attr('action');
                var container = $('.partial-container[data-name='+$(this).data('container')+']');
                var mode = $(this).data('mode');
                var method = $form.attr('method');
                var data = $form.serialize();
                $.ajaxSetup({
                    header:$('meta[name="_token"]').attr('content')
                });
                $.ajax({
                    type: method,
                    url: url,
                    data: data,
                    dataType: 'html',
                    beforeSend: function () {  
                        $('#loading-screen').fadeIn('fast');
                    }
                }).done(function (data, status) {
                    if(container != null)
                        if (mode == 'append') {
                            container.append(data);
                        } else if (mode == 'prepend') {
                            container.prepend(data);
                        } else {
                            container.html(data);
                        }
                    else
                        flash_message('Browser could not load the requested page', 'Error loading page', 'SYSTEM', 'ERROR');
                }).fail(function (data, status, error) {  
                    flash_message('Server responded with error code ' + data.status, error, 'SERVER', 'ERROR');
                }).always(function (data) {
                    $('#loading-screen').fadeOut('fast');
                });
            });
        }
        else if (action == 'remove') {
            $(this).click(function(e) {
                e.preventDefault();
                var mode = $(this).data('mode');
                if (mode == 'parent') {
                    $(this).parent().remove();
                } else if (mode == 'self') {
                    $(this).remove();
                } else {
                    var container = $($(this).data('target'));
                    container.remove();
                }
            });
        }
    }
}(jQuery));