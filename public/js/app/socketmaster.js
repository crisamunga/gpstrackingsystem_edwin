var socket = io.connect('http://localhost:31340');
var imeis = [];

function Terminal() {
    this.imei = imei;    
    this.marker = null;
}

socket.on('connected', function() {
    // var msg = new Object();

    // $('.terminal-entry').each(function (index, element) {
    //     var imei = $(this).data('imei');
    //     if ($.inArray(imei, imeis) < 0) {
    //         imeis.push(imei);
    //     }
    // });

    // msg.imeis = imeis;
    // msg.type = 'Monitor';

    // socket.emit('configureMonitor', msg);
    
    socket.emit('authenticate', key);
});

socket.on('notification', function(msg) {      
    if (msg.type == 'Login') {
        changeOnlineStatus(msg.imei, true);     
        msg.description = 'Now online';
    } else if (msg.type == 'Logout') {
        changeOnlineStatus(msg.imei, false);
        msg.description = 'Now offline';
    }
    showNotification($('#'+msg.imei + '-name ').data('name') + ":<br> " + msg.description, msg.type);
});

socket.on('alarm', function(msg) {             
    showNotification(msg.description + " from " + $('#'+msg.imei + '-name ').data('name'), msg.type);
});

socket.on('disconnect', function(){
    imeis.forEach( function (imei) {
        changeOnlineStatus(imei, false);
        showNotification("Lost connection to server", "alarm");
    });
});

setupCommandButtons();

function setupCommandButtons() {
    $('.terminal-command').each(function (index, element) {
        // element == this
        $(this).click(function (e) { 
            e.preventDefault();
            var imeis = [];
            $('.terminal-entry.selected').each(function (index, element) {
                // element == this
                var imei = $(this).data('imei');
                imeis.push(imei);
            });
            if (imeis.length < 1) {
                return;
            }
            var command = new Object();
            command.name = $(this).data('name');
            command.parameter = $(this).data('parameter');
            command.imeis = imeis;
            socket.emit('command', command);
        });
    });
}

function setupTerminals() {
    
}

function changeOnlineStatus(imei, newStatus) {
    target = '#'+imei+'-status';
    if (newStatus) {
        $(target).removeClass('offline');
        $(target).addClass('online');
    } else {
        $(target).removeClass('online');
        $(target).addClass('offline');
    }
}

function showNotification (data, msgtype) {
    var type = 'notify-inverse-info';
    var delay = 5000;
    if (msgtype == 'alarm') {
        type = 'notify-inverse-danger';
        delay = 0;
    } else if (msgtype == 'Login' || msgtype == 'Logout') {
        type = 'notify-inverse-success';
    }
    $.notify({	// options
        icon: 'glyphicon glyphicon-warning-sign',
        message: data,        
        target: '_blank'
    },{
        // settings
        element: 'body',
        position: null,
        type: type,
        allow_dismiss: true,
        newest_on_top: false,
        showProgressbar: false,
        placement: {
            from: "top",
            align: "right"
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
        delay: delay,
        timer: 1000,
        url_target: '_blank',
        mouse_over: null,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
        template:   '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} notify-alert-inverse" role="alert">' +
                        '<button type="button" aria-hidden="true" class="close notify-close" data-notify="dismiss"><span class="mdi mdi-close-circle-outline"> </span></button>' +
                        '<span data-notify="icon"></span> ' +
                        '<span data-notify="title">{1}</span> ' +
                        '<span data-notify="message">{2}</span>' +
                        '<div class="progress" data-notify="progressbar">' +
                            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                        '</div>' +
                        '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>' 
    });
}