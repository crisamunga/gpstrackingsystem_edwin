// var socket = io.connect('http://'+sockserverip+':'+ sockserverport +'');

/*
    socket.on('connected', function() {
        socket.emit('authenticate', key);
    });

    socket.on('alert', function(msg) {      
        var terminal = terminalList[msg['imei']];
        if (msg.type == 'Login') {
            terminal.setOnlineStatus(true);
        } else if (msg.type == 'Logout') {
            terminal.setOnlineStatus(false);
        } else if (msg.type == 'Stopped') {
            terminal.stop();
        } else if (msg.type == 'Moving') {
            terminal.move();
        }
        showNotification(msg.description, msg.type, terminal.getName());
    });

    socket.on('live data', function (data) {  
        var terminal = terminalList[data['imei']];
        terminal.newLiveData(data);
    });

    socket.on('disconnect', function(){
        var terminals = Object.values(terminalList);
        for (let i = 0; i < terminals.length; i++) {
            var terminal = terminals[i];
            terminal.setOnlineStatus(false);
        }
        showNotification("Lost connection to server", "alarm", "SERVER");
    });
*/

/*
    setupCommandButtons();

    function setupCommandButtons() {
        $('.terminal-command').each(function (index, element) {
            // element == this
            $(this).click(function (e) { 
                e.preventDefault();
                var imeis = [];
                $('.terminal-entry.selected').each(function (index, element) {
                    // element == this
                    var imei = $(this).data('imei');
                    imeis.push(imei);
                });
                if (imeis.length < 1) {
                    return;
                }
                var command = new Object();
                command.name = $(this).data('name');
                command.parameter = $(this).data('parameter');
                command.imeis = imeis;
                socket.emit('command', command);
            });
        });
    }
*/

function showNotification (data, msgtype, source) {
    var type = 'INFO';
    var delay = 5000;
    
    if (msgtype == 'alarm') {
        type = 'ERROR';
    } else if (msgtype == 'Login' || msgtype == 'Logout') {
        type = 'PRIMARY';
    } else if (msgtype == 'notification') {
        type = 'SUCCESS';
    }

    var icon = 'TERMINAL';
    if (source == 'SERVER') {
        icon = source;
    }
    
    flash_message(data, source, icon, type);
}

var imeis = Object.keys(terminalList);
imeis.forEach((imei) => {
    var terminal = terminalList[imei];

    window.Echo.private('gpstrackingsystem-data-'+imei).listen('.notification', (msg) => {
        terminal.newLiveData(msg.data.data);
    });

    window.Echo.private('gpstrackingsystem-event-'+imei).listen('.notification', (msg) => {
        if (msg.data.type == 'Login') {
            terminal.setOnlineStatus(true);
        } else if (msg.data.type == 'Logout') {
            terminal.setOnlineStatus(false);
        }
        showNotification(msg.data.description, msg.data.type, terminal.getName());
    });
});