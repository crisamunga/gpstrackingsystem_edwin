<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DrivermetricsRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('data_log_entries', function (Blueprint $table) {
            $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');
        });

        Schema::table('terminal_duration_logs', function (Blueprint $table) {
            $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');
        });

        Schema::table('terminal_event_entries', function (Blueprint $table) {
            $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_log_entries', function (Blueprint $table) {
            $table->dropForeign(['driver_id']);
        });

        Schema::table('terminal_duration_logs', function (Blueprint $table) {
            $table->dropForeign(['driver_id']);
        });

        Schema::table('terminal_event_entries', function (Blueprint $table) {
            $table->dropForeign(['driver_id']);
        });       
        
    }
}