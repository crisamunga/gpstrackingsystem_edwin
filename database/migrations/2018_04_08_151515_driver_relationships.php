<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DriverRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drivers', function (Blueprint $table) {
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('imei')->references('imei')->on('terminals')->onDelete('set null');
        });
        Schema::table('terminals', function (Blueprint $table) {
            $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drivers', function (Blueprint $table) {
            $table->dropForeign(['owner_id']);
            $table->dropForeign(['imei']);
        });
        Schema::table('terminals', function (Blueprint $table) {
            $table->dropForeign(['driver_id']);
        });
    }
}
