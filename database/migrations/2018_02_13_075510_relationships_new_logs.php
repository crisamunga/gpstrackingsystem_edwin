<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelationshipsNewLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('terminal_duration_logs', function (Blueprint $table) {
            $table->foreign('imei')->references('imei')->on('terminals')->onDelete('cascade');
        });

        Schema::table('terminal_access_logs', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('terminal_id')->references('id')->on('terminals')->onDelete('cascade');
        });

        Schema::table('terminal_config_logs', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('terminal_id')->references('id')->on('terminals')->onDelete('cascade');
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('terminal_duration_logs', function (Blueprint $table) {
            $table->dropForeign('imei');
        });

        Schema::table('terminal_access_logs', function (Blueprint $table) {
            $table->dropForeign('user_id');
            $table->dropForeign('terminal_id');
        });

        Schema::table('terminal_config_logs', function (Blueprint $table) {
            $table->dropForeign('user_id');
            $table->dropForeign('terminal_id');
        });
    }
}
