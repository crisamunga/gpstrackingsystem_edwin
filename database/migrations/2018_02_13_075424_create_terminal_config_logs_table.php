<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerminalConfigLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terminal_config_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('terminal_id')->nullable()->unsigned();
            $table->integer('user_id')->nullable()->unsigned();
            $table->string('description')->nullable()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terminal_config_logs');
    }
}
