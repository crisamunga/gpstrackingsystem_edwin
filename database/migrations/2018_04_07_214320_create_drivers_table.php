<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->integer('owner_id')->nullable()->unsigned();
            $table->string('imei')->nullable();
            $table->timestamps();
        });
        Schema::table('terminals', function (Blueprint $table) {
            $table->integer('driver_id')->after('client_id')->nullable()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('terminals', function (Blueprint $table) {
            $table->dropColumn('driver_id');
        });
        Schema::dropIfExists('drivers');
    }
}
