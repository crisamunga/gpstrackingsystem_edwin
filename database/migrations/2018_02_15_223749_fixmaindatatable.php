<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fixmaindatatable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('terminal_main_data_entries', function (Blueprint $table) {
            $table->float('distance',9,3)->default(0)->after('speed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('terminal_main_data_entries', function (Blueprint $table) {
            $table->dropColumn('distance');
        });
    }
}
