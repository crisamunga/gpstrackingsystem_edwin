<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fixlogdata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('terminal_event_entries', function (Blueprint $table) {
            $table->double('latitude',9,6)->nullable()->after('keyword');
            $table->double('longitude',8,6)->nullable()->after('latitude');
        });

        Schema::table('terminal_main_data_entries', function (Blueprint $table) {
            $table->dropColumn('distance');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
