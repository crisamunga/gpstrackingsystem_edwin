<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Drivermetrics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_log_entries', function (Blueprint $table) {
            $table->integer('driver_id')->nullable()->unsigned()->after('imei');
        });

        Schema::table('terminal_duration_logs', function (Blueprint $table) {
            $table->integer('driver_id')->nullable()->unsigned()->after('imei');
        });

        Schema::table('terminal_event_entries', function (Blueprint $table) {
            $table->integer('driver_id')->nullable()->unsigned()->after('imei');
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_log_entries', function (Blueprint $table) {
            $table->dropColumn('driver_id');
        });

        Schema::table('terminal_duration_logs', function (Blueprint $table) {
            $table->dropColumn('driver_id');
        });

        Schema::table('terminal_event_entries', function (Blueprint $table) {
            $table->dropColumn('driver_id');
        });       
    }
}