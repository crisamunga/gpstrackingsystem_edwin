<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Carbon\Carbon;

class PrimaryUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('name','Super Admin')->first();
        if ($role === NULL) {
            $permission = Permission::where('name','Administer roles and permissions')->first();
            if ($permission === NULL) {
                $permission = new Permission();
                $permission->name = 'Administer roles and permissions';
                $permission->save();   # code...
            }
            
            $role = new Role();
            $role->name = 'Super Admin';
            $role->save();
            $role->givePermissionTo($permission);
        }

        $user = new User();
        $user->fill([
            'first_name' => 'Chris',
            'middle_last_name' => 'Martin Amunga',
            'email' => 'am.martin.ac@gmail.com',
            'phone' => '254706514389',
            'password' => 'a000000a'
        ]);
        $user->lastdashboardcheck = Carbon::now()."";
        $user->save();
        $user->assignRole($role);
    }
}
