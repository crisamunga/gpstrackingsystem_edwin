<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\TerminalEventType;

class TerminalEventTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = config('gpsprotocol.TK 103A.device.keywords');

        foreach($data as $key => $value)
        {
            if ($key != 'parameters') {
                $terminal = TerminalEventType::where('keyword', '=', $key)->first();
                if (!isset($terminal)) {
                    TerminalEventType::create([
                        'keyword' => $key,
                        'type' => $value['type'],
                        'description' => $value['description']
                    ]);
                }
            }
        }
    }
}