<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client_permissions = [
            'Terminal Control',
            'Access Map',
            'View Reports',
            'Manage Terminals - Client',
        ];

        $admin_permissions = [
            'Manage Terminals - Admin',
            'Manage Users',
            'Terminal Control - All Terminals',
            'Manage Server',
            'Access Admin Panel',
            'View Logs',
            'Administer roles and permissions',
        ];

        $admin = Role::where('name','Super Admin')->first();
        if ($admin === NULL) {
            $admin = new Role();
            $admin->name = 'Super Admin';
            $admin->save();
        }

        $client = Role::where('name','Client')->first();
        if ($client === NULL) {
            $client = new Role();
            $client->name = 'Client';
            $client->save();
        }

        foreach ($admin_permissions as $name) {
            $permission = new Permission();
            $permission->name = $name;
            $permission->save();
            $admin->givePermissionTo($permission);
        }

        foreach ($client_permissions as $name) {
            $permission = new Permission();
            $permission->name = $name;
            $permission->save();
            $admin->givePermissionTo($permission);
            $client->givePermissionTo($permission);
        }
    }
}