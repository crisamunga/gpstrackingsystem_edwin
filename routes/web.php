<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');


Route::get('fire', function () {
    // this fires the event
    event(new App\Events\MapAccess());
    return "event fired";
});

Route::get('test', function () {
    // this checks for the event
    return view('test');
});

/*
|--------------------------------------------------------------------------
| Admin Panel Routes
|--------------------------------------------------------------------------
| These routes manage access to the admin panel. 
|  
*/

Route::get('/admin', 'AdminPanelController@index')->name('admin')->middleware(['auth', 'admin']);

Route::get('/admin/dashboard', 'AdminPanelController@dashboard')->name('admin.dashboard')->middleware(['auth', 'ajax', 'admin']);

Route::resource('users', 'UserController', ['except', [
    'show'
]])->middleware(['auth', 'ajax', 'admin', 'manageUsers']);

Route::resource('roles', 'RoleController', ['except', [
    'show'
]])->middleware(['auth', 'ajax', 'admin', 'manageUsers']);

Route::resource('permissions', 'PermissionController', ['except', [
    'show'
]])->middleware(['auth', 'ajax', 'admin', 'manageUsers']);

Route::resource('terminals', 'TerminalController', ['except', [
    'show'
]])->middleware(['auth', 'ajax', 'admin', 'terminalAdmin']);

Route::get('terminals/edituser/{id}', 'TerminalController@editUser')->name('terminals.editUser')->middleware(['auth', 'ajax']);

Route::post('terminals/updateuser/{id}', 'TerminalController@updateUser')->name('terminals.updateUser')->middleware(['auth', 'ajax']);

Route::resource('terminalconfigs', 'TerminalConfigurationController')->middleware(['auth', 'ajax']);


/*
|--------------------------------------------------------------------------
| My Account Panel Routes
|--------------------------------------------------------------------------
| These routes manage access to the my account panel. 
|  
*/

Route::resource('myaccount', 'MyAccountController', ['only', [
    'index','create', 'store'
]])->middleware(['auth', 'ajax']);

/*
|--------------------------------------------------------------------------
| Home Panel Routes
|--------------------------------------------------------------------------
| These routes manage access to the home panel. 
|  
*/

Route::get('/home', 'HomeController@index')->name('home')->middleware(['auth']);

Route::get('/home/dashboard', 'HomeController@dashboard')->name('home.dashboard')->middleware(['auth', 'ajax', 'admin']);


Route::resource('userterminals', 'UserTerminalsController', ['only', [
    'index','edit','update'
]])->middleware(['auth', 'ajax', 'terminalManage']);

Route::resource('terminalgroups', 'TerminalGroupController', ['except', [
    'show'
]])->middleware(['auth', 'ajax', 'terminalManage']);

Route::resource('drivers', 'DriverController', ['except', [
    'show'
]]);


/**
 * Reports subsection routes
 * This is meant for the reports subsection of home
 */

Route::get('/home/reports', 'ReportController@index')->name('reports')->middleware(['auth','reports']);

Route::get('/home/reports/dashboard', 'ReportController@dashboard')->name('reports.dashboard')->middleware(['auth', 'ajax', 'reports']);

Route::post('home/reports/export', 'ReportController@export')->name('exportreport')->middleware(['auth', 'reports']);

Route::resource('/home/reports/terminalperformance', 'TerminalPerformanceReportController', ['names' => [
    'index' => 'terminalperformance.index',
    'create' => 'terminalperformance.create',
    'store' => 'terminalperformance.store',
    'show' => 'terminalperformance.show'
]], ['except', [
    'edit', 'update', 'delete'
]])->middleware(['auth', 'ajax', 'reports']);

Route::resource('/home/reports/terminalalarms', 'TerminalAlarmReportController', ['names' => [
    'index' => 'terminalalarms.index',
    'create' => 'terminalalarms.create',
    'store' => 'terminalalarms.store',
    'show' => 'terminalalarms.show'
]], ['except', [
    'edit', 'update', 'delete'
]])->middleware(['auth', 'ajax', 'reports']);

Route::resource('/home/reports/driverperformance', 'DriverPerformanceReportController', ['names' => [
    'index' => 'driverperformance.index',
    'create' => 'driverperformance.create',
    'store' => 'driverperformance.store'
]], ['except', [
    'show','edit', 'update', 'delete'
]])->middleware(['auth', 'ajax', 'reports']);

Route::resource('/home/reports/driveralarms', 'DriverAlarmReportController', ['names' => [
    'index' => 'driveralarms.index',
    'create' => 'driveralarms.create',
    'store' => 'driveralarms.store'
]], ['except', [
    'show','edit', 'update', 'delete'
]])->middleware(['auth', 'ajax', 'reports']);

/*
|--------------------------------------------------------------------------
| Map Panel Routes
|--------------------------------------------------------------------------
| These routes manage access to the map panel. 
|  
*/

Route::get('/map', 'MapController@index')->name('map')->middleware(['auth','map']);

Route::resource('routerepeat', 'RouteRepeatController', ['only', [
    'create','store'
]])->middleware(['auth','map']);

Route::resource('routerepeat', 'RouteRepeatController', ['only', [
    'create','store'
]])->middleware(['auth','map']);

Route::resource('/map/statistics/performance', 'PerformanceMapController', ['names' => [
    'create' => 'mapperformance.create',
    'store' => 'mapperformance.store',
]], ['except', [
    'index','edit','show','update', 'delete'
]])->middleware(['auth', 'map', 'ajax']);

Route::resource('/map/statistics/terminalperformance', 'MapTerminalPerformanceController', ['names' => [
    'create' => 'terminalmapperformance.create',
    'store' => 'terminalmapperformance.store',
]], ['except', [
    'index','edit','show','update', 'delete'
]])->middleware(['auth', 'map', 'ajax']);

Route::resource('/map/statistics/terminalalarms', 'MapTerminalAlarmController', ['names' => [
    'create' => 'terminalmapalarms.create',
    'store' => 'terminalmapalarms.store',
]], ['except', [
    'index','edit','show','update', 'delete'
]])->middleware(['auth', 'map', 'ajax']);

Route::resource('/map/statistics/driverperformance', 'MapDriverPerformanceController', ['names' => [
    'create' => 'drivermapperformance.create',
    'store' => 'drivermapperformance.store',
]], ['except', [
    'index','edit','show','update', 'delete'
]])->middleware(['auth', 'map', 'ajax']);

Route::resource('/map/statistics/driveralarms', 'MapDriverAlarmController', ['names' => [
    'create' => 'drivermapalarms.create',
    'store' => 'drivermapalarms.store',
]], ['except', [
    'index','edit','show','update', 'delete'
]])->middleware(['auth', 'map', 'ajax']);

Route::get('commandterminal/{id}', 'CommandTerminalController@show')
    ->name('commandterminal.show')
    ->middleware(['auth', 'terminalControl', 'ajax']);

Route::post('commandterminal/{id}', 'CommandTerminalController@send')
    ->name('commandterminal.send')
    ->middleware(['auth', 'terminalControl', 'ajax']);

/*
|--------------------------------------------------------------------------
| Server Panel Routes
|--------------------------------------------------------------------------
| These routes manage access to the map panel. 
|  
*/

// Route::resource('tcpservers', 'TCPSocketServerController')->middleware('auth');

Route::resource('mapservers', 'MapSocketServerController')->middleware('auth');

Route::resource('apikeys', 'APIKeyController')->middleware('auth');

Route::get('serverlogs','ViewLogController@index')->name('serverlogs.index')->middleware('auth');

Route::get('serverlogs/other','ViewLogController@other')->name('serverlogs.other')->middleware('auth');

Route::post('serverlogs/show','ViewLogController@show')->name('serverlogs.show')->middleware('auth');   