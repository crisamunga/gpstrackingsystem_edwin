try {
    window.$ = window.jQuery = require('jquery');
} catch (e) {}

$(document).ajaxError(function(event, jqxhr, settings, exception) {
    if (exception == 'Unauthorized') {
        window.location = '/login';
    }
});

/**
 * This secton manages notifications
 */

var notify_sender_icons = {
    SYSTEM: 'mdi mdi-circle-outline mdi-24px',
    SERVER: 'mdi mdi-server mdi-24px',
    TERMINAL: 'mdi mdi-car mdi-24px',
    DRIVER: 'mdi mdi-steering mdi-24px',
    ERROR: 'mdi mdi-exclamation mdi-24px',
    USER: 'mdi mdi-account mdi-24px',
    UGROUP: 'mdi mdi-account-multiple mdi-24px',
    TGROUP: 'mdi mdi-google-circles-extended mdi-24px',
    REPORT: 'mdi mdi-clipboard-text mdi-24px',
    ACCOUNT: 'mdi mdi-account-card-details mdi-24px',
    NOTIFICATION: 'mdi mdi-email-alert mdi-24px',
    SETTINGS: 'mdi mdi-settings mdi-24px',
    PERMISSION: 'mdi mdi-key-variant mdi-24px',
    API: 'mdi mdi-server-security mdi-24px',
    SOCKSERVER: 'mdi mdi-server-network mdi-24px',
    'LOGS': 'mdi mdi-server-remove mdi-24px'
};

var notify_types = {
    INFO: 'notify-inverse-info',
    PRIMARY: 'notify-inverse-primary', 
    ERROR: 'notify-inverse-danger',
    SUCCESS: 'notify-inverse-success' 
};

function flash_message(message, title, sender, type) {
    $.notify({	// options
        icon: notify_sender_icons[sender || 'SYSTEM'],
        message: message,
        title: title || "System"
    },{
        // settings
        element: 'body',
        position: null,
        type: notify_types[type],
        allow_dismiss: true,
        newest_on_top: false,
        showProgressbar: false,
        placement: {
            from: "top",
            align: "right"
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
        delay: 5000,
        timer: 1000,
        url_target: '_blank',
        mouse_over: null,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
        template:   '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} notify-alert-inverse" role="alert">' +
                        '<button type="button" aria-hidden="true" class="close notify-close" data-notify="dismiss"><span class="mdi mdi-close"> </span></button>' +
                        '<div class="pull-left alert-icon"><span data-notify="icon"></span></div>'+
                        '<div class="pull-left" style="margin-left: 1em">' +
                            '<div><h5><span data-notify="title">{1}</span></h5></div>'+
                            '<div><span data-notify="message">{2}</span></div>'+
                        '</div>'+
                        '<div class="progress" data-notify="progressbar">' +
                            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                        '</div>' +
                        '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>'
    });
}