import Echo from "laravel-echo"

window.io = require('socket.io-client');

window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':31340',
    auth: {
        headers: {
            Authorization: 'Bearer ' + AUTH_API_TOKEN,
        },
    },
});