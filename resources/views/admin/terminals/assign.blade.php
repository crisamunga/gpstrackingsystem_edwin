@extends('layouts.partial')

@section('header')
    <h2>Add terminal</h2>
    <p>Add a new terminal to the system</p>
    @if (count($errors) > 0)
        <p class="text-danger" >It seems there were errors in the input</p>
    @endif
@endsection

@section('body')
    {{ Form::model($terminal, array('route' => array('terminals.updateUser', $terminal->id), 'method' => 'POST', 'class' => 'partial-form-embed', 'data-container' => 'home')) }}    

    <div class="form-group {{ $errors->has('user') ? ' has-error' : '' }}">
        {{ Form::label('description', 'Select User') }}
        @if ($errors->has('user'))
            <span class="help-block">
                <strong>{{ $errors->first('user') }}</strong>
            </span>
        @endif
        <div class="table-responsive">
            <table class="table table-bordered data-table-embed" width="100%">
                <colgroup>
                    <col span="1" style="width: 3%; background-color: gray">
                </colgroup>
                <thead>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Phone</th>                
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Phone</th>                
                    </tr>
                </tfoot>

                <tbody>
                    <tr>                
                        <td>
                            <label class="switch">
                                <input name='user' type='radio' value='-1' {{ ( $terminal->user == null ) ? 'checked' : '' }}>
                                <span class="slider round"></span>
                            </label>
                        </td>
                        <td>None</td>
                        <td>None</td>
                        <td>None</td>                                
                    </tr>
                    @foreach ($users as $user)
                    <tr>                
                        <td>
                            <label class="switch">
                                <input name='user' type='radio' value='{{ $user->id }}' {{ ( $terminal->user != null && $user->id == $terminal->user->id) ? 'checked' : '' }}>
                                <span class="slider round"></span>
                            </label>
                        </td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>                                
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="form-group">
        <button data-source="{{ route('terminals.index') }}" class="btn btn-red pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
            <i class="mdi mdi-close mdi-18px"></i> Cancel
        </button>
    
        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Submit
        </button>
    </div>

    {{ Form::close() }}
@endsection

@section('footer')
    @include('components.embeddedtable')
@endsection