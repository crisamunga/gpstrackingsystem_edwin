@extends('layouts.partial')

@section('header')
    <h2>Manage Permissions</h2>
    <p>Create, edit and remove user permissions from the system for access control</p>
@endsection

@section('body')
    <div class="table-responsive">
        <table class="table data-table-embed" width="100%">
            <thead>
                <tr>
                    <th>Permissions</th>
                    <th>Operation</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($permissions as $permission)
                <tr>
                    <td>{{ $permission->name }}</td> 
                    <td>
                    <button type="button" data-source="{{ URL::to('permissions/'.$permission->id.'/edit') }}" class="btn btn-blue pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">Edit</button>
    
                    {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id], 'class' => 'form-horizontal partial-form-embed', 'data-container' => 'home' ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-red']) !!}
                    {!! Form::close() !!}
    
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <button type="button" data-source="{{ URL::to('permissions/create') }}" class="btn btn-green partial-button-embed" data-container="home">Add Permission</button>
    </div>    
@endsection

@section('footer')
    @include('components.embeddedtable')
@endsection