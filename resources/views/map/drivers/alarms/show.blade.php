
<script>
    var data = {!! json_encode($data) !!};
    var display = '{{$display}}';
    switch (display) {
        case '0':
            showMarkers(data);
            break;
        case '1':
            drawPolyLine(data)
            break;
        case '2':
            showHeatMap(data);
            break;
        default:
            break;
    }
    // $('#sidebarcontent').remove();
</script>