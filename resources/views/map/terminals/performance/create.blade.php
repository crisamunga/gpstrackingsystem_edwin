@extends('layouts.mapsidebar')

@section('title')
    Terminal route statistics
@endsection

@section('body')

{{ Form::open(array('url' => route('terminalmapperformance.store'), 'class' => 'partial-form-embed form-stepform', 'data-container' => 'right-sidebar')) }}

<div class="page-stepform" id="page1">

<div class="form-group {{ $errors->has('terminals') ? ' has-error' : '' }}">
    {{ Form::label('', 'Choose terminals to show in the report') }}
    <div class="table-responsive" style="margin:0px auto">
        <table class="table table-bordered">
            <colgroup>
                <col span="1" style="width: 3%; background-color: grey">
            </colgroup>
            <thead>
                <tr>
                    <th></th>
                    <th>Terminal</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($terminals as $terminal)
                    <tr>
                        <td>
                            <label class="switch">
                                {{ Form::checkbox('terminals[]',  $terminal->id ) }}
                                <span class="slider round"></span>
                            </label>
                        </td>
                        <td>{{ Form::label($terminal->display, ucfirst($terminal->display)) }}<br></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @if ($errors->has('terminals'))
        <span class="help-block">
            <strong>{{ $errors->first('terminals') }}</strong>
        </span>
    @endif
    <button type="button" class="btn btn-aqua btn-stepform" data-target="#page2" data-hide="#page1">
            Next <i class="mdi mdi-arrow-right mdi-18px"></i>
    </button>
</div>

</div>

<div class="page-stepform hidepage" id="page2">
    <div class="form-group {{ $errors->has('display') ? ' has-error' : '' }}">
        {{ Form::label('display', 'Select data display to use') }}
        {{ Form::select('display', [
            'Markers', 'Route', 'Heatmap'
        ], null, ['class' => 'form-control', 'id' => 'display']) }}
        @if ($errors->has('display'))
            <span class="help-block">
                <strong>{{ $errors->first('display') }}</strong>
            </span>
        @endif
    </div>
        
    <div class="form-group {{ $errors->has('summary') ? ' has-error' : '' }}">
        {{ Form::label('description', 'Select data summary period to use') }}
        {{ Form::select('summary', [
            '5 minutes',
            '10 minutes',
            '30 minutes',
            '1 Hour',
            '2 Hours',
            '4 Hours',
            '6 Hours',
            '12 Hours',
            '1 Day',
            '1 Week',
            '2 Weeks',
            '1 Month',
            '3 Months',
            '6 Months',
            '1 Year'
        ], 8, ['class' => 'form-control']) }}
        @if ($errors->has('summary'))
            <span class="help-block">
                <strong>{{ $errors->first('summary') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('start_date') ? ' has-error' : '' }}">
        {{ Form::label('start_date', 'Start Date') }}
        {{ Form::date('start_date', '', array('class' => 'form-control', 'required')) }}
        @if ($errors->has('start_date'))
            <span class="help-block">
                <strong>{{ $errors->first('start_date') }}</strong>
            </span>
        @endif
    </div>
    
    <div class="form-group clockpicker {{ $errors->has('start_time') ? ' has-error' : '' }}" data-placement="top" data-autoclose="true">
        {{ Form::label('start_time', 'Start Time') }}
        {{ Form::text('start_time', '', array('class' => 'form-control', 'required', 'placeholder' => 'hh:mm')) }}
        @if ($errors->has('start_time'))
            <span class="help-block">
                <strong>{{ $errors->first('start_time') }}</strong>
            </span>
        @endif
    </div>
    
    <div class="form-group {{ $errors->has('end_date') ? ' has-error' : '' }}">
        {{ Form::label('end_date', 'Stop Date') }}
        {{ Form::date('end_date', \Carbon\Carbon::now(), array('class' => 'form-control', 'required')) }}
        @if ($errors->has('end_date'))
            <span class="help-block">
                <strong>{{ $errors->first('end_date') }}</strong>
            </span>
        @endif
    </div>
    
    <div class="form-group clockpicker {{ $errors->has('end_time') ? ' has-error' : '' }}" data-placement="top" data-autoclose="true">
        {{ Form::label('end_time', 'Stop Time') }}
        {{ Form::text('end_time', \Carbon\Carbon::now()->format('H:i'), array('class' => 'form-control', 'required', 'placeholder' => 'hh:mm')) }}
        @if ($errors->has('end_time'))
            <span class="help-block">
                <strong>{{ $errors->first('end_time') }}</strong>
            </span>
        @endif
    </div>

    <div>
        <button type="button" class="btn btn-red btn-stepform" data-target="#page1" data-hide="#page2">
            <i class="mdi mdi-arrow-left mdi-18px"></i> Back
        </button>
        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Get statistics
        </button>
    </div>
</div>

{{ Form::close() }}

@endsection

@section('footer')
    @include('components.stepform')
    <script>
        $('.clockpicker').clockpicker();
        $('#sidebarcontent').on("sidebar-closed", function(e) {
            removePolyline();
        });
    </script>
@endsection