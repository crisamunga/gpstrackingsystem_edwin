@extends('layouts.mapsidebar')

@section('title')
    Route repeat
@endsection

@section('body')

{{ Form::open(array('url' => 'routerepeat', 'class' => 'partial-form-embed', 'data-container' => 'right-sidebar')) }}

<div class="form-group {{ $errors->has('start_date') ? ' has-error' : '' }}">
    {{ Form::label('start_date', 'Start Date') }}
    {{ Form::date('start_date', '', array('class' => 'form-control', 'required')) }}
    @if ($errors->has('start_date'))
        <span class="help-block">
            <strong>{{ $errors->first('start_date') }}</strong>
        </span>
    @endif
</div>

<div class="form-group clockpicker {{ $errors->has('start_time') ? ' has-error' : '' }}" data-placement="bottom" data-align="top" data-autoclose="true">
    {{ Form::label('start_time', 'Start Time') }}
    {{ Form::text('start_time', '', array('class' => 'form-control', 'required', 'placeholder' => 'hh:mm')) }}
    @if ($errors->has('start_time'))
        <span class="help-block">
            <strong>{{ $errors->first('start_time') }}</strong>
        </span>
    @endif
</div>

<div class="form-group {{ $errors->has('end_date') ? ' has-error' : '' }}">
    {{ Form::label('end_date', 'Stop Date') }}
    {{ Form::date('end_date', \Carbon\Carbon::now(), array('class' => 'form-control', 'required')) }}
    @if ($errors->has('end_date'))
        <span class="help-block">
            <strong>{{ $errors->first('end_date') }}</strong>
        </span>
    @endif
</div>

<div class="form-group clockpicker {{ $errors->has('end_time') ? ' has-error' : '' }}" data-placement="top" data-align="top" data-autoclose="true">
    {{ Form::label('end_time', 'Stop Time') }}
    {{ Form::text('end_time', \Carbon\Carbon::now()->format('H:i'), array('class' => 'form-control', 'required', 'placeholder' => 'hh:mm')) }}
    @if ($errors->has('end_time'))
        <span class="help-block">
            <strong>{{ $errors->first('end_time') }}</strong>
        </span>
    @endif
</div>

<div class="form-group {{ $errors->has('terminal') ? ' has-error' : '' }}">
    {{ Form::label('terminal', 'Terminal') }}
    <select name="terminal" class='form-control' required>
        @foreach ($terminals as $terminal)
            <option value="{{$terminal->id}}">{{$terminal->display}}</option>
        @endforeach    
    </select> 
    @if ($errors->has('terminal'))
        <span class="help-block">
            <strong>{{ $errors->first('terminal') }}</strong>
        </span>
    @endif
</div>

<button type="submit" class="btn btn-aqua">
    <i class="mdi mdi-upload mdi-18px"></i> Get route repeat data
</button>

{{ Form::close() }}

@endsection

@section('footer')
    <script>
        $('.clockpicker').clockpicker();
    </script>
@endsection