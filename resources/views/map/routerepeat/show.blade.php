@extends('layouts.mapsidebar')

@section('title')
    Route repeat
@endsection

@if (isset($data))    
    @section('body')
        <div id="route-repeat-controls">
            <div class="form-group">
                <label for="speed">Choose speed</label>
                <input type="number" name="speed" id="speed" min="0.25" max="5" step="0.25">
                <button class="btn btn-blue" id="btn-set-speed">Set speed</button>
            </div>
            <div>
                <a href="" id="btn-step-back"><span class="mdi mdi-step-backward mdi-18px btn btn-green btn-circle"></span></a>
                <a href="" id="btn-pause"><span class="mdi mdi-pause-circle-outline mdi-18px btn btn-green btn-circle"></span></a>
                <a href="" id="btn-play"><span class="mdi mdi-play-circle-outline mdi-18px btn btn-green btn-circle"></span></a>
                <a href="" id="btn-step-forward"><span class="mdi mdi-step-forward mdi-18px btn btn-green btn-circle"></span></a>
            </div>
        </div>
    @endsection

    @section('footer')
        <script>
        
            var data = {!! json_encode($data) !!};
            createRouteRepeat(data);

            $('#btn-set-speed').click(function (e) {
                e.preventDefault();
                var speed = $('#speed').val();
                routeRepeat.setSpeed(speed);
            });
            $('#btn-step-back').click(function (e) {
                e.preventDefault();
                routeRepeat.stepBack();
            });
            $('#btn-pause').click(function (e) {
                e.preventDefault();
                routeRepeat.pause();
            });
            $('#btn-play').click(function (e) {
                e.preventDefault();
                routeRepeat.resume();
                playRouteRepeat();
            });
            $('#btn-step-forward').click(function (e) {
                e.preventDefault();
                routeRepeat.stepForward();
            });
        
        </script>
    @endsection
@endif