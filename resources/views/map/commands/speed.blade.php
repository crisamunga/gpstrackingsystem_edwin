@extends('layouts.commandterminal')

@section('formfields')

<div class="form-group" {{ $errors->has('speed') ? ' has-error' : '' }}">
    {{ Form::label('speed', 'Speed to trigger alarm') }}
    <select class="form-control" id="speed" name="speed">
        <option value="010">10 km/h</option>
        <option value="020">20 km/h</option>
        <option value="030">30 km/h</option>
        <option value="040">40 km/h</option>
        <option value="050">50 km/h</option>
        <option value="060">60 km/h</option>
        <option value="080">80 km/h</option>
        <option value="100">100 km/h</option>
        <option value="120">120 km/h</option>
        <option value="150">150 km/h</option>
        <option value="180">180 km/h</option>
        <option value="200">200 km/h</option>
        <option value="220">220 km/h</option>
        <option value="250">250 km/h</option>
        <option value="300">300 km/h</option>
    </select> 
    @if ($errors->has('speed'))
        <span class="help-block">
            <strong>{{ $errors->first('speed') }}</strong>
        </span>
    @endif
</div>

@endsection