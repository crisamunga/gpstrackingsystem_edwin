@extends('layouts.commandterminal')

@section('formfields')

<div class="form-group" {{ $errors->has('distance') ? ' has-error' : '' }}">
    {{ Form::label('distance', 'Distance') }}
    <select class="form-control" id="distance" name="distance">
        <option value="0050m">every 50 m</option>
        <option value="0100m">every 100 m</option>
        <option value="0200m">every 200 m</option>
        <option value="0500m">every 500 m</option>
        <option value="0800m">every 800 m</option>
        <option value="1km">every 1 km</option>
        <option value="2km">every 2 km</option>
        <option value="5km">every 5 km</option>
        <option value="10km">every 10 km</option>
    </select> 
    @if ($errors->has('distance'))
        <span class="help-block">
            <strong>{{ $errors->first('distance') }}</strong>
        </span>
    @endif
</div>

@endsection