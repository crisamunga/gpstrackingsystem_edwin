@extends('layouts.commandterminal')

@section('formfields')

<div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('latitude_topleft') ? ' has-error' : '' }}">
            {{ Form::label('latitude_topleft', 'Top left latitude') }}
            {{ Form::number('latitude_topleft', '', array('class' => 'form-control', 'min' => '-90', 'max' => '90', 'step' => '0.000001', 'required')) }}
            @if ($errors->has('latitude_topleft'))
                <span class="help-block">
                    <strong>{{ $errors->first('latitude_topleft') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('longitude_topleft') ? ' has-error' : '' }}">
            {{ Form::label('longitude_topleft', 'Top left longitude') }}
            {{ Form::number('longitude_topleft', '', array('class' => 'form-control', 'min' => '-180', 'max' => '180', 'step' => '0.000001', 'required')) }}
            @if ($errors->has('longitude_topleft'))
                <span class="help-block">
                    <strong>{{ $errors->first('longitude_topleft') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('latitude_bottomright') ? ' has-error' : '' }}">
            {{ Form::label('latitude_bottomright', 'Bottom right latitude') }}
            {{ Form::number('latitude_bottomright', '', array('class' => 'form-control', 'min' => '-90', 'max' => '90', 'step' => '0.000001', 'required')) }}
            @if ($errors->has('latitude_bottomright'))
                <span class="help-block">
                    <strong>{{ $errors->first('latitude_bottomright') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('longitude_bottomright') ? ' has-error' : '' }}">
            {{ Form::label('longitude_bottomright', 'Bottom right longitude') }}
            {{ Form::number('longitude_bottomright', '', array('class' => 'form-control', 'min' => '-180', 'max' => '180', 'step' => '0.000001', 'required')) }}
            @if ($errors->has('longitude_bottomright'))
                <span class="help-block">
                    <strong>{{ $errors->first('longitude_bottomright') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

@endsection