@extends('layouts.commandterminal')

@section('formfields')

<div class="form-group" {{ $errors->has('duration') ? ' has-error' : '' }}">
    {{ Form::label('duration', 'Duration') }}
    <select class="form-control" id="duration" name="duration">
        <option value="10s">every 10 seconds</option>
        <option value="15s">every 15 seconds</option>
        <option value="20s">every 20 seconds</option>
        <option value="30s">every 30 seconds</option>
        <option value="1m">every 1 minute</option>
        <option value="5m">every 5 minutes</option>
        <option value="10m">every 10 minutes</option>
        <option value="30m">every 30 minutes</option>
        <option value="1h">every 1 hour</option>
        <option value="5h">every 5 hours</option>
        <option value="10h">every 10 hours</option>
        <option value="24h">every 24 hours</option>
    </select> 
    @if ($errors->has('duration'))
        <span class="help-block">
            <strong>{{ $errors->first('duration') }}</strong>
        </span>
    @endif
</div>

@endsection