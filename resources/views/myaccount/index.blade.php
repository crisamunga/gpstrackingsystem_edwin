@extends('layouts.app')

@section('content')

<div class='col-lg-4 col-lg-offset-4'>

    <h1> Edit <br> {{$user->username}}</h1>
    <hr>

    {{ Form::model($user, array('route' => array('myaccount.update'), 'method' => 'POST')) }}    

    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
        {{ Form::label('email', 'Email') }} <span>: {{$user->email}}</span>
        {{ Form::email('email', '', array('class' => 'form-control', 'placeholder' => 'New email')) }}
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
        {{ Form::label('phone', 'Phone') }} <span>: {{$user->phone}}</span>
        {{ Form::text('phone', '', array('class' => 'form-control', 'placeholder' => 'New phone number')) }}
        @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        {{ Form::label('password', 'Password') }}<br>
        {{ Form::password('password', array('class' => 'form-control','placeholder' => 'New password')) }}
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif

    </div>

    <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
        {{ Form::label('password_confirmation', 'Confirm Password') }}<br>
        {{ Form::password('password_confirmation', array('class' => 'form-control','placeholder' => 'New password confirmation')) }}

    </div>    

    <div class="form-group">
        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Submit
        </button>
    </div>

    {{ Form::close() }}    

</div>

@endsection