<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>FiveOne Socket.io</title>
</head>
<body>
    <h4 id="info"></h4>
    <div id="messages"></div>

    <script type="text/javascript" src= "{{ asset('jQuery/jquery-3.2.1.min.js') }}"></script>
    <script src="https://cdn.socket.io/socket.io-1.3.4.js"></script>
    <script>
        var socket = io.connect('http://localhost:31337');
        socket.on('connect')

        socket.on('message', function (data) {
            $( "#messages" ).append( "<p>"+data+"</p>" );
        });
        socket.on('info', function (data) {
            $( "#info" ).append( "<p>"+data+"</p>" );
        });
    </script>

</body>
</html>