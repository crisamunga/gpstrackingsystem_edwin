<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>FiveOne Socket.io</title>
</head>
<body>
    <h4 id="info"></h4>
    <div id="messages"></div>
    <div>
        <form id="command" action = "">
            <div id="imeilist"></div>
            <input id="c" type="text" autocomplete="off">            
            <input type="submit" value="Command Terminal">
        </form>
    </div>

    <div>
        <form id="setterminal" action = "">
            <input id="i" type="text" autocomplete="off">
            <input type="submit" value="Set Terminal">
        </form>
    </div>

    <script type="text/javascript" src= "{{ asset('jQuery/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src= "{{ asset('socket.io/socket.io.js') }}"></script>
    
    <script>
        var socket = io.connect('http://localhost:31340');                

        socket.on('connected', function(msg) {            
            $('#info').append(msg.msg);
        })

        socket.on('receive', function(msg) {
            $('#messages').append($('<p>').text(msg));
        });

        socket.on('device data', function(msg) {
            $('#messages').append($('<p>').text(msg));
        });

        $(init)

        function init() {
            $('#command').submit(function() {
                var imeis = [];
                var msg = new Object();
                $.each($('input[name="imei"]:checked'), function() {
                    imeis.push($(this).val());
                });
                msg.imeis = imeis;
                msg.type = 'Command';
                msg.command = $('#c').val();
                socket.emit('command', msg);
                $('#c').val('');
                return false;
            });

            $('#setterminal').submit(function() {
                var msg = new Object();
                var imei = $('#i').val();

                msg.type = 'Monitor';
                msg.imei = imei;
                socket.emit('configureMonitor', msg);
                $('#imeilist').append('<label><input type="checkbox" value="'+imei+'" name="imei">'+imei+'</label>')
                $('#i').val('');
                return false;
            });
        }                
    </script>

</body>
</html>