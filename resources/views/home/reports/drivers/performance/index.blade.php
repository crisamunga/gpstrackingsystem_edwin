@extends('layouts.partial')

@section('header')
    <h3>Create and view driver performance reports</h3>
@endsection

@section('body')
    <div class="row">
        <a id="addreport" href="{{ route('driverperformance.create') }}" class="btn btn-blue" data-container="driverreports" data-mode="prepend">
            <span class="mdi mdi-plus-circle-outline mdi-18px"></span>
            New report
        </a>
    </div>
    <div class="partial-container" id="driverreports" data-name="driverreports"></div>
@endsection

@section('footer')
    <script>
        $('#addreport').partialLoader('link');
    </script>
@endsection