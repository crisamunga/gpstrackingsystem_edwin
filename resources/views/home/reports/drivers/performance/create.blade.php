<div class="partial-container" id="{{$id}}" data-name="{{$id}}">

    @component('components.card')
        @slot('heading')
            <h4>Create a new report</h4>
        @endslot
        <div>
            {{ Form::open(array('url' => route('driverperformance.store'), 'id' => 'form1', 'class' => 'partial-form-embed', 'data-container' => $id)) }}
            
            <input type="hidden" id="reportid" name="reportid" value="{{$id}}">
            
            <div class="form-group {{ $errors->has('data-source') ? ' has-error' : '' }}">
                {{ Form::label('data-source', 'Select data to use') }}
                {{ Form::select('data-source', [
                    'Distance', 'Speed', 'Oil 1', 'Oil 2', 'Temperature', 'Working Time'
                ], null, ['class' => 'form-control']) }}
                @if ($errors->has('data-source'))
                    <span class="help-block">
                        <strong>{{ $errors->first('data-source') }}</strong>
                    </span>
                @endif
            </div>
    
            <div class="form-group {{ $errors->has('trendchart') ? ' has-error' : '' }}">
                {{ Form::label('trendchart', 'Select output to use') }}
                {{ Form::select('trendchart', [
                    'Column chart', 'Bar graph', 'Line graph', 'Area chart', 'Scatter plot', 'Table'
                ], null, ['class' => 'form-control']) }}
                @if ($errors->has('trendchart'))
                    <span class="help-block">
                        <strong>{{ $errors->first('trendchart') }}</strong>
                    </span>
                @endif
            </div>
            
            <div class="form-group {{ $errors->has('summary') ? ' has-error' : '' }}">
                {{ Form::label('description', 'Select data summary period to use') }}
                {{ Form::select('summary', [
                    '5 minutes',
                    '10 minutes',
                    '30 minutes',
                    '1 Hour',
                    '2 Hours',
                    '4 Hours',
                    '6 Hours',
                    '12 Hours',
                    '1 Day',
                    '1 Week',
                    '2 Weeks',
                    '1 Month',
                    '3 Months',
                    '6 Months',
                    '1 Year'
                ], 8, ['class' => 'form-control']) }}
                @if ($errors->has('summary'))
                    <span class="help-block">
                        <strong>{{ $errors->first('summary') }}</strong>
                    </span>
                @endif
            </div>
    
            <div class="form-group {{ $errors->has('drivers') ? ' has-error' : '' }}">
                {{ Form::label('', 'Choose drivers to show in the report') }}
    
                <div class="table-responsive" style="margin:0px auto">
                    <table class="table table-bordered">
                        <colgroup>
                            <col span="1" style="width: 3%; background-color: grey">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>Select</th>
                                <th>Driver</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($drivers as $driver)
                                <tr>
                                    <td>
                                        <label class="switch">
                                            {{ Form::checkbox('drivers[]',  $driver->id, ['id' => $driver->id] ) }}
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td>{{ Form::label($driver->id, ucfirst($driver->name)) }}<br></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                        
                </div>
    
                @if ($errors->has('drivers'))
                    <span class="help-block">
                        <strong>{{ $errors->first('drivers') }}</strong>
                    </span>
                @endif
            </div>
    
            <div class="form-group {{ $errors->has('start_date') ? ' has-error' : '' }}">
                {{ Form::label('start_date', 'Start Date') }}
                {{ Form::date('start_date', '', array('class' => 'form-control', 'required')) }}
                @if ($errors->has('start_date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('start_date') }}</strong>
                    </span>
                @endif
            </div>
            
            <div class="form-group clockpicker {{ $errors->has('start_time') ? ' has-error' : '' }}" data-placement="bottom" data-align="top" data-autoclose="true">
                {{ Form::label('start_time', 'Start Time') }}
                {{ Form::text('start_time', '', array('class' => 'form-control', 'required', 'placeholder' => 'hh:mm')) }}
                @if ($errors->has('start_time'))
                    <span class="help-block">
                        <strong>{{ $errors->first('start_time') }}</strong>
                    </span>
                @endif
            </div>
    
            <div class="form-group {{ $errors->has('end_date') ? ' has-error' : '' }}">
                {{ Form::label('end_date', 'End Date') }}
                {{ Form::date('end_date', \Carbon\Carbon::now(), array('class' => 'form-control', 'required')) }}
                @if ($errors->has('end_date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('end_date') }}</strong>
                    </span>
                @endif
            </div>
            
            <div class="form-group clockpicker {{ $errors->has('end_time') ? ' has-error' : '' }}" data-placement="bottom" data-align="top" data-autoclose="true">
                {{ Form::label('end_time', 'End Time') }}
                {{ Form::text('end_time', \Carbon\Carbon::now()->format('H:i'), array('class' => 'form-control', 'required', 'placeholder' => 'hh:mm')) }}
                @if ($errors->has('end_time'))
                    <span class="help-block">
                        <strong>{{ $errors->first('end_time') }}</strong>
                    </span>
                @endif
            </div>
            
            <div class="form-group">
                <button id="remove-{{$id}}" data-target="#{{$id}}" class="btn btn-red pull-left partial-remove-embed" style="margin-right: 3px;">
                    <i class="mdi mdi-close mdi-18px"></i> Cancel
                </button>
            
                <button type="submit" class="btn btn-aqua">
                    <i class="mdi mdi-arrow-right mdi-18px"></i> Next
                </button>
            </div>
            
            {{ Form::close() }}
            
        </div>
    
        @slot('footer')
            <script>
                $('#remove-{{$id}}').partialLoader('remove');
                $('#form1').partialLoader('form');
                $('.clockpicker').clockpicker();
            </script>
        @endslot
    
    @endcomponent
    
</div>