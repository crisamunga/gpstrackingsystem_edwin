@extends('layouts.partial')

@section('body')
<div class="partial-container" id="reportsdashboard" data-name="reportsdashboard">
    <div class="auto-load-embed" data-source="{{ route('reports.dashboard') }}" data-container="reportsdashboard" data-state="ready"></div>
</div>
<div class="partial-container" id="reports" data-name="reports"></div>
@endsection

@section('footer')

@yield('footer')
@include('components.flash')

<script>            
    $(init)
    function init() {
        $('.partial-navbar a').each(function (index, element) {
            var element = this;
            $(element).on('click',function (e) {
                $('.partial-nav li.active').removeClass('active');
                $(element).parent().addClass('active');
            });
        });
        $('.partial-container').each(function(index){
            $(this).partialLoader('container');
        });
    }
</script>

@endsection