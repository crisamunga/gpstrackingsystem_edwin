@extends('layouts.manage')

@section('side-bar')
<li class="active"><button class="btn btn-sidebarlink btn-void bg-transparent btn-fullwidth partial-button" data-container="home" data-source="{{ route('home.dashboard') }}"><span class="mdi mdi-view-dashboard mdi-18px"></span> Dashboard</button></li>

<hr class="ruler">
<li><button class="btn btn-fullwidth btn-void bg-transparent" data-toggle="collapse" data-target="#usercontent"><span class="mdi mdi-home mdi-18px"></span> Home <span class="caret"></span></button></li>
<div class="collapse in" id="usercontent">
    <ul class="nav nav-stacked nav-sidebar bg-transparent">
        @can('Manage Terminals - Client')
            <li><button type="button" class="btn btn-sidebarlink btn-void bg-transparent btn-fullwidth partial-button" data-container="home" data-source="{{ route('userterminals.index') }}"><span class="mdi mdi-car mdi-18px"></span> My terminals</button></li>
            <li><button type="button" class="btn btn-sidebarlink btn-void bg-transparent btn-fullwidth partial-button" data-container="home" data-source="{{ route('terminalgroups.index') }}"><span class="mdi mdi-google-circles-extended mdi-18px"></span> My groups</button></li>
            <li><button type="button" class="btn btn-sidebarlink btn-void bg-transparent btn-fullwidth partial-button" data-container="home" data-source="{{ route('drivers.index') }}"><span class="mdi mdi-steering mdi-18px"></span> My drivers</button></li>
        @endcan
        @can('View Reports')
            <li><button type="button" class="btn btn-sidebarlink btn-void bg-transparent btn-fullwidth partial-button" data-container="home" data-source="{{ route('reports') }}"><span class="mdi mdi-clipboard-text mdi-18px"></span> Reports</button></li>
        @endcan
        <li> <button type="button" class="btn btn-sidebarlink btn-void bg-transparent btn-fullwidth partial-button" data-container="home" data-source="{{ route('myaccount.index') }}"><span class="mdi mdi-account-card-details mdi-18px"></span> My account</button> </li>
        {{-- <li> <a href=""><span class="mdi mdi-account-alert mdi-24px"></span> Notifications</a> </li> --}}
    </ul>
</div>

@can('Access Admin Panel')
<hr class="ruler">
<li><button class="btn btn-fullwidth btn-void bg-transparent" data-toggle="collapse" data-target="#admincontent"><span class="mdi mdi-account-key mdi-18px"></span> Admin <span class="caret"></span></button></li>
<div class="collapse in" id="admincontent">
    <ul class="nav nav-stacked nav-sidebar bg-transparent">
        @can('Manage Users')
            <li><button type="button" class="btn btn-sidebarlink btn-void bg-transparent btn-fullwidth partial-button" data-container="home" data-source="{{ route('users.index') }}"><span class="mdi mdi-account mdi-18px"></span> Users</button></li>
        @endcan
        @can('Administer roles and permissions')
            <li><button type="button" class="btn btn-sidebarlink btn-void bg-transparent btn-fullwidth partial-button" data-container="home" data-source="{{ route('roles.index') }}"><span class="mdi mdi-account-multiple mdi-18px"></span> Groups</button></li>
            <li><button type="button" class="btn btn-sidebarlink btn-void bg-transparent btn-fullwidth partial-button" data-container="home" data-source="{{ route('permissions.index') }}"><span class="mdi mdi-key-variant mdi-18px"></span> Permissions</button></li>
        @endcan

        @can('Manage Terminals - Admin')
            <li><button type="button" class="btn btn-sidebarlink btn-void bg-transparent btn-fullwidth partial-button" data-container="home" data-source="{{ route('terminals.index') }}"><span class="mdi mdi-car mdi-18px"></span> Terminals</button></li>
            <li><button type="button" class="btn btn-sidebarlink btn-void bg-transparent btn-fullwidth partial-button" data-container="home" data-source="{{ route('terminalconfigs.index') }}"><span class="mdi mdi-wrench mdi-18px"></span> Configs</button></li>
        @endcan
    </ul>
</div>
@endcan

@can('Manage Server')
<hr class="ruler">
<li><button class="btn btn-fullwidth btn-void bg-transparent" data-toggle="collapse" data-target="#servercontent"><span class="mdi mdi-server mdi-18px"></span> Server <span class="caret"></span></button></li>
<div class="collapse in" id="servercontent">
    <ul class="nav nav-stacked nav-sidebar">
        <li><button type="button" class="btn btn-sidebarlink btn-void bg-transparent btn-fullwidth partial-button" data-container="home" data-source="{{ route('serverlogs.index') }}"><span class="mdi mdi-server-remove mdi-18px"></span> Web Server logs</button></li>
        <li><button type="button" class="btn btn-sidebarlink btn-void bg-transparent btn-fullwidth partial-button" data-container="home" data-source="{{ route('serverlogs.other') }}"><span class="mdi mdi-server-network mdi-18px"></span> Other logs</button></li>
    </ul>
</div>
@endcan

@endsection

@section('body')

<div class="partial-container" id="home" data-name="home">
    <div class="auto-load" data-source="{{ route('home.dashboard') }}" data-container="home" data-state="ready"></div>
</div>

@endsection

@section('footer')
<script>
    $('.partial-container').each(function(index){
        $(this).partialLoader('container');
    });
    $('.partial-link').each(function(index){            
        $(this).partialLoader('link');
    });
    $('.partial-button').each(function(index){            
        $(this).partialLoader('button');
    });
    $('.auto-load').each(function(index) {        
        $(this).partialLoader('autoload');
    });
</script>
@endsection