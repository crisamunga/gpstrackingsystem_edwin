@extends('layouts.partial')

@section('header')
<h2>Manage terminal groups</h2>
<p>Create, edit and delete terminal groups</p>
@endsection

@section('body')
    <div class="table-responsive">
        <table class="table data-table-embed" width="100%">
            <thead>
                <tr>
                    <th>Group</th>                
                    <th>Operation</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($terminal_groups as $terminal_group)
                <tr>

                    <td>{{ $terminal_group->name }}</td>
                                    
                    <td>
                    <button type="button" data-source="{{ URL::to('terminalgroups/'.$terminal_group->id.'/edit') }}" class="btn btn-blue pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">Edit</button>

                    {!! Form::open(['method' => 'DELETE', 'route' => ['terminalgroups.destroy', $terminal_group->id], 'class' => 'partial-form-embed', 'data-container' => 'home' ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-red']) !!}
                    {!! Form::close() !!}

                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
        
        <button type="button" data-source="{{ URL::to('terminalgroups/create') }}" class="btn btn-green partial-button-embed" data-container="home">Add Group</button>
    </div>    
@endsection

@section('footer')
    @include('components.embeddedtable')
@endsection