@extends('layouts.partial')

@section('body')
    <div class="col-lg-9">
        <div class="panel ">
            <div class="panel-heading">Dashboard</div>
            <div class="panel-body">
                <h1>{{ $y }}</h1>
                <h4>{{ $md }}</h4>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
        
                You are logged in!
            </div>
        </div>
        <div class="panel ">
            <div class="panel-heading">Manage</div>
            <div class="panel-body">
                <div class="partial-container" id="homelinks" data-name="homelinks">
                    @can('Terminal Control')
                    <div class="col-sm-3">
                            <a href="{{ url('/map') }}" class="btn btn-dashboard btn-dashboard-aqua pull-left" style="margin-right: 3px;">
                                <div class="mdi mdi-map-marker-multiple mdi-48px"></div> Map
                            </a>
                    </div>
                    @endcan
                    
                    @can('Manage Terminals - Client')
                    <div class="col-sm-3">
                        <button data-source="{{ route('userterminals.index') }}" class="btn btn-dashboard btn-dashboard-purple pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
                            <div class="mdi mdi-car mdi-48px"></div> Terminals
                        </button>
                    </div>

                    <div class="col-sm-3">
                        <button data-source="{{ route('terminalgroups.index') }}" class="btn btn-dashboard btn-dashboard-green pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
                            <div class="mdi mdi-google-circles-extended mdi-48px"></div> Terminal groups
                        </button>
                    </div>
                    
                    <div class="col-sm-3">
                        <button data-source="{{ route('drivers.index') }}" class="btn btn-dashboard btn-dashboard-blue pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
                            <div class="mdi mdi-steering mdi-48px"></div> Drivers
                        </button>
                    </div>
                    @endcan
                    
                    @can('View Reports')
                    <div class="col-sm-3">
                        <button data-source="{{ url('/home/reports') }}" class="btn btn-dashboard btn-dashboard-red pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
                            <div class="mdi mdi-clipboard-text mdi-48px"></div> Reports
                        </button>
                    </div>
                    @endcan
                    
                    @auth                    
                    <div class="col-sm-3">
                        <button data-source="{{ route('myaccount.index') }}" class="btn btn-dashboard btn-dashboard-green pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
                            <div class="mdi mdi-account-card-details mdi-48px"></div> My Account
                        </button>
                    </div>
                    {{-- <div class="col-sm-3">
                        <a href="{{ route('myaccount.index') }}" class="btn btn-dashboard btn-dashboard-aqua pull-left" style="margin-right: 3px;">
                            <div class="mdi mdi-account-alert mdi-48px"></div> Notifications
                        </a>
                    </div> --}}
                    @endauth
                </div>
            </div>

        </div>

        @can('Access Admin Panel')
            <div class="panel ">
                <div class="panel-heading headelem">Admin</div>
                <div class="panel-body">
                    <div class="partial-container" id="homelinks" data-name="homelinks">
                            <div class="col-sm-3">

                                <button data-source="{{ route('users.index') }}" class="btn btn-dashboard btn-dashboard pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
                                    <div class="mdi mdi-account mdi-48px"></div> Users
                                </button>
                            
                            </div>
                            
                            @can('Administer roles and permissions')
                                
                            <div class="col-sm-3">
                            
                                <button data-source="{{ route('roles.index') }}" class="btn btn-dashboard btn-dashboard-red pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
                                    <div class="mdi mdi-account-multiple mdi-48px"></div> User groups
                                </button>
                            
                            </div>
                            
                            <div class="col-sm-3">
                            
                                <button data-source="{{ route('permissions.index') }}" class="btn btn-dashboard btn-dashboard-green pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
                                    <div class="mdi mdi-key-variant mdi-48px"></div> User permissions
                                </button>
                            
                            </div>

                            @endcan
                            
                            <div class="col-sm-3">
                            
                                <button data-source="{{ route('terminals.index') }}" class="btn btn-dashboard btn-dashboard-blue pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
                                    <div class="mdi mdi-car mdi-48px"></div> Terminals
                                </button>
                            
                            </div>
                    </div>
                </div>

            </div>
        @endcan

        @can('Manage Server')
            <div class="panel ">
                <div class="panel-heading headelem">Server</div>
                <div class="panel-body">
                    <div class="partial-container" id="homelinks" data-name="homelinks">
                        <div class="col-sm-3">
                            <a data-source="{{ route('serverlogs.index') }}" class="btn btn-dashboard btn-dashboard-purple pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
                                <div class="mdi mdi-server-remove mdi-48px"></div> Web Server logs
                            </a>
                        </div>

                        <div class="col-sm-3">
                            <a data-source="{{ route('serverlogs.other') }}" class="btn btn-dashboard btn-dashboard-purple pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
                                <div class="mdi mdi-server-remove mdi-48px"></div> Other Server logs
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endcan

    </div>

    <div class="col-lg-3">
        <div class="panel">
            <div class="panel-heading">Recent Events</div>
            <div class="panel-body sim-tall">
                @if (isset($data))
                    @foreach ($data as $entry)
                        <div>
                            <h4>{{$entry['age']}}</h4>
                            <h5>{{$entry['name']}}</h5>
                            <p>{{$entry['description']}}</p>
                            <p>{{$entry['date']}}</p>
                            <hr class="ruler">
                        </div>
                    @endforeach
                @else
                    <p>No recent events</p>
                @endif
            </div>
        </div>
    </div>

@endsection

@section('footer')
@include('components.flash')
<script>
    $(function(){
        $('.partial-button-embed').each(function(index) {
            $(this).click(function(e) {
                $('.nav-sidebar .active').removeClass();
                var link = $(this).data('source');
                $('a[href = "'+link+'"]').parent().addClass('active');
            });
        });
    });
</script>

@endsection