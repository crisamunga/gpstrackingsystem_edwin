@extends('layouts.manage')

@section('side-bar')

<li class="active"><a class="partial-link" data-container="reports" href="{{ route('dashboard.reports') }}">Dashboard</a></li>
@can('View Reports')
    <li><a class="partial-link" data-container="reports" href="">Overview</a></li>
    <li><a class="partial-link" data-container="reports" href="{{ route('distancereports.index') }}">Distance</a></li>
    <li><a class="partial-link" data-container="reports" href="{{ route('speedreports.index') }}">Speed</a></li>
    <li><a class="partial-link" data-container="reports" href="{{ route('performancereports.index') }}">Oil</a></li>
    <li><a class="partial-link" data-container="reports" href="{{ route('performancereports.index') }}">Temperature</a></li>
    <li><a class="partial-link" data-container="reports" href="{{ route('performancereports.index') }}">Working time</a></li>
    <li><a class="partial-link" data-container="reports" href="{{ route('locationreports.index') }}">Route repeat</a></li>
    <li><a class="partial-link" data-container="reports" href="{{ route('performancereports.index') }}">Heat map</a></li>
    <li><a class="partial-link" data-container="reports" href="{{ route('performancereports.index') }}">Alerts</a></li>
@endcan

@endsection

@section('body')
<div class="partial-container" id="reports" data-name="reports">
    <div class="auto-load" data-source="{{ route('dashboard.reports') }}" data-container="reports" data-state="ready"></div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
            <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>

    </div>
</div>

@endsection

@section('footer')
<script>
    $('.partial-container').each(function(index){
        $(this).partialLoader('container');
    });
    $('.partial-link').each(function(index){            
        $(this).partialLoader('link');
    });
    $('.auto-load').each(function(index) {        
        $(this).partialLoader('autoload');
    });
</script>
@endsection