@extends('layouts.partial')

@section('header')
    <h3>Create and view terminal reports</h3>
@endsection

@section('body')
    <div class="row">
        <a id="addreport" href="{{ route('terminalreports.create') }}" class="btn btn-blue" data-container="terminalreports" data-mode="prepend">
            <span class="mdi mdi-plus-circle-outline mdi-18px"></span>
            New report
        </a>
    </div>
    <div class="partial-container" id="terminalreports" data-name="terminalreports"></div>
@endsection

@section('footer')
    <script>
        $('#addreport').partialLoader('link');
    </script>
@endsection