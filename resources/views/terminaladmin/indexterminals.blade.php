@extends('layouts.partial')

@section('header')
    <h2>Manage terminal</h2>
    <p>Add, edit, remove, assign or configure terminals in the system</p>    
@endsection

@section('body')
    <div class="table-responsive">
        <table class="table table-bordered table-striped data-table-embed" width="100%">
            <thead>
                <tr>
                    <th>Phone Number</th>
                    <th>IMEI</th>
                    <th>Date/Time added</th>
                    <th>Expiry Date</th>
                    <th>Model</th>
                    <th>Owner</th>
                    <th>Configuration</th>
                    <th>Operations</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Phone Number</th>
                    <th>IMEI</th>
                    <th>Date/Time added</th>
                    <th>Expiry Date</th>
                    <th>Model</th>
                    <th>Owner</th>
                    <th>Configuration</th>
                    <th>Operations</th>
                </tr>
            </tfoot>
    
            <tbody>
                @foreach ($terminals as $terminal)
                <tr>
                    <td>{{ $terminal->phone }}</td>
                    <td>{{ $terminal->imei }}</td>
                    <td>{{ $terminal->created_at->format('F d, Y h:ia') }}</td>
                    <td>{{ $terminal->expiry_date }}</td>
                    <td>{{ $terminal->model }}</td>
                    <td>{{ ($terminal->user) ? $terminal->user->username : 'Not assigned' }}</td>
                    <td>{{ ($terminal->configuration) ? $terminal->configuration->configuration_name : 'None'}}</td>
                    <td>
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle btn btn-default full-width" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            Action <span class="caret"></span>
                        </a>
    
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="{{ route('terminals.edit', $terminal->id) }}" class="btn btn-default full-width partial-link-embed" data-container="terminaladmin" style="margin-right: 3px;">Edit</a></li>
                            <li><a href="{{ route('terminals.editUser', $terminal->id) }}" class="btn btn-default full-width partial-link-embed" data-container="terminaladmin" style="margin-right: 3px;">Set Client</a></li>
                            <li>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['terminals.destroy', $terminal->id], 'class' => 'partial-form-embed', 'data-container' => 'terminaladmin' ]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-red full-width']) !!}
                                {!! Form::close() !!}
                            </li>
                        </ul>
                    </div>                                                
                    </td>
                </tr>
                @endforeach
            </tbody>
    
        </table>
        <a id="btn-addterminal" href="{{ route('terminals.create') }}" class="btn btn-green partial-link-embed" data-container="terminaladmin">Add terminal</a>
        <a id="btn-globalterminalconfig" href="" class="btn btn-red partial-link-embed" data-container="terminaladmin">Global Terminal Configuration</a>
    </div>
@endsection

@section('footer')
    @include('components.embeddedtable')
@endsection