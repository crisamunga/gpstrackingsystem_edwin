@extends('layouts.manage')

@section('side-bar')
<li class="active"><a class="partial-link" data-container="terminaladmin" href="{{ route('terminals.index') }}">Terminals</a></li>
<li><a class="partial-link" data-container="terminaladmin" href="{{ route('terminalconfigs.index') }}">Terminal COnfigurations</a></li>
@endsection

@section('body')
<div class="partial-container" id="terminaladmin" data-name="terminaladmin">
    <div class="auto-load" data-source="{{ route('terminals.index') }}" data-container="terminaladmin" data-state="ready"></div>
</div>
@endsection

@section('footer')
    <script>
        $('.partial-container').each(function(index){
            $(this).partialLoader('container');
        });
        $('.partial-link').each(function(index){            
            $(this).partialLoader('link');
        });
        $('.auto-load').each(function(index) {        
            $(this).partialLoader('autoload');
        });
    </script>
@endsection