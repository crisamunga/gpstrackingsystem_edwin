{{-- \resources\views\errors\401.blade.php --}}
@extends('layouts.app')

@section('content')
    <div class="text-center tada animated">
        <h1>401</h1>
        <h1>ACCESS DENIED</h1>
        <p class="text-center text-danger">You don't have the permissions to access this page</p>
    </div>
@endsection