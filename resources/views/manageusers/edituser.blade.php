@extends('layouts.partial')

@section('header')
    <h2>Edit user</h2>
    <h4>{{ $user->username }}</h4>
    <p>Change user details</p>    
    @if (count($errors) > 0)
        <p class="text-danger" >It seems there were errors in the input</p>
    @endif
@endsection

@section('body')

    {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT', 'class' => 'partial-form-embed', 'data-container' => 'admin')) }}{{-- Form model binding to automatically populate our fields with user data --}}

    <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
        {{ Form::label('first_name', 'First Name') }} <span>: {{$user->first_name}}</span>
        {{ Form::text('first_name', '', array('class' => 'form-control', 'placeholder' => 'New first name')) }}
        @if ($errors->has('first_name'))
            <span class="help-block">
                <strong>{{ $errors->first('first_name') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('middle_last_name') ? ' has-error' : '' }}">
        {{ Form::label('middle_last_name', 'Middle, Last Name') }} <span>: {{$user->middle_last_name}}</span>
        {{ Form::text('middle_last_name', '', array('class' => 'form-control', 'placeholder' => 'New middle, last name')) }}
        @if ($errors->has('middle_last_name'))
            <span class="help-block">
                <strong>{{ $errors->first('middle_last_name') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
        {{ Form::label('email', 'Email') }} <span>: {{$user->email}}</span>
        {{ Form::email('email', '', array('class' => 'form-control', 'placeholder' => 'New email')) }}
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
        {{ Form::label('phone', 'Phone') }} <span>: {{$user->phone}}</span>
        {{ Form::text('phone', '', array('class' => 'form-control', 'placeholder' => 'New phone number')) }}
        @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>

    <h5><b>Assign to group</b></h5>

    <div class='form-group'>
        @foreach ($roles as $role)
            {{ Form::checkbox('roles[]',  $role->id, $user->roles ) }}
            {{ Form::label($role->name, ucfirst($role->name)) }}<br>

        @endforeach
    </div>

    <div class="form-group">
        {{ Form::label('password', 'Password') }}<br>
        {{ Form::password('password', array('class' => 'form-control','placeholder' => 'New password')) }}
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif

    </div>

    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
        {{ Form::label('password', 'Confirm Password') }}<br>
        {{ Form::password('password_confirmation', array('class' => 'form-control','placeholder' => 'New password confirmation')) }}

    </div>

    <div class="form-group">
        <button data-source="{{ route('users.index') }}" class="btn btn-red pull-left partial-button-embed" data-container="admin" style="margin-right: 3px;">
            <i class="mdi mdi-close mdi-18px"></i> Cancel
        </button>
    
        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Submit
        </button>
    </div>

    {{ Form::close() }}    

@endsection