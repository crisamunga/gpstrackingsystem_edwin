@extends('layouts.manage')

@section('side-bar')

<li class="active"><a class="partial-link" data-container="admin" href="{{ route('dashboard.admin') }}">Dashboard</a></li>
@can('Manage Users')
    <li><a class="partial-link" data-container="admin" href="{{ route('users.index') }}">Users</a></li>
@endcan
@can('Administer roles and permissions')
    <li><a class="partial-link" data-container="admin" href="{{ route('roles.index') }}">User groups</a></li>
    <li><a class="partial-link" data-container="admin" href="{{ route('permissions.index') }}">User permissions</a></li>
@endcan

@can('Manage Terminals - Admin')
    <li><a class="partial-link" data-container="admin" href="{{ route('terminals.index') }}">Terminals</a></li>
    <li><a class="partial-link" data-container="admin" href="{{ route('terminalconfigs.index') }}">Terminal COnfigurations</a></li>
@endcan

@endsection

@section('body')    
<div class="partial-container" id="admin" data-name="admin">
    <div class="auto-load" data-source="{{ route('dashboard.admin') }}" data-container="admin" data-state="ready"></div>
</div>
@endsection

@section('footer')
    <script>
        $('.partial-container').each(function(index){
            $(this).partialLoader('container');
        });
        $('.partial-link').each(function(index){            
            $(this).partialLoader('link');
        });
        $('.auto-load').each(function(index) {        
            $(this).partialLoader('autoload');
        });
    </script>
@endsection