@extends('layouts.partial')

@section('header')
    <h2>Manage Permissions</h2>
    <p>Create, edit and remove user permissions from the system for access control</p>
@endsection

@section('body')
    <div class="table-responsive">
        <table class="table table-bordered table-striped data-table-embed" width="100%">
            <thead>
                <tr>
                    <th>Permissions</th>
                    <th>Operation</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($permissions as $permission)
                <tr>
                    <td>{{ $permission->name }}</td> 
                    <td>
                    <a href="{{ URL::to('permissions/'.$permission->id.'/edit') }}" class="btn btn-blue pull-left partial-link-embed" data-container="admin" style="margin-right: 3px;">Edit</a>
    
                    {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id], 'class' => 'partial-form-embed', 'data-container' => 'admin' ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-red']) !!}
                    {!! Form::close() !!}
    
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <a href="{{ URL::to('permissions/create') }}" class="btn btn-green partial-link-embed" data-container="admin">Add Permission</a>
    </div>    
@endsection

@section('footer')
    @include('components.embeddedtable')
@endsection