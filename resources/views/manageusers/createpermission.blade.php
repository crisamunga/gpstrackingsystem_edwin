@extends('layouts.partial')

@section('header')
    <h2>Create permission</h2>    
    <p>Create a new permission</p>    
    @if (count($errors) > 0)
        <p class="text-danger" >It seems there were errors in the input</p>
    @endif
@endsection

@section('body')

    {{ Form::open(array('url' => 'permissions', 'class' => 'partial-form-embed', 'data-container' => 'admin')) }}

    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', '', array('class' => 'form-control')) }}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    <div>
    @if(!$roles->isEmpty())
        <h4>Assign Permission to User <Group></Group>s</h4>

        @foreach ($roles as $role) 
            {{ Form::checkbox('roles[]',  $role->id ) }}
            {{ Form::label($role->name, ucfirst($role->name)) }}<br>

        @endforeach
    @endif
    </div>
    
    <div class="form-group">
        <button data-source="{{ route('permissions.index') }}" class="btn btn-red pull-left partial-button-embed" data-container="admin" style="margin-right: 3px;">
            <i class="mdi mdi-close mdi-18px"></i> Cancel
        </button>
    
        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Submit
        </button>
    </div>

    {{ Form::close() }}
@endsection