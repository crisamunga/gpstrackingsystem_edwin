@extends('layouts.partial')

@section('header')
    <h2>Add user</h2>    
    <p>Add a new user to the system</p>    
    @if (count($errors) > 0)
        <p class="text-danger" >It seems there were errors in the input</p>
    @endif
@endsection

@section('body')

    {{ Form::open(array('url' => 'users', 'class' => 'partial-form-embed', 'data-container' => 'admin')) }}

    <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
        {{ Form::label('first_name', 'First Name *') }}
        {{ Form::text('first_name', '', array('class' => 'form-control', 'required', 'placeholder' => 'First Name')) }}
        @if ($errors->has('first_name'))
            <span class="help-block">
                <strong>{{ $errors->first('first_name') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('middle_last_name') ? ' has-error' : '' }}">
        {{ Form::label('middle_last_name', 'Middle, Last Name *') }}
        {{ Form::text('middle_last_name', '', array('class' => 'form-control', 'required', 'placeholder' => 'Middle, Last Name')) }}
        @if ($errors->has('middle_last_name'))
            <span class="help-block">
                <strong>{{ $errors->first('middle_last_name') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
        {{ Form::label('email', 'Email *') }}
        {{ Form::email('email', '', array('class' => 'form-control', 'required','placeholder' => 'someone@example.com')) }}
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
        {{ Form::label('phone', 'Phone *') }}
        {{ Form::text('phone', '', array('class' => 'form-control', 'required' , 'placeholder' => '254700000000')) }}
        @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('roles') ? ' has-error' : '' }}">
        {{ Form::label('roles', 'User group (Optional)') }}<br>
        @foreach ($roles as $role)
            {{ Form::checkbox('roles[]',  $role->id ) }}
            {{ Form::label($role->name, ucfirst($role->name)) }}<br>
        @endforeach
        @if ($errors->has('roles'))
            <span class="help-block">
                <strong>{{ $errors->first('roles') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
        {{ Form::label('password', 'Password *') }}<br>
        {{ Form::password('password', array('class' => 'form-control', 'required', 'placeholder' => 'Type a password here')) }}
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        {{ Form::label('password', 'Confirm Password *') }}<br>
        {{ Form::password('password_confirmation', array('class' => 'form-control', 'required', 'placeholder' => 'Retype password here to confirm')) }}

    </div>

    <div class="form-group">
        <button data-source="{{ route('users.index') }}" class="btn btn-red pull-left partial-button-embed" data-container="admin" style="margin-right: 3px;">
            <i class="mdi mdi-close mdi-18px"></i> Cancel
        </button>
    
        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Submit
        </button>
    </div>

    {{ Form::close() }}    
@endsection