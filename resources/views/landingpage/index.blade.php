@extends('layouts.manage')

@section('side-bar')

<li class="active"><a class="partial-link" data-container="home" href="{{ route('dashboard.home') }}">Dashboard</a></li>
@can('Manage Terminals - Client')
    <li><a class="partial-link" data-container="home" href="{{ route('userterminals.index') }}">Terminals</a></li>
    <li><a class="partial-link" data-container="home" href="{{ route('terminalgroups.index') }}">Terminal Groups</a></li>
    <li><a class="partial-link" data-container="home" href="">Drivers</a></li>
@endcan
@can('View Reports')
    <li><a class="partial-link" data-container="home" href="{{ route('dashboard.reports') }}">Reports</a></li>
@endcan

@endsection

@section('body')

<div class="partial-container" id="home" data-name="home">
    <div class="auto-load" data-source="{{ route('dashboard.home') }}" data-container="home" data-state="ready"></div>
</div>

@endsection

@section('footer')
<script>
    $('.partial-container').each(function(index){
        $(this).partialLoader('container');
    });
    $('.partial-link').each(function(index){            
        $(this).partialLoader('link');
    });
    $('.auto-load').each(function(index) {        
        $(this).partialLoader('autoload');
    });
</script>
@endsection