@extends('layouts.manage')

@section('side-bar')
<li class="active"><a class="partial-link" data-container="terminalmanagement" href="{{ route('userterminals.index') }}">Terminals</a></li>
<li><a class="partial-link" data-container="terminalmanagement" href="{{ route('terminalgroups.index') }}">Terminal groups</a></li>
@endsection

@section('body')

<div class="partial-container" id="terminalmanagement" data-name="terminalmanagement">
    <div class="auto-load" data-source="{{ route('userterminals.index') }}" data-container="terminalmanagement" data-state="ready"></div>
</div>

@endsection

@section('footer')
    <script>
        $('.partial-container').each(function(index){
            $(this).partialLoader('container');
        });
        $('.partial-link').each(function(index){            
            $(this).partialLoader('link');
        });
        $('.auto-load').each(function(index) {        
            $(this).partialLoader('autoload');
        });
    </script>
@endsection