@extends('layouts.partial')

@section('header')
<h2>Manage terminals</h2>
<p>View and edit terminals</p>
@endsection

@section('body')
    <div class="table-responsive">    
        <table class="table table-bordered table-striped data-table-embed" width="100%">
            <thead>
                <tr>
                    <th>Description</th>
                    <th>Identifier</th>
                    <th>Group</th>
                    <th>IMEI</th>                
                    <th>Expiry Date</th>
                    <th>Model</th>
                    <th>Owner</th>
                    <th>Configuration</th>
                    <th>Operations</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Description</th>
                    <th>Identifier</th>
                    <th>Group</th>
                    <th>IMEI</th>
                    <th>Expiry Date</th>
                    <th>Model</th>
                    <th>Owner</th>
                    <th>Configuration</th>
                    <th>Operations</th>
                </tr>
            </tfoot>
    
            <tbody>
                @foreach ($terminals as $terminal)
                <tr>
                    <td>{{ ($terminal->description) ? $terminal->description : 'No description' }}</td>
                    <td>{{ ($terminal->identifier) ? $terminal->identifier : 'No identifier' }}</td>
                    <td>{{ ($terminal->terminal_group) ? $terminal->terminal_group->name : 'No group' }}</td>
                    <td>{{ $terminal->imei }}</td>
                    <td>{{ $terminal->expiry_date }}</td>
                    <td>{{ $terminal->model }}</td>
                    <td>{{ ($terminal->user) ? $terminal->user->username : 'Not assigned' }}</td>
                    <td>{{ ($terminal->configuration) ? $terminal->configuration->configuration_name : 'None'}}</td>
                    <td>
                    <a href="{{ route('userterminals.edit', $terminal->id) }}" class="btn btn-blue full-width partial-link-embed" data-container="terminalmanagement" style="margin-right: 3px;">Edit</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>    
    </div>
@endsection

@section('footer')
    @include('components.embeddedtable')
@endsection