@extends('layouts.mapsidebar')

@section('title')
    Route repeat
@endsection

@section('body')

{{ Form::open(array('url' => 'terminalcontrol/map/routerepeatdata', 'class' => 'partial-form-embed', 'data-container' => 'right-sidebar')) }}

<div class="form-group {{ $errors->has('start_date') ? ' has-error' : '' }}">
    {{ Form::label('start_date', 'Start Date') }}
    {{ Form::date('start_date', '', array('class' => 'form-control', 'required')) }}
    @if ($errors->has('start_date'))
        <span class="help-block">
            <strong>{{ $errors->first('start_date') }}</strong>
        </span>
    @endif
</div>

<div class="form-group {{ $errors->has('alarm') ? ' has-error' : '' }}">
    {{ Form::label('alarm', 'Select data to use') }}
    {{ Form::select('alarm',$alarms->pluck('description')->toArray(), null, ['class' => 'form-control']) }}
    @if ($errors->has('alarm'))
        <span class="help-block">
            <strong>{{ $errors->first('alarm') }}</strong>
        </span>
    @endif
</div>

<div class="form-group clockpicker {{ $errors->has('start_time') ? ' has-error' : '' }}" data-placement="left" data-align="top" data-autoclose="true">
    {{ Form::label('start_time', 'Start Time') }}
    {{ Form::text('start_time', '', array('class' => 'form-control', 'required', 'placeholder' => 'hh:mm')) }}
    @if ($errors->has('start_time'))
        <span class="help-block">
            <strong>{{ $errors->first('start_time') }}</strong>
        </span>
    @endif
</div>

<div class="form-group {{ $errors->has('stop_date') ? ' has-error' : '' }}">
    {{ Form::label('stop_date', 'Stop Date') }}
    {{ Form::date('stop_date', \Carbon\Carbon::now(), array('class' => 'form-control', 'required')) }}
    @if ($errors->has('stop_date'))
        <span class="help-block">
            <strong>{{ $errors->first('stop_date') }}</strong>
        </span>
    @endif
</div>

<div class="form-group clockpicker {{ $errors->has('stop_time') ? ' has-error' : '' }}" data-placement="left" data-align="top" data-autoclose="true">
    {{ Form::label('stop_time', 'Stop Time') }}
    {{ Form::text('stop_time', \Carbon\Carbon::now()->format('H:i'), array('class' => 'form-control', 'required', 'placeholder' => 'hh:mm')) }}
    @if ($errors->has('stop_time'))
        <span class="help-block">
            <strong>{{ $errors->first('stop_time') }}</strong>
        </span>
    @endif
</div>

<div class="form-group {{ $errors->has('terminal') ? ' has-error' : '' }}">
    {{ Form::label('terminal', 'Terminal') }}
    <select name="terminal" class='form-control' required>
        @foreach ($terminals as $terminal)
            <option value="{{$terminal->id}}">{{$terminal->display}}</option>
        @endforeach    
    </select> 
    @if ($errors->has('terminal'))
        <span class="help-block">
            <strong>{{ $errors->first('terminal') }}</strong>
        </span>
    @endif
</div>

{{ Form::submit('Get route repeat data', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

@endsection

@section('footer')
    <script>
        $('.clockpicker').clockpicker();
        $('#sidebarcontent').on("sidebar-closed", function(e) {
            removePolyline();
        });
    </script>
@endsection