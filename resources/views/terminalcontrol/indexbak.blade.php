@extends('layouts.controlpanel')

@section('terminallist')
    <div>
        <div class="table-responsive">
            <table class="table table-bordered table-striped data-table-control" width="100%">
                <thead>
                    <tr>
                        <th></th>                        
                        <th>Terminal</th>
                        <th>Description</th>
                        <th>Identifier</th>
                        <th>IMEI</th>
                        <th>Group</th>
                        <th>Owner</th>
                        <th>Configuration</th>
                        <th></th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>                                            
                        <th>Terminal</th>
                        <th>Description</th>
                        <th>Identifier</th>
                        <th>IMEI</th>
                        <th>Group</th>
                        <th>Owner</th>
                        <th>Configuration</th>       
                        <th></th>                 
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($terminals as $terminal)
                    <tr class="terminal-entry" data-imei="{{ $terminal->imei }}">                        
                        <td><span class="glyphicon glyphicon-signal offline" id="{{ $terminal->imei }}-status"></span></td>
                        <td>{{ $terminal->display }}</td>
                        <td>{{ ($terminal->description) ? $terminal->description : 'No description'}}</td>
                        <td>{{ ($terminal->identifier) ? $terminal->identifier : 'No Identifier'}}</td>
                        <td>{{ ($terminal->imei) ? $terminal->imei : 'No IMEI'}}</td>
                        <td>{{ ($terminal->terminal_group) ? $terminal->terminal_group->name : 'Ungrouped'}}</td>
                        <td>{{ ($terminal->user) ? $terminal->user->username : 'Not assigned' }}</td>
                        <td>{{ ($terminal->configuration) ? $terminal->configuration->configuration_name : 'None'}}</td>                        
                        <td></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('display')

@endsection

@section('footer')
<script>
    $('.data-table-control').DataTable( {
        order: [[5, 'asc']],
        rowGroup: {
            dataSrc: 5
        },
        responsive: {
            details: {
                type: 'column',
                target: -1
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   -1
        } ],        
        select: true
    } );    
</script>

<style>
    .row div {
        width: 100%;
        padding: 0;
        margin: 0;
    }

    .group td {
        background-color: #333;
        color: #ddf;
        border-left: 2px solid #00f; 
    }
</style>

<script type="text/javascript" src= "{{ asset('socket.io/socket.io.js') }}"></script>
<script type="text/javascript" src= "{{ asset('js/app/socketmaster.js') }}"></script>

@endsection