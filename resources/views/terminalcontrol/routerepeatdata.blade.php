@extends('layouts.mapsidebar')

@section('title')
    Route repeat
@endsection
    
@section('body')
    <div>
        @foreach ($summary as $key => $value)
            <p><strong>{{$key}}: </strong>{{$value}}</p>
        @endforeach
    </div>
@endsection

@section('footer')
    <script>
        var path = [];
        @foreach ($locationdata as $location)
            var coord = {lat: "{{$location['latitude']}}" * 1, lng: "{{$location['longitude']}}" * 1};
            path.push(coord);
        @endforeach
        if (path.length > 0) drawPolyLine(path);
        $('#sidebarcontent').on("sidebar-closed", function(e) {
            removePolyline();
        });
    </script>
@endsection