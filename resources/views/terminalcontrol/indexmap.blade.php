<div class="mapcontent-holder partial-container-embed" id="right-sidebar" data-name="right-sidebar">
    @include('terminalcontrol.routerepeat')
</div>
<div class="container-fluid" id="map">

</div>
<script type="text/javascript" src= "{{ asset('js/app/mapmaster.js') }}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAH_JEW5yzDXLdpaXVLzx01rtvPu3j_8W4&callback=initMap"></script>
<script>
    $('.partial-container-embed').each(function(index){
        $(this).partialLoader('container');
    });
</script>