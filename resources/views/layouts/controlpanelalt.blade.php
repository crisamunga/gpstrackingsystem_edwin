@extends('layouts.app')

@section('content')
<script type="text/javascript" src= "{{ asset('js/plugins/jquery/dataTables.select.min.js') }}"></script>
<script type="text/javascript" src= "{{ asset('js/plugins/jquery/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src= "{{ asset('js/plugins/jquery/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src= "{{ asset('js/plugins/jquery/dataTables.rowGroup.min.js') }}"></script>
<link href="{{ asset('css/select.dataTables.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/buttons.dataTables.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/responsive.dataTables.min.css') }}" rel="stylesheet">
<div>
    <div class="navbar-inverse">
        <div class="navbar-header">
            <button type="button" class="pull-right navbar-toggle" data-toggle="collapse" data-target="#control-menu">
                <span class="sr-only">Toggle menu bar</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>   
            <a class="pull-left btn" data-toggle="collapse" data-target="#side-bar">
                <span data-toggle="tooltip" title="Hide or show terminal list" data-placement="bottom">
                    <span class="sr-only">Toggle terminal list</span>
                    <span class="mdi mdi-car-connected mdi-24px mdi-light"></span>
                <span>
            </a>
            <a class="pull-left btn" data-toggle="collapse" data-target="#navbar">
                <span data-toggle="tooltip" title="Hide or show navigation bar" data-placement="bottom">
                        <span class="sr-only">Toggle navigation bar</span>
                    <span class="mdi mdi-fullscreen mdi-24px mdi-light"></span>
                <span>
            </a>
            <!-- Branding Image -->
            <div class="navbar-brand">
                Control Panel
            </div>        
        </div>
        <div class="collapse navbar-collapse" id="control-menu">
            <ul class="nav navbar-nav nav-inverse">
                <li class="active"><a class="menubtn partial-link" href="">Terminal List</a></li>
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Terminal Control<b class="caret"></b></a>
                    <ul class="dropdown-menu multi-level">
                        <li class="dropdown-submenu">
                            <a href="javascript:alert('here');" class="dropdown-toggle" data-toggle="dropdown">Terminal time</a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu">
                                    <a href="" class="dropdown-toggle" data-toggle="dropdown">+</a>
                                    <ul class="dropdown-menu multi-level">
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+0.5" href="">+0.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+1.0" href="">+1.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+1.5" href="">+1.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+2.0" href="">+2.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+2.5" href="">+2.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+3.0" href="">+3.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+3.5" href="">+3.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+4.0" href="">+4.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+4.5" href="">+4.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+5.0" href="">+5.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+5.5" href="">+5.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+6.0" href="">+6.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+6.5" href="">+6.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+7.0" href="">+7.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+7.5" href="">+7.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+8.0" href="">+8.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+8.5" href="">+8.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+9.0" href="">+9.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+9.5" href="">+9.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+10.0" href="">+10.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+10.5" href="">+10.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+11.0" href="">+11.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+11.5" href="">+11.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="+12.0" href="">+12.0</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="" class="dropdown-toggle" data-toggle="dropdown">-</a>
                                    <ul class="dropdown-menu multi-level">
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-0.5" href="">-0.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-1.0" href="">-1.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-1.5" href="">-1.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-2.0" href="">-2.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-2.5" href="">-2.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-3.0" href="">-3.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-3.5" href="">-3.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-4.0" href="">-4.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-4.5" href="">-4.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-5.0" href="">-5.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-5.5" href="">-5.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-6.0" href="">-6.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-6.5" href="">-6.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-7.0" href="">-7.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-7.5" href="">-7.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-8.0" href="">-8.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-8.5" href="">-8.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-9.0" href="">-9.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-9.5" href="">-9.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-10.0" href="">-10.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-10.5" href="">-10.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-11.0" href="">-11.0</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-11.5" href="">-11.5</a></li>
                                        <li><a class="terminal-command" data-name="set-time-zone" data-parameter="-12.0" href="">-12.0</a></li>
                                    </ul>
                                </li>
                                <li><a class="terminal-command" data-name="set-time-zone" data-parameter="0" href="">0</a></li>
                            </ul>                    
                        </li>
                        <li class="dropdown-submenu">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown">Track</a>
                            <ul class="dropdown-menu multi-level">
                                <li class="dropdown-submenu">
                                    <a href="" class="dropdown-toggle" data-toggle="dropdown">By Time</a>
                                    <ul class="dropdown-menu multi-level">
                                        <li><a class="terminal-command" data-name="track-by-time" data-parameter="10s" href="">10 seconds</a></li>
                                        <li><a class="terminal-command" data-name="track-by-time" data-parameter="15s" href="">15 seconds</a></li>
                                        <li><a class="terminal-command" data-name="track-by-time" data-parameter="20s" href="">20 seconds</a></li>
                                        <li><a class="terminal-command" data-name="track-by-time" data-parameter="30s" href="">30 seconds</a></li>
                                        <li><a class="terminal-command" data-name="track-by-time" data-parameter="1m" href="">1 minutes</a></li>
                                        <li><a class="terminal-command" data-name="track-by-time" data-parameter="5m" href="">5 minutes</a></li>
                                        <li><a class="terminal-command" data-name="track-by-time" data-parameter="10m" href="">10 minutes</a></li>
                                        <li><a class="terminal-command" data-name="track-by-time" data-parameter="30m" href="">30 minutes</a></li>
                                        <li><a class="terminal-command" data-name="track-by-time" data-parameter="1h" href="">1 hours</a></li>
                                        <li><a class="terminal-command" data-name="track-by-time" data-parameter="5h" href="">5 hours</a></li>
                                        <li><a class="terminal-command" data-name="track-by-time" data-parameter="10h" href="">10 hours</a></li>
                                        <li><a class="terminal-command" data-name="track-by-time" data-parameter="24h" href="">24 hours</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="" class="dropdown-toggle" data-toggle="dropdown">By distance</a>
                                    <ul class="dropdown-menu multi-level">
                                        <li><a class="terminal-command" data-name="track-by-distance" data-parameter="0050m" href="">50 meters</a></li>
                                        <li><a class="terminal-command" data-name="track-by-distance" data-parameter="0100m" href="">100 meters</a></li>
                                        <li><a class="terminal-command" data-name="track-by-distance" data-parameter="0200m" href="">200 meters</a></li>
                                        <li><a class="terminal-command" data-name="track-by-distance" data-parameter="0500m" href="">500 meters</a></li>
                                        <li><a class="terminal-command" data-name="track-by-distance" data-parameter="0800m" href="">800 meters</a></li>
                                        <li><a class="terminal-command" data-name="track-by-distance" data-parameter="1km" href="">1 KM</a></li>
                                        <li><a class="terminal-command" data-name="track-by-distance" data-parameter="2km" href="">2 KM</a></li>
                                        <li><a class="terminal-command" data-name="track-by-distance" data-parameter="5km" href="">5 KM</a></li>
                                        <li><a class="terminal-command" data-name="track-by-distance" data-parameter="10km" href="">10 KM</a></li>
                                    </ul>
                                </li>
                                <li><a class="terminal-command" data-name="single-track" data-parameter="" href="">Single track</a></li>
                                <li><a class="terminal-command" data-name="cancel-track" data-parameter="" href="">Cancel</a></li>                            
                            </ul>
                        </li>
                        <li class="dropdown-submenu">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown">Overspeed alarm</a>
                            <ul class="dropdown-menu multi-level">
                                <li><a class="terminal-command" data-name="overspeed-alarm" data-parameter="010" href="">10 km/h</a></li>
                                <li><a class="terminal-command" data-name="overspeed-alarm" data-parameter="020" href="">20 km/h</a></li>
                                <li><a class="terminal-command" data-name="overspeed-alarm" data-parameter="030" href="">30 km/h</a></li>
                                <li><a class="terminal-command" data-name="overspeed-alarm" data-parameter="040" href="">40 km/h</a></li>
                                <li><a class="terminal-command" data-name="overspeed-alarm" data-parameter="050" href="">50 km/h</a></li>
                                <li><a class="terminal-command" data-name="overspeed-alarm" data-parameter="060" href="">60 km/h</a></li>
                                <li><a class="terminal-command" data-name="overspeed-alarm" data-parameter="080" href="">80 km/h</a></li>
                                <li><a class="terminal-command" data-name="overspeed-alarm" data-parameter="100" href="">100 km/h</a></li>
                                <li><a class="terminal-command" data-name="overspeed-alarm" data-parameter="120" href="">120 km/h</a></li>
                                <li><a class="terminal-command" data-name="overspeed-alarm" data-parameter="150" href="">150 km/h</a></li>
                                <li><a class="terminal-command" data-name="overspeed-alarm" data-parameter="180" href="">180 km/h</a></li>
                                <li><a class="terminal-command" data-name="overspeed-alarm" data-parameter="200" href="">200 km/h</a></li>
                                <li><a class="terminal-command" data-name="overspeed-alarm" data-parameter="220" href="">220 km/h</a></li>
                                <li><a class="terminal-command" data-name="overspeed-alarm" data-parameter="250" href="">250 km/h</a></li>
                                <li><a class="terminal-command" data-name="overspeed-alarm" data-parameter="300" href="">300 km/h</a></li>
                            </ul>
                        </li>
                        <li class="dropdown-submenu">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown">Move alarm</a>
                            <ul class="dropdown-menu multi-level">
                                <li><a class="terminal-command" data-name="set-movement-alarm" data-parameter="" href="">Setting</a></li>
                                <li><a class="terminal-command" data-name="cancel-movement-alarm" data-parameter="" href="">Cancel</a></li>                         
                            </ul>
                        </li>
                        <li class="dropdown-submenu">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown">Vibration alarm</a>
                            <ul class="dropdown-menu multi-level">
                                <li><a class="terminal-command" data-name="set-shock-alarm" data-parameter="" href="">Open</a></li>
                                <li><a class="terminal-command" data-name="cancel-shock-alarm" data-parameter="" href="">Close</a></li>                         
                            </ul>
                        </li>
                        <li><a class="terminal-command" data-name="cancel-alarm" data-parameter="" href="">Stop alarm</a></li>
                        <li class="dropdown-submenu">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown">Engine control</a>
                            <ul class="dropdown-menu multi-level">
                                <li><a class="terminal-command" data-name="cut-off-oil-and-power" data-parameter="" href="">Stop engine</a></li>
                                <li><a class="terminal-command" data-name="resume-oil-and-power" data-parameter="" href="">Resume engine</a></li>                         
                            </ul>
                        </li>
                        <li><a class="terminal-command" data-name="set-arm" data-parameter="" href="">Arm</a></li>
                        <li><a class="terminal-command" data-name="set-disarm" data-parameter="" href="">Disarm</a></li>
                        <li class="dropdown-submenu">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown">Save GPRS Setting</a>
                            <ul class="dropdown-menu multi-level">
                                <li><a class="terminal-command" data-name="set-less-gprs" data-parameter="" href="">Open</a></li>
                                <li><a class="terminal-command" data-name="cancel-less-gprs" data-parameter="" href="">Close</a></li>                         
                            </ul>
                        </li>
                        <li class="dropdown-submenu">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown">Geo fence</a>
                            <ul class="dropdown-menu multi-level">
                                <li><a class="terminal-command" data-name="set-geo-fence" data-parameter="" href="">Setting</a></li>
                                <li><a class="terminal-command" data-name="cancel-geo-fence" data-parameter="" href="">Cancel</a></li>                         
                            </ul>
                        </li>
                        <li><a class="terminal-command" data-name="remote-start" data-parameter="" href="">Remote start</a></li>
                        <li><a class="terminal-command" data-name="request-photo" data-parameter="" href="">Request photo</a></li>
                        <li class="dropdown-submenu">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown">Upload data</a>
                            <ul class="dropdown-menu multi-level">
                                <li><a class="terminal-command" data-name="data-load" data-parameter="" href="">Upload</a></li>
                                <li><a class="terminal-command" data-name="cancel-data-load" data-parameter="" href="">Cancel</a></li>                         
                            </ul>
                        </li>
                        <li class="dropdown-submenu">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown">OBD Settings</a>
                            <ul class="dropdown-menu multi-level">
                                <li><a class="terminal-command" data-name="obd-setting" data-parameter="cancel" href="">Turn off OBD</a></li>
                                <li><a class="terminal-command" data-name="obd-setting" data-parameter="multiple" href="">Follow multiple tracking</a></li>
                                <li><a class="terminal-command" data-name="obd-setting" data-parameter="once" href="">Follow once tracking</a></li>                         
                            </ul>
                        </li>
                        <li><a class="terminal-command" data-name="dispatch-management" data-parameter="" href="">Dispatch management</a></li>
                        <li><a class="terminal-command" data-name="led-advertising" data-parameter="" href="">LED Advertising</a></li>                                        
                    </ul>
                </li>
                <li class="dropdown">                                
                    <a href="" class="dropdown-toggle" data-toggle="dropdown">Statistics
                    <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li> <a class="partial-link contained-right" data-container="right-sidebar" href="{{ route('routerepeat.create') }}">Route repeat</a></li>
                        <li> <a class="partial-link contained-right" data-container="right-sidebar" href="{{ route('terminalmapperformance.create') }}" href="">Terminal route statistics</a> </li>
                        <li> <a class="partial-link contained-right" data-container="right-sidebar" href="{{ route('terminalmapalarms.create') }}" href="">Terminal event statistics</a> </li>
                        <li> <a class="partial-link contained-right" data-container="right-sidebar" href="{{ route('drivermapperformance.create') }}" href="">Driver route statistics</a> </li>
                        <li> <a class="partial-link contained-right" data-container="right-sidebar" href="{{ route('drivermapalarms.create') }}" href="">Driver event statistics</a> </li>
                    </ul>
                </li>
            </ul>
            @yield('menu')
            <ul class="navbar-right">
                    @yield('menu-right')
            </ul>
        </div>
    </div>
</div>

<div>
    <div class="collapse in col-md-2 control-container sidebar" id="side-bar">
        @yield('terminallist')
    </div>

    <div class="control-container container-fluid">
        @yield('display')
    </div>
</div>

<script>
    $("[rel='tooltip']").tooltip();
    $('[data-toggle="tooltip"]').tooltip();
    $('.partial-link').each(function(index){
        $(this).partialLoader('link');
    });
</script>

<script type="text/javascript" src= "{{ asset('socket.io/socket.io.js') }}"></script>
@yield('footer')

@endsection