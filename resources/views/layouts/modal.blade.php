<div class="container-fluid">
        @include('components.flash')
    <div>
        @yield('header')
    </div>
    
    <div class="container-fluid">
            @yield('body')
    </div>
        
    <div>
        <script>
            $('.partial-link-modal').each(function(index){
                $(this).partialLoader('link');
            });
            $('.partial-form-modal').each(function(index) {        
                $(this).partialLoader('form');
            });
            $('.partial-button-modal').each(function(index) {        
                $(this).partialLoader('button');
            });
            $('.auto-load-modal').each(function(index) {        
                $(this).partialLoader('autoload');
            });        
        </script>
        @yield('footer')
    </div>
</div>