@extends('layouts.partial')

@section('header')
<h2 class="text-center">@yield('name')</h2>
@endsection

@yield('body')

@section('footer')
<script>
    $(function(){
        $('.partial-button-embed').each(function(index) {
            $(this).click(function(e) {
                $('.manage-sidebar .active').removeClass();
                var link = $(this).data('source');
                $('a[href = "'+link+'"]').parent().addClass('active');
                $('a[href = "'+link+'"]').scrollTop();
            });
        });
    });
</script>
@yield('footer')
@endsection