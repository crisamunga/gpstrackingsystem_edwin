<div class="container-fluid">
        @include('components.flash')
    <div>
        @yield('header')
    </div>
    
    <div class="container-fluid">
            @yield('body')
    </div>
        
    <div>
        <script>
            $('.subpartial.partial-link-embed').each(function(index){
                $(this).partialLoader('link');
            });
            $('.subpartial.partial-form-embed').each(function(index) {        
                $(this).partialLoader('form');
            });
            $('.subpartial.partial-button-embed').each(function(index) {        
                $(this).partialLoader('button');
            });
            $('.subpartial.auto-load-embed').each(function(index) {        
                $(this).partialLoader('autoload');
            });        
        </script>
        @yield('footer')
    </div>
</div>