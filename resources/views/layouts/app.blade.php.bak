<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'GPS Tracking System') }}</title>

    <!-- Scripts -->    
    <script type="text/javascript" src= "{{ asset('jQuery/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src= "{{ asset('Bootstrap/js/bootstrap.min.js') }}"></script>    
    <script type="text/javascript" src= "{{ asset('js/app/partiallinks.js') }}"></script>
    <script type="text/javascript" src= "{{ asset('js/app/myapp.js') }}"></script>
    <script type="text/javascript" src= "{{ asset('js/plugins/jquery/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src= "{{ asset('js/plugins/jquery/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src= "{{ asset('js/plugins/jquery/bootstrap-notify-master/bootstrap-notify.min.js') }}"></script>    
    <script type="text/javascript" src= "{{ asset('clockpicker/bootstrap-clockpicker.min.js') }}"></script>    
    <script>
        $(function(){
            $('ul#nav li a').removeClass();
            var pathname = window.location; 
            $('ul.nav a[href="'+ pathname +'"]').parent().addClass('active');                
        });
    </script>
    
    <!-- Styles -->        
    <link rel="stylesheet" href= "{{ asset('Bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href= "{{ asset('Bootstrap/css/bootstrap-theme.min.css') }}">
    <link rel="stylesheet" href= "{{ asset('MDI/css/materialdesignicons.min.css') }}">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">    
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">  
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">    
    <link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">    
    <link href="{{ asset('clockpicker/bootstrap-clockpicker.min.css') }}" rel="stylesheet">  

</head>
<body>
    <div id="app">
        <div id="navbar" class="collapse in">

            <nav class="navbar navbar-static-top">
                <div class="container-fluid">
                    <div class="navbar-header">

                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/') }}">
                            {{-- config('app.name', 'Laravel') --}}
                            GPS Tracking System
                        </a>
                    </div>

                    <div class="collapse navbar-collapse text-center" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav">
                            @auth
                            <li><a href="{{ url('/home') }}"><span class="mdi mdi-home mdi-18px"></span> Home</a></li>
                            @can('Terminal Control')
                            <li><a href="{{ url('/map') }}"><span class="mdi mdi-map-marker-multiple mdi-18px"></span> Map</a></li>
                            @endcan

                            @can('Access Admin Panel')
                            <li><a href="{{ url('/admin') }}"><span class="mdi mdi-account-key mdi-18px"></span> Admin</a></li>
                            @endcan
                            
                            @can('Manage Server')
                            <li><a href="{{ url('') }}"><span class="mdi mdi-server mdi-18px"></span> Server</a></li>
                            @endcan
                            
                            @endauth
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @guest
                                <li><a href="{{ route('login') }}"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>
                            @else
                                <li class="dropdown">                                
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                        <span class="glyphicon glyphicon-user"></span>
                                        {{ Auth::user()->first_name }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li> <a href="{{ route('myaccount.index') }}">My Account</a> </li>
                                        <li>
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>        

        </div>
        
        @yield('content')
    </div>
        
</body>
</html>
