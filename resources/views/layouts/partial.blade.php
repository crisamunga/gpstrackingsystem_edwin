<div class="container-fluid slideInDown animated">
        @include('components.flash')
    <div>
        @yield('header')
    </div>
    
    <div class="container-fluid">
            @yield('body')
    </div>
        
    <div>
        <script>
            $('.partial-link-embed').each(function(index){
                $(this).partialLoader('link');
            });
            $('.partial-form-embed').each(function(index) {        
                $(this).partialLoader('form');
            });
            $('.partial-button-embed').each(function(index) {        
                $(this).partialLoader('button');
            });
            $('.auto-load-embed').each(function(index) {        
                $(this).partialLoader('autoload');
            });        
        </script>
        @yield('footer')
    </div>
</div>