@extends('layouts.app')

@section('brand')
    <button type="button" class="pull-right" style="background-color: transparent; border: none; padding: 0px 6px;" data-toggle="collapse" data-target="#side-bar">
        <span data-toggle="tooltip" title="Hide or show terminal list" data-placement="bottom">
            <span class="mdi mdi-notification-clear-all mdi-24px mdi-light" ></span>
            <span class="sr-only">Toggle terminal list</span>
        </span>
    </button>
@endsection

@section('content')
    
    <script type="text/javascript" src= "{{ asset('js/plugins/jquery/dataTables.select.min.js') }}"></script>
    <script type="text/javascript" src= "{{ asset('js/plugins/jquery/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src= "{{ asset('js/plugins/jquery/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src= "{{ asset('js/plugins/jquery/dataTables.rowGroup.min.js') }}"></script>
    <link href="{{ asset('css/select.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/buttons.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.dataTables.min.css') }}" rel="stylesheet">

    <div>
        <div class="collapse in col-md-3 bg-transparent floating-panel slideInLeft animated" id="side-bar">
            <div class="panel">
                <div class="panel-heading text-right">
                    <h4 class="pull-left">
                        Terminal List
                    </h4>
                    <span class="dropdown">
                        <button class="btn btn-void dropdown-toggle" type="button" id="control-menu" data-toggle="dropdown">
                            <span class="mdi mdi-settings mdi-18px"></span>
                            Control
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu bg-light">
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"settimezone") }}">Set timezone</button></li>
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"trackbytime") }}">Track by time</button></li>
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"trackbydistance") }}">Track by distance</button></li>
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"canceltrack") }}">Cancel track</button></li>
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"setoverspeedalarm") }}">Set overspeed alarm</button></li>
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"setmovementalarm") }}">Set move alarm</button></li>
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"cancelalarm") }}">Cancel alarm</button></li>
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"cutoffoilandpower") }}">Cut off engine</button></li>
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"resumeoilandpower") }}">Resume engine</button></li>
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"arm") }}">Arm</button></li>
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"disarm") }}">Disarm</button></li>
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"setgeofence") }}">Set geofence</button></li>
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"cancelgeofence") }}">Cancel geofence</button></li>
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"uploaddata") }}">Upload data</button></li>
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"canceldataupload") }}">Cancel data upload</button></li>
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"setlessgprs") }}">Set less GPRS</button></li>
                            <li> <button class="btn btn-default btn-fullwidth partial-button" data-container="modal-content" data-source="{{ route('commandterminal.show',"requestPhoto") }}">Request photo</button></li>
                        </ul>
                    </span>
                </div>
                <div class="panel-body"> @yield('terminallist') </div>
            </div>
        </div>
    
        <div class="col-sm-9">
            <div class="partial-container" id="display" data-name="display">
                <div class="slideInDown animated">        
                    <div class="panel" style="position: relative">
                        <div class="text-center panel-heading" style="position: relative; z-index: 1000">
                            <div class="pull-left">
                                <h4>Map</h4>
                            </div>
                            <div class="slideInUp animated text-right">
                                <span class="dropdown">
                                    <button class="btn btn-void dropdown-toggle" type="button" id="control-menu" data-toggle="dropdown">
                                        <span class="mdi mdi-chart-areaspline mdi-18px"></span>
                                        Statistics
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu bg-light">
                                        <li> <button class="btn btn-default btn-fullwidth partial-button contained-right" data-container="right-sidebar" data-source="{{ route('routerepeat.create') }}">Route repeat</button> </li>
                                        <li> <button class="btn btn-default btn-fullwidth partial-button contained-right" data-container="right-sidebar" data-source="{{ route('terminalmapperformance.create') }}">Terminal route statistics</button> </li>
                                        <li> <button class="btn btn-default btn-fullwidth partial-button contained-right" data-container="right-sidebar" data-source="{{ route('terminalmapalarms.create') }}">Terminal event statistics</button> </li>
                                        <li> <button class="btn btn-default btn-fullwidth partial-button contained-right" data-container="right-sidebar" data-source="{{ route('drivermapperformance.create') }}">Driver route statistics</button> </li>
                                        <li> <button class="btn btn-default btn-fullwidth partial-button contained-right" data-container="right-sidebar" data-source="{{ route('drivermapalarms.create') }}">Driver event statistics</button> </li>
                                    </ul>
                                </span>
                                <form class="form-inline" id="search-form" style="display: inline;">
                                    <div class="form-group">
                                        <input class="form-control" type="number" step="0.000001" min="-90" max="90" placeholder="Latitude" name="search-latitude" id="search-latitude">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="number" step="0.000001" min="-180" max="180" placeholder="Longitude" name="search-longitude" id="search-longitude">
                                    </div>
                                    <div class="form-group">
                                        <button class="btn form-control btn-blue" type="submit" title="Find location by coordinates">
                                            <span class="mdi mdi-magnify mdi-18px"></span>
                                            Search
                                        </button>
                                    </div>
                                </form>
                                <button class="btn btn-aqua" style="vertical-align: middle; text-align: center;"
                                    onclick="event.preventDefault();resetMap();;">
                                    <span class="mdi mdi-map mdi-18px"></span>
                                    Clear map
                                </button>
                            </div>
                        </div>
                        <div class="panel-body" style="position: relative; z-index: 0">
                            <div class="col-sm-3 pull-right sim-tall partial-container floating-panel bg-light" id="right-sidebar" data-name="right-sidebar"></div>
                            @yield('display')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <div>
    
        </div>
    </div>

    <script>
        $("[rel='tooltip']").tooltip();
        $('[data-toggle="tooltip"]').tooltip();
        $('.partial-link').each(function(index){
            $(this).partialLoader('link');
        });
        $('.partial-button').each(function(index){
            $(this).partialLoader('button');
        });
    </script>
    
    <script type="text/javascript" src= "{{ asset('socket.io/socket.io.js') }}"></script>
    @yield('footer')
@endsection