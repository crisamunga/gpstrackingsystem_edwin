<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'GPS Tracking System') }}</title>

    <!-- Scripts -->    
    <script type="text/javascript" src= "{{ asset('jQuery/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src= "{{ asset('Bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src= "{{ asset('js/plugins/jquery/bootstrap-notify-master/bootstrap-notify.min.js') }}"></script> 
    <script type="text/javascript" src= "{{ asset('js/app/partiallinks.js') }}"></script>
    <script type="text/javascript" src= "{{ asset('js/app/myapp.js') }}"></script>
    <script type="text/javascript" src= "{{ asset('js/plugins/jquery/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src= "{{ asset('js/plugins/jquery/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src= "{{ asset('clockpicker/bootstrap-clockpicker.min.js') }}"></script>    
    <script type="text/javascript" src= "{{ asset('js/sockets.js') }}"></script>
    <script>
        $(function(){
            $('ul#nav li a').removeClass();
            var pathname = window.location; 
            $('ul.nav a[href="'+ pathname +'"]').parent().addClass('active');                
        });
    </script>
    
    <!-- Styles -->        
    <link rel="stylesheet" href= "{{ asset('Bootstrap/css/bootstrap.min.css') }}">
    {{-- <link rel="stylesheet" href= "{{ asset('Bootstrap/css/bootstrap-theme.min.css') }}"> --}}
    <link rel="stylesheet" href= "{{ asset('MDI/css/materialdesignicons.min.css') }}">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">    
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet">   --}}
    @yield('html-head')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">    
    <link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">    
    <link href="{{ asset('clockpicker/bootstrap-clockpicker.min.css') }}" rel="stylesheet">

</head>
<body>
    <div class="cover-image-container bg-light">
        <img src="{{asset('img/cover.jpg')}}" alt="Satellite" class="cover-image">
    </div>
    <div id="app">
        <div id="navbar" class="collapse in">
            <nav class="navbar navbar-static-top bg-dark slideInRight animated">
                <div class="container-fluid">
                    <div class="navbar-header">

                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <div class="pull-left">
                            @yield('brand')
                        </div>

                        <div class="navbar-brand">
                            <a href="/">GPS Tracking System</a>
                        </div>

                    </div>

                    <div class="collapse navbar-collapse text-center" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav">
                            @auth
                            <li><a href="{{ url('/home') }}"><span class="mdi mdi-home mdi-18px"></span> Home</a></li>
                            @can('Terminal Control')
                            <li><a href="{{ url('/map') }}"><span class="mdi mdi-map-marker-multiple mdi-18px"></span> Map</a></li>
                            @endcan

                            @endauth
                        </ul>

                        @yield('navigation')

                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="" title="Having a problem?"><span class="mdi mdi-help mdi-18px"></span> Help</a>
                            </li>
                            <!-- Authentication Links -->
                            @guest
                                <li><a href="{{ route('login') }}"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>
                            @else
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                        <span class="mdi mdi-logout mdi-18px"></span>
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>        

        </div>
        
        <div id="loading-screen" class="loader fadeIn" >
            <div class="sk-fading-circle">
                <div class="sk-circle1 sk-circle"></div>
                <div class="sk-circle2 sk-circle"></div>
                <div class="sk-circle3 sk-circle"></div>
                <div class="sk-circle4 sk-circle"></div>
                <div class="sk-circle5 sk-circle"></div>
                <div class="sk-circle6 sk-circle"></div>
                <div class="sk-circle7 sk-circle"></div>
                <div class="sk-circle8 sk-circle"></div>
                <div class="sk-circle9 sk-circle"></div>
                <div class="sk-circle10 sk-circle"></div>
                <div class="sk-circle11 sk-circle"></div>
                <div class="sk-circle12 sk-circle"></div>
            </div>
            <span class="blink">Loading ...</span>
        </div>

        @yield('content')

        <div id="alert-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content partial-container" data-name="modal-content">
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#loading-screen').hide();
        setTimeout(() => {
            
        }, 3000);

        setTimeout(() => {
            $('#loading-screen').fadeOut();
        }, 10000);
    </script>
        
</body>
</html>
