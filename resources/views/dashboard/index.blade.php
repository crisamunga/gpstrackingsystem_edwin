@extends('layouts.manage')

@section('side-bar')

<button data-source="" class="btn btn-dashboard btn-dashboard pull-left partial-button-embed" data-container="terminalmanagement" style="margin-right: 3px;">
    <div class="mdi mdi-clipboard-text mdi-48px"></div> Overview
</button>

<button data-source="" class="btn btn-dashboard btn-dashboard-red pull-left partial-button-embed" data-container="terminalmanagement" style="margin-right: 3px;">
    <div class="mdi mdi-speedometer mdi-48px"></div> Performance
</button>

<button data-source="" class="btn btn-dashboard btn-dashboard-green pull-left partial-button-embed" data-container="terminalmanagement" style="margin-right: 3px;">
    <div class="mdi mdi-map-marker-radius mdi-48px"></div> Location data
</button>

<button data-source="" class="btn btn-dashboard btn-dashboard-blue pull-left partial-button-embed" data-container="terminalmanagement" style="margin-right: 3px;">
    <div class="mdi mdi-alert mdi-48px"></div> Alerts
</button>

@endsection

@section('body')

<div class="partial-container" id="terminalmanagement" data-name="terminalmanagement">
    <div class="--auto-load" data-source="" data-container="terminalmanagement" data-state="ready"></div>
</div>

@endsection

@section('footer')
@include('components.flash')    

<script>
    $('.partial-container').each(function(index){
        $(this).partialLoader('container');
    });
    $('.partial-link').each(function(index){            
        $(this).partialLoader('link');
    });
    $('.auto-load').each(function(index) {        
        $(this).partialLoader('autoload');
    });
    $('.partial-button').each(function(index) {        
        $(this).partialLoader('button');
    });
</script>

@endsection