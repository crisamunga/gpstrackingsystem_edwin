@extends('layouts.partial')

@section('body')

<h2 class="text-center">Admin Control Panel</h2>

<div class="col-sm-3">

<button data-source="{{ route('users.index') }}" class="btn btn-dashboard btn-dashboard pull-left partial-button-embed" data-container="admin" style="margin-right: 3px;">
    <div class="mdi mdi-account mdi-48px"></div> Users
</button>

</div>

<div class="col-sm-3">

<button data-source="{{ route('roles.index') }}" class="btn btn-dashboard btn-dashboard-red pull-left partial-button-embed" data-container="admin" style="margin-right: 3px;">
    <div class="mdi mdi-account-multiple mdi-48px"></div> User groups
</button>

</div>

<div class="col-sm-3">

<button data-source="{{ route('permissions.index') }}" class="btn btn-dashboard btn-dashboard-green pull-left partial-button-embed" data-container="admin" style="margin-right: 3px;">
    <div class="mdi mdi-key-variant mdi-48px"></div> User permissions
</button>

</div>

<div class="col-sm-3">

<button data-source="{{ route('terminals.index') }}" class="btn btn-dashboard btn-dashboard-blue pull-left partial-button-embed" data-container="admin" style="margin-right: 3px;">
    <div class="mdi mdi-car mdi-48px"></div> Terminals
</button>

</div>

@endsection

@section('footer')
@include('components.flash')

<script>
    $(function(){
        $('.partial-button-embed').each(function(index) {
            $(this).click(function(e) {
                $('.active').removeClass();
                var link = $(this).data('source');
                $('a[href = "'+link+'"]').parent().addClass('active');
            });
        });
    });
</script>

@endsection