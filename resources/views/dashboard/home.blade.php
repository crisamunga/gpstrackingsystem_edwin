@extends('layouts.partial')

@section('body')
    <div class="col-lg-9">

        <div class="panel panel-default">
            <div class="panel-heading">Links</div>
            <div class="panel-body">
                <div class="partial-container" id="homelinks" data-name="homelinks">
                    <div class="col-sm-3">
                        @can('Terminal Control')
                            <a href="{{ url('/terminalcontrol') }}" class="btn btn-dashboard btn-dashboard-aqua pull-left" style="margin-right: 3px;">
                                <div class="mdi mdi-map-marker-multiple mdi-48px"></div> Map
                            </a>
                        @endcan
                    </div>
                    <div class="col-sm-3">
                        <button data-source="{{ route('users.index') }}" class="btn btn-dashboard btn-dashboard-purple pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
                            <div class="mdi mdi-car mdi-48px"></div> Terminals
                        </button>
                    </div>
                        
                    <div class="col-sm-3">
                        <button data-source="{{ route('reports.index') }}" class="btn btn-dashboard btn-dashboard-blue pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
                            <div class="mdi mdi-clipboard-text mdi-48px"></div> Reports
                        </button>
                    </div>
                        
                    <div class="col-sm-3">
                        <a href="{{ route('myaccount.index') }}" class="btn btn-dashboard btn-dashboard-green pull-left" style="margin-right: 3px;">
                            <div class="mdi mdi-account mdi-48px"></div> My Account
                        </a>
                    </div>
                </div>
            </div>

        </div>

        @can('Access Admin Panel')
            <div class="panel panel-default">
                <div class="panel-heading">Admin</div>
                <div class="panel-body">
                    <div class="partial-container" id="homelinks" data-name="homelinks">
                        <div class="col-sm-3">
                            <a href="{{ url('/manageusers') }}" class="btn btn-dashboard btn-dashboard-aqua pull-left" style="margin-right: 3px;">
                                <div class="mdi mdi-account-key mdi-48px"></div> Admin
                            </a>
                        </div>
                        @can('Manage Server')
                            <div class="col-sm-3">
                                <button data-source="{{ route('users.index') }}" class="btn btn-dashboard btn-dashboard-purple pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
                                    <div class="mdi mdi-server mdi-48px"></div> Server
                                </button>
                            </div>
                        @endcan
                    </div>
                </div>

            </div>
        @endcan

    </div>

    <div class="col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">Dashboard</div>
            <div class="panel-body">
                <h1>{{ $y }}</h1>
                <h4>{{ $md }}</h4>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
        
                You are logged in!
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Recent Events</div>
            <div class="panel-body">
                @if (isset($data))
                    @foreach ($data as $entry)
                        <div>
                            <h4>{{$entry['age']}}</h4>
                            <h5>{{$entry['name']}}</h5>
                            <p>{{$entry['description']}}</p>
                            <p>{{$entry['date']}}</p>
                        </div>
                    @endforeach
                @else
                    <p>No recent events</p>
                @endif
            </div>
        </div>
    </div>

</div>

@endsection

@section('footer')
@include('components.flash')
<script>
    $(function(){
        $('.partial-button-embed').each(function(index) {
            $(this).click(function(e) {
                $('.active').removeClass();
                var link = $(this).data('source');
                $('a[href = "'+link+'"]').parent().addClass('active');
            });
        });
    });
</script>

@endsection