<script>
    $('.page-stepform.hidepage').each((index, elem) => {
        $(elem).hide();
    });
    $('.btn-stepform').each((index, elem) => {
        var target = $($(elem).data('target'));
        var subject = $($(elem).data('hide'));
        $(elem).click(function (e) {
            target.show();
            subject.hide();
        });
    });
</script>