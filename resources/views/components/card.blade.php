<div class="panel">
    <div class="text-center panel-heading">{{$heading}}</div>
    <div class="panel-body"> {{$slot}} </div>
    <div class="text-center panel-footer"> {{$footer}} </div>
</div>