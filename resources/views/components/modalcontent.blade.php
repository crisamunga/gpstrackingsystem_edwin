<div class="modal-header bg-blue">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3 class="modal-title" id="alert-title">
        {{$title}}
    </h3>
</div>
<div class="modal-body" style="margin: 1em;">
    {{ $slot }}
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-red" data-dismiss="modal"><span class="mdi mdi-close"></span> Dismiss</button>
</div>
