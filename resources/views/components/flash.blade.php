@if(isset($flash_message))
{{-- <div id="flash" data-notification="{{ $flash_message }}"><div> --}}
<script>
    flash_message("{{ $flash_message['message'] }}", "{{ $flash_message['title'] }}", "{{ $flash_message['sender'] }}", "{{ $flash_message['type'] }}");
</script>

@endif