<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>GPSTrackingSystem</title>

        <!-- Fonts -->
        {{-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css"> --}}

        <!-- Styles -->
        <style>
            @font-face {
                font-family: Raleway;   
                src: url(fonts/Raleway/Raleway-Medium.ttf);
            }
            html, body {
                color: #eee;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #ddd;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .cover-image-container {
                background-color: rgb(23, 23, 23);
                position: absolute;
                top: 0;
                left: 0;
                height: 100%;
                width: 100%;
                z-index: -1;
            }

            .cover-image {
                opacity: .3;
                height: 100%;
                width: 100%;
            }

            a:hover {
                color: rgb(255, 124, 17);
            }
        </style>
        <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="cover-image-container">
            <img src="{{asset('img/cover.jpg')}}" alt="Satellite" class="cover-image">
        </div>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links slideInLeft animated">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                        <a href="{{ url('/map') }}">Map</a>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md bounceInDown animated">
                    GPS Tracking System
                </div>

                <div class="links slideInUp animated">
                    <a href="">Website</a>
                    <a href="">Forum</a>
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                        <a href="{{ url('/map') }}">Map</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                    @endauth
                    <a href="">Site Help</a>
                    <a href="">FAQs</a>
                </div>
            </div>
        </div>
    </body>
</html>
