<?php

return [
    "webapp" => [
        "settimezone" => [
            "description" => "Set time zone"
        ],
    
        "trackbytime" => [
            "description" => "Set track on time intervals"
        ],

        "trackbydistance" => [
            "description" => "Set track by distance intervals"
        ],
    
        "canceltrack" => [
            "description" => "Cancel track"
        ],

        "setoverspeedalarm" => [
            "description" => "Set Overspeed alarm"
        ],
    
        "setmovementalarm" => [
            "description" => "Set movement alarm"
        ],

        "cancelalarm" => [
            "description" => "Cancel alarm"
        ],
    
        "cutoffoilandpower" => [
            "description" => "Set cut off oil and engine power"
        ],
    
        "resumeoilandpower" => [
            "description" => "Set resume oil and engine power"
        ],
    
        "arm" => [
            "description" => "Arm"
        ],
    
        "disarm" => [
            "description" => "Disarm"
        ],
    
        "smsmode" => [
            "description" => "Set SMS Mode"
        ],
    
        "setgeofence" => [
            "description" => "Set geo fence"
        ],
    
        "cancelgeofence" => [
            "description" => "Cancel geo fence"
        ],
    
        "uploaddata" => [
            "description" => "Set load data"
        ],
    
        "canceldataupload" => [
            "description" => "Cancel load data"
        ],
    
        "setlessgprs" => [
            "description" => "Set less GPRS"
        ],

        "requestPhoto" => [
            "description" => "Request photo"
        ]
    ],

    "TK 103A" => [
        // Separator
        "delimiter" => "/,/",
        
        // Terminator for each message. Removed with rtrim
        "terminator" => ";",

        // Prefix for each message. Removed with preg replace
        "prefix" => "##,imei:",

        // Format => string or binary
        "format" => "string",
        
        "device" => [
            "parameters" => [
                "imei",
                "keyword",
                "time",
                "cellPhoneNumber",
                "gpsSignalPresent",
                "gmtTime",
                "dataValidity",
                "latitude",
                "latitudeOrientation",
                "longitude",
                "longitudeOrientation",
                "speed",
                "directionOrRequest",
                "altitude",
                "accState",
                "doorOpenState",
                "oilPercent1",
                "oilPercent2",
                "temperature"
            ],

            "keywords" => [
                "empty" => [
                    "type" => "Heartbeat",
                    "description" => "Heartbeat",
                    "response" => "ON"
                ],

                "A" => [
                    "type" => "Login",
                    "description" => "Now online",
                    "response" => "LOAD"
                ],
            
                "Logout" => [
                    "type" => "Logout",
                    "description" => "Now offline",
                    "response" => ""
                ],
            
                "tracker" => [
                    "type" => "location data",
                    "description" => "Location data",
                    "response" => ""
                ],
            
                "dt" => [
                    "type" => "response",
                    "description" => "Cancel auto track successful",
                    "response" => ""        
                ],
            
                "et" => [
                    "type" => "response",
                    "description" => "Cancel alarm successful",
                    "response" => ""        
                ],
            
                "gt" => [
                    "type" => "response",
                    "description" => "Set movement alarm successful",
                    "response" => ""        
                ],
            
                "ht" => [
                    "type" => "response",
                    "description" => "Set overspeed alarm successful",
                    "response" => ""        
                ],
            
                "it" => [
                    "type" => "response",
                    "description" => "Set time zone successful",
                    "response" => ""        
                ],
            
                "jt" => [
                    "type" => "response",
                    "description" => "Cut off oil and power successful",
                    "response" => ""        
                ],
            
                "kt" => [
                    "type" => "response",
                    "description" => "Resume oil and power successful",
                    "response" => ""        
                ],
            
                "lt" => [
                    "type" => "response",
                    "description" => "Set arm successful",
                    "response" => ""        
                ],
            
                "mt" => [
                    "type" => "response",
                    "description" => "Disarm successful",
                    "response" => ""        
                ],
            
                "nt" => [
                    "type" => "response",
                    "description" => "Switch to SMS mode successful",
                    "response" => ""        
                ],

                "ot" => [
                    "type" => "response",
                    "description" => "Set GEO fence successful",
                    "response" => ""        
                ],
            
                "pt" => [
                    "type" => "response",
                    "description" => "Cancel geo fence successful",
                    "response" => ""        
                ],

                "qt" => [
                    "type" => "response",
                    "description" => "Start data upload successful",
                    "response" => ""        
                ],
            
                "st" => [
                    "type" => "response",
                    "description" => "Stop data upload successful",
                    "response" => ""        
                ],

                "tt" => [
                    "type" => "response",
                    "description" => "Set less GPRS successful",
                    "response" => ""        
                ],

                "vt" => [
                    "type" => "response",
                    "description" => "Take photo request successful",
                    "response" => ""        
                ],
            
                "help me" => [
                    "type" => "alarm",
                    "description" => "SOS alarm",
                    "response" => ""
                ],
            
                "low battery" => [
                    "type" => "alarm",
                    "description" => "Low battery alarm",
                    "response" => ""
                ],
            
                "move" => [
                    "type" => "alarm",
                    "description" => "Movement alarm",
                    "response" => ""
                ],
            
                "speed" => [
                    "type" => "alarm",
                    "description" => "Overspeed alarm",
                    "response" => ""
                ],
            
                "stockade" => [
                    "type" => "alarm",
                    "description" => "GEO fence has been breached",
                    "response" => ""
                ],
            
                "ac alarm" => [
                    "type" => "alarm",
                    "description" => "Vehicle was powered off",
                    "response" => ""
                ],
            
                "door alarm" => [
                    "type" => "alarm",
                    "description" => "Vehicle door is open",
                    "response" => ""
                ],
            
                "sensor alarm" => [
                    "type" => "alarm",
                    "description" => "Shock alarm",
                    "response" => ""
                ],
            
                "acc alarm" => [
                    "type" => "alarm",
                    "description" => "ACC alarm",
                    "response" => ""
                ],

                "acc off" => [
                    "type" => "alarm",
                    "description" => "ACC off alarm",
                    "response" => ""
                ],

                "acc on" => [
                    "type" => "alarm",
                    "description" => "ACC on alarm",
                    "response" => ""
                ],
            
                "accident alarm" => [
                    "type" => "alarm",
                    "description" => "Accident may have occured",
                    "response" => ""
                ],
            
                "bonnet alarm" => [
                    "type" => "alarm",
                    "description" => "Bonnet alarm",
                    "response" => ""
                ],
            
                "footbreak alarm" => [
                    "type" => "alarm",
                    "description" => "Footbreak alarm",
                    "response" => ""
                ],
            
                "T:" => [
                    "type" => "alarm",
                    "description" => "Temperature alarm",
                    "response" => ""
                ],
            
                "oil" => [
                    "type" => "alarm",
                    "description" => "Fuel alarm",
                    "response" => ""
                ],
            
                "DTC" => [
                    "type" => "alarm",
                    "description" => "Diagnostic problem in terminal",
                    "response" => ""
                ],
            
                "service" => [
                    "type" => "alarm",
                    "description" => "Vehicle maintenance notification",
                    "response" => ""
                ],
            
                "Stopped" => [
                    "type" => "soft alarm",
                    "description" => "Vehicle has stopped",
                    "response" => ""
                ],
            
                "Start" => [
                    "type" => "soft alarm",
                    "description" => "Vehicle has started moving",
                    "response" => ""
                ],
            
                "Harsh Brake" => [
                    "type" => "soft alarm",
                    "description" => "Vehicle experienced a harsh brake",
                    "response" => ""
                ],
            
                "Rapid acceleration" => [
                    "type" => "soft alarm",
                    "description" => "Vehicle experienced a rapid acceleration",
                    "response" => ""
                ]
            ],
        ],

        "webapp" => [
            // Separator
            "delimiter" => ",",
            
            // Terminator for each message. Removed with rtrim
            "terminator" => ";",

            // Prefix for each message. Removed with preg replace
            "prefix" => "**,imei:",

            "settimezone" => [
                "keyword" => "I",
                "parameter" => "(^[+]?[09][1,2][\\.]?[05]?$)",
                "description" => "Set time zone"
            ],
        
            "trackbytime" => [
                "keyword" => "C",
                "parameter" => "(^([123][05]s)|([15]m)|([13]0m)|([15]h)|(10h)|(24h)$)",
                "description" => "Set track on time intervals"
            ],

            "trackbydistance" => [
                "keyword" => "F",
                "parameter" => "(^(0[01258][05]0m)|([125]km)|(10km)$)",
                "description" => "Set track by distance intervals"
            ],
        
            "canceltrack" => [
                "keyword" => "D",
                "description" => "Cancel track"
            ],

            "setoverspeedalarm" => [
                "keyword" => "H",
                "parameter" => "(^(0[1234568]0)|(1[258]0)|(2[25]0)|(300)$)",
                "description" => "Set Overspeed alarm"
            ],
        
            "setmovementalarm" => [
                "keyword" => "G",
                "description" => "Set movement alarm"
            ],

            "cancelalarm" => [
                "keyword" => "E",
                "description" => "Cancel alarm"
            ],
        
            "cutoffoilandpower" => [
                "keyword" => "J",
                "description" => "Set cut off oil and engine power"
            ],
        
            "resumeoilandpower" => [
                "keyword" => "K",
                "description" => "Set resume oil and engine power"
            ],
        
            "arm" => [
                "keyword" => "L",
                "description" => "Set arm"
            ],
        
            "disarm" => [
                "keyword" => "M",
                "description" => "Cancel arm"
            ],
        
            "smsmode" => [
                "keyword" => "N",
                "description" => "Set SMS Mode"
            ],
        
            "setgeofence" => [
                "keyword" => "O",
                "parameter" => [
                    "latitude" => "(^[+]?(([09][1,2])|(1[07][09]))(\\.[09]+)?$)",
                    "longitude" => "(^[+]?([08]?[09])(\\.[09]+)?$)"
                ],
                "description" => "Set geo fence"
            ],
        
            "cancelgeofence" => [
                "keyword" => "P",
                "description" => "Cancel geo fence"
            ],
        
            "uploaddata" => [
                "keyword" => "Q",
                "description" => "Set load data"
            ],
        
            "canceldataupload" => [
                "keyword" => "S",
                "description" => "Cancel load data"
            ],
        
            "setlessgprs" => [
                "keyword" => "T",
                "description" => "Set less GPRS"
            ],

            "requestPhoto" => [
                "keyword" => "V",
                "description" => "Request photo"
            ]
        ]
    ]
];