<?php

return [
    // Frequency in seconds after which polled values are stored
    'save_frequency' => 0,
    
    'web_sockets' => [
        [
            "name" => "main",
            "ip" => "127.0.0.1",
            "port" => "31340",
            "client count limit" => "-1"
        ]
    ],

    'api_keys' => [
        'google_maps' => 'AIzaSyAH_JEW5yzDXLdpaXVLzx01rtvPu3j_8W4'
    ]
];